function FXData = loadFXData()

pathname = 'C:\Users\rogie\Dropbox\PhD\Data\ProjectsWithProfPurnanandam_RogierHanselaar\CoCosAndRiskTaking\Data\FXData\';
FXData = readtable(strcat(pathname,'FormattedFXDataBloomberg2000_2016.csv'));
FXData.FromISOCodeCurrency = cellfun(@(x) x(1:3),FXData.Identifiers,'UniformOutput',false);
FXData = FXData(:,[end 1:end-1]);
FXData.Properties.VariableNames{'PX_LAST'} = 'ExchangeRate';
FXData.date = str2double(cellfun(@(x) [x(1:4) x(6:7) x(9:10)], FXData.Date,'UniformOutput',false)); FXData.Date=[];

%% Fix the fact that for HUF, JPY, and XPF the exchange rates are too large
% Deflate by factor 100
IndexDeflate = strcmpi(FXData.FromISOCodeCurrency,'HUF') | strcmpi(FXData.FromISOCodeCurrency,'JPY') | strcmpi(FXData.FromISOCodeCurrency,'XPF');
FXData.ExchangeRate(IndexDeflate) = FXData.ExchangeRate(IndexDeflate)/100;

%% Fix the fact there is no exchange rate on non-trading dates (e.g. 20111231)
% Create framework (this creates non-existent dates but that is not an
% issue)
MonthsKron = kron([1:12]',ones(31,1))*100 + repmat([1:31]',12,1);
DatesFrameWork = kron([2000:2016]',ones(size(MonthsKron,1),1))*10000+repmat(MonthsKron,17,1);
DatesFrameWork(DatesFrameWork>20161231) = [];
DatesFrameWork = array2table(DatesFrameWork);
DatesFrameWork.Properties.VariableNames{'DatesFrameWork'} = 'date';
Currencies = unique(FXData.FromISOCodeCurrency)';
Dates = table();
for c = Currencies
    TempDates = DatesFrameWork;
    TempDates.Currency = repmat(c,size(TempDates,1),1);
    Dates = [Dates;TempDates];
end

% Merge FXData into framework
FXData = outerjoin(Dates,FXData,...
    'Type','Left',...
    'LeftKeys',{'Currency','date'},...
    'RightKeys',{'FromISOCodeCurrency','date'},...
    'RightVariables',{'ExchangeRate'});

% Fill missing values at 20000101 with first non-missing value after
% 20000101
FXData.ExchangeRate(FXData.date==20000101 & strcmpi(FXData.Currency,'EUR'))=1;
for c = unique(FXData.Currency)'
    if ~strcmpi(c{1},'EUR')
        IndexC = strcmpi(FXData.Currency,c{1});
        IndexStartDate = FXData.date==20000101;
        LocFirstNonNaNCountryC = find(IndexC & ~isnan(FXData.ExchangeRate),1);
        FXData.ExchangeRate(IndexC & IndexStartDate) = FXData.ExchangeRate(LocFirstNonNaNCountryC);
    end
end

% Replace missing observations with the first non-missing preceding value
% (note: missing values at 20000101 have been filled up already)
FXData = sortrows(FXData,'date','ascend');
FXData = sortrows(FXData,'Currency','ascend');
while sum(isnan(FXData.ExchangeRate))>0
    FXData.ExchangeRate_lag = NaN(size(FXData,1),1);
    FXData.ExchangeRate_lag(2:end) = FXData.ExchangeRate(1:end-1);
    IndexNaN = isnan(FXData.ExchangeRate) & ~isnan(FXData.ExchangeRate_lag);
    FXData.ExchangeRate(IndexNaN) = FXData.ExchangeRate_lag(IndexNaN);
    FXData.ExchangeRate_lag=[];
end

end