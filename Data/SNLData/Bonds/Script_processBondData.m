clear

%% Load data
% Set pathname where data is
pathname = 'C:\Users\rogie\Dropbox\PhD\Data\ProjectsWithProfPurnanandam_RogierHanselaar\CoCosAndRiskTaking\Data\SNLData\Bonds\';

% Load SNL Bond data
filename = strcat(pathname,'20170717BondIssuanceSNL.csv');
SNLBondData = importSNLBondData(filename);
SNLBondData(SNLBondData.OfferingAnnouncementDate>20170000,:)=[];

% Load linking table SNL key to ISIN
filename = strcat(pathname,'20170717LinkingTableFromSNLInstitutionKey.csv');
LinkingTableSNLInstitutionKey = importLinkingTableSNLKeyToISIN(filename);

% Add ISINs to SNL Bond data
SNLBondData = outerjoin(SNLBondData,LinkingTableSNLInstitutionKey,...
    'Type','Left',...
    'LeftKeys',{'SNLInstitutionKey'},...
    'RightKeys',{'SNLInstitutionKey'},...
    'RightVariables',{'InstitutionISIN','InstitutionSWIFT'});

%% Impose filters
% Funding type: 'Junior Subordinated Debt', 'Senior Debt', 'Subordinated
% Debt' 

% Secondary offering: No
SNLBondData(~strcmpi(SNLBondData.SecondaryYesNo,'No'),:)=[];

% Callable: No
SNLBondData(~strcmpi(SNLBondData.CallableYesNo,'No'),:)=[];

% Convertible: No
SNLBondData(~strcmpi(SNLBondData.ConvertibleYesNo,'No'),:)=[];

%% Convert currencies to EUR
% Load FXData
FXData = loadFXData();

% Convert variables to EUR
varsToConvert = {'OfferingPrice','GrossAmountOfferedExcludingOverallotment'};
SNLBondData = convertCurrenciesToEUR(SNLBondData,FXData,varsToConvert);


%% Write data away
filename = strcat(pathname,'ProcessedBondIssuanceSNL.mat');
save(filename,'SNLBondData')