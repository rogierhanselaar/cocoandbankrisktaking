function SwiftBvDIDNumberLinkingTable = createSwiftBvDIDNumberLinkingTable(Data)

SwiftBvDIDNumberLinkingTable = array2table(Data(3:end,1:2),'VariableNames',Data(1,1:2));
IndexMultipleSwifts = cell2mat(cellfun(@(x) ~isempty(strfind(x,' COMMA ')),SwiftBvDIDNumberLinkingTable.SWIFT,'UniformOutput',false));
SwiftBvDIDNumberLinkingTable.SWIFT_split = cell(size(SwiftBvDIDNumberLinkingTable,1),1);
SwiftBvDIDNumberLinkingTable.SWIFT_split(IndexMultipleSwifts) = cellfun(@(x) strsplit(x,' COMMA '),SwiftBvDIDNumberLinkingTable.SWIFT(IndexMultipleSwifts),'UniformOutput',false);

% Add each row with multiple SWIFT to a separate table and unfold the
% SWIFTs into separate rows
UnfoldedPartOfSNLToSWIFTLinkingTable = SwiftBvDIDNumberLinkingTable(:,1:2);
UnfoldedPartOfSNLToSWIFTLinkingTable(:,:)=[];
for i = find(IndexMultipleSwifts)'
    N_swifts = size(SwiftBvDIDNumberLinkingTable.SWIFT_split{i},2);
    UnfoldedPartOfSNLToSWIFTLinkingTable = ...
        [UnfoldedPartOfSNLToSWIFTLinkingTable; ...
        array2table(...
        [repmat(SwiftBvDIDNumberLinkingTable.SNLInstitutionKey(i),N_swifts,1)...
        SwiftBvDIDNumberLinkingTable.SWIFT_split{i}'],...
        'VariableNames',UnfoldedPartOfSNLToSWIFTLinkingTable.Properties.VariableNames)...
        ];
end

% Replace the rows with multiple SWIFTs with the unfolded rows
SwiftBvDIDNumberLinkingTable(IndexMultipleSwifts,:)=[];
SwiftBvDIDNumberLinkingTable.SWIFT_split=[];
SwiftBvDIDNumberLinkingTable = [SwiftBvDIDNumberLinkingTable;UnfoldedPartOfSNLToSWIFTLinkingTable];

%% Create column indicating which rows need to be aggregated
% Create column indicating SWIFT groups that need to be aggregated
SwiftBvDIDNumberLinkingTable.SWIFTGroup = NaN(size(SwiftBvDIDNumberLinkingTable,1),1);
counter=1;
for s = unique(SwiftBvDIDNumberLinkingTable.SWIFT)'
    IndexS = strcmpi(SwiftBvDIDNumberLinkingTable.SWIFT,s);
    SwiftBvDIDNumberLinkingTable.SWIFTGroup(IndexS) = counter;
    counter=counter+1;
end
SwiftBvDIDNumberLinkingTable.SWIFTGroup(SwiftBvDIDNumberLinkingTable.SWIFTGroup==1)=NaN;

% Create column indicating SNLInstitutionKey groups that need to be aggregated
SwiftBvDIDNumberLinkingTable.SNLInstitutionKeyGroup = NaN(size(SwiftBvDIDNumberLinkingTable,1),1);
counter=1;
for s = unique(SwiftBvDIDNumberLinkingTable.SNLInstitutionKey)'
    IndexS = strcmpi(SwiftBvDIDNumberLinkingTable.SNLInstitutionKey,s);
    SwiftBvDIDNumberLinkingTable.SNLInstitutionKeyGroup(IndexS) = counter;
    counter=counter+1;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The order in which SWIFTGroups are combined with SNLGroups and
% CombinedGroups into new CombinedGroups may matter for the final
% CombinedGroups. Perhaps this can be fixed by repreating the procedure
% until the CombinedGroups do not change anymore. For the current sample,
% this does not seem to be an issue
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Create column indicating combined groups that need to be aggregated
SwiftBvDIDNumberLinkingTable.CombinedGroup = NaN(size(SwiftBvDIDNumberLinkingTable,1),1);
counter=1;
for swg = unique(SwiftBvDIDNumberLinkingTable.SWIFTGroup)'
    IndexSWIFTGroup = SwiftBvDIDNumberLinkingTable.SWIFTGroup==swg;

    % Loop over SNL institution keys with SWIFTGroup swg
    IndexSNLInstutionKeyGroup = false(size(SwiftBvDIDNumberLinkingTable,1),1);
    for sng = unique(SwiftBvDIDNumberLinkingTable.SNLInstitutionKeyGroup(IndexSWIFTGroup))'
        IndexSNLInstutionKeyGroup = IndexSNLInstutionKeyGroup | SwiftBvDIDNumberLinkingTable.SNLInstitutionKeyGroup==sng;
    end
    % In addition, loop over CombinedGroups with SWIFTGroup swg
    IndexCombinedGroup = false(size(SwiftBvDIDNumberLinkingTable,1),1);
    for cg = unique(SwiftBvDIDNumberLinkingTable.CombinedGroup(IndexSWIFTGroup))'
        IndexCombinedGroup = IndexCombinedGroup | SwiftBvDIDNumberLinkingTable.CombinedGroup==cg;
    end
    
    % Set combined group as the union of the SWIFTGroup, the
    % SNLInstutionKeyGroup , and the CombinedGroup
    SwiftBvDIDNumberLinkingTable.CombinedGroup(IndexSNLInstutionKeyGroup | IndexSWIFTGroup | IndexCombinedGroup) = counter;
    counter=counter+1;

end
% Check whether there are non-missing SWIFT codes that are not assigned to
% a CombinedGroup
if sum(isnan(SwiftBvDIDNumberLinkingTable.CombinedGroup(~strcmpi(SwiftBvDIDNumberLinkingTable.SWIFT,''))))>0
    error('There are non-missing SWIFT codes that are not assigned to a CombinedGroup')
end

%% Merge BvDIDNumbers into the key
% Load BvDIDNumbers and SWIFT link
BvDIDNumbersSWIFTLink = importBvDIDNumbersSWIFTLink('C:\Users\rogie\Git\cocoandbankrisktaking\Data\BankScopeData\20170719SwiftToBvDIDNumber.xlsx');

% Merge into SNLToSWIFTLinkingTable
SwiftBvDIDNumberLinkingTable = outerjoin(SwiftBvDIDNumberLinkingTable,BvDIDNumbersSWIFTLink,...
    'Type','Left',...
    'LeftKeys','SWIFT',...
    'RightKeys','SWIFT',...
    'RightVariables','BvDIDNumbers');

% Assure that one BvDIDNumber is linked to only one CombinedGroup
IndexMissing = strcmpi(SwiftBvDIDNumberLinkingTable.BvDIDNumbers,'');
if size(unique(SwiftBvDIDNumberLinkingTable(~IndexMissing,{'CombinedGroup','BvDIDNumbers'})),1)~=size(unique(SwiftBvDIDNumberLinkingTable(~IndexMissing,{'BvDIDNumbers'})),1)
    error('One BvDIDNumber is allocated to more than one CombinedGroup')
end

% Remove CombinedGroups that are linked to multiple BvDIDNumbers
temp = unique(SwiftBvDIDNumberLinkingTable(~IndexMissing,{'CombinedGroup','BvDIDNumbers'}));
temp2 = temp.CombinedGroup;
[n, bin] = histc(temp2, unique(temp2));
if n>1
    multiple = find(n > 1);
    index    = find(ismember(bin, multiple));
    CombinedGroupsToRemove = unique(temp.CombinedGroup(index));
    SwiftBvDIDNumberLinkingTable(SwiftBvDIDNumberLinkingTable.CombinedGroup==CombinedGroupsToRemove,:)=[];
end

% Remove rows with missing BvDIDNumbers and keep only the SWIFT and
% BvDIDNumbers link
SwiftBvDIDNumberLinkingTable(strcmpi(SwiftBvDIDNumberLinkingTable.BvDIDNumbers,''),:)=[];
SwiftBvDIDNumberLinkingTable = unique(SwiftBvDIDNumberLinkingTable(:,{'SWIFT','BvDIDNumbers'}));

end