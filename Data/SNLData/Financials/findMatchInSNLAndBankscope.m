function IndexMatch = findMatchInSNLAndBankscope(Data,CodesBankscope,identifier)

IdentifiersBS = unique(CodesBankscope.(identifier));
IdentifiersBS(strcmpi(IdentifiersBS,'<undefined>'))=[];
IdentifiersBS(strcmpi(IdentifiersBS,''))=[];

if strcmpi(identifier,'ISIN')
    IndexMatch = ismember(Data(:,3),IdentifiersBS);
elseif strcmpi(identifier,'SWIFT')
    Data(2,1) = {'-'};Data(2,2) = {'-'};
    Data = array2table(Data(:,1:2),'VariableNames',Data(1,1:2));
    IndexMatch = ismember(Data.(identifier),IdentifiersBS);
    
    % find firms with multiple swift codes
    IndexMultipleSwiftCodes = cellfun(@(x) ~isempty(x),(cellfun(@(x) findstr(x,' COMMA '),Data.SWIFT,'UniformOutput',false)));
    
    % Split the SWIFT codes
    splitSWIFTCodesTemp = cellfun(@(x) strsplit(x,' COMMA '),Data.SWIFT(IndexMultipleSwiftCodes), 'UniformOutput',false);
    N_splitSWIFTCodes = size(splitSWIFTCodesTemp,1);
    splitSWIFTCodes = cell(N_splitSWIFTCodes,1);
    for i = 1:N_splitSWIFTCodes
        N = size(splitSWIFTCodesTemp{i},2);
        splitSWIFTCodes(i,1:N) = splitSWIFTCodesTemp{i};
    end
    EmptyDoubles = cellfun(@(x) isnumeric(x),splitSWIFTCodes);
    splitSWIFTCodes(EmptyDoubles) = cellfun(@(x) num2str(x),splitSWIFTCodes(EmptyDoubles),'UniformOutput',false);

    % Give each SWIFT code a place in the table
    for i = 1:size(splitSWIFTCodes,2)
        SwiftVariableName = strcat('SWIFT',num2str(i));
        Data.(SwiftVariableName) = cell(size(Data,1),1);
        Data.(SwiftVariableName)(IndexMultipleSwiftCodes) = splitSWIFTCodes(:,i);
        EmptyDoubles = cellfun(@(x) isnumeric(x),Data.(SwiftVariableName));
        Data.(SwiftVariableName)(EmptyDoubles) = cellfun(@(x) num2str(x),Data.(SwiftVariableName)(EmptyDoubles),'UniformOutput',false);
    end

    % Update IndexMatch
    for i = 1:size(splitSWIFTCodes,2)
        IndexMatch = IndexMatch | ismember(Data.(strcat('SWIFT',num2str(i))),IdentifiersBS);
    end
    
end

IndexMatch(1:2,1) = 1;

end