function formattedData = reformatData(Data, identifier)

% Load variable classifications and readable names
[TA,varsLevel,varsLevelNames,varsRatio,varsRatioNames] = loadVariableClassifications();

if strcmpi(identifier,'ISIN')
    % Only keep the ISINs
    formattedData = Data(:,3:end)';
elseif strcmpi(identifier,'SWIFT')
    % Load key between SWIFTs and BvDIDNumbers
    SwiftBvDIDNumberLinkingTable = createSwiftBvDIDNumberLinkingTable(Data);
    
    % Merge the BvDIDNumbers into Data using the key
    formattedData = Data;
    formattedData(3:end,:) = sortrows(formattedData(3:end,:));
    tempData = array2table(formattedData(3:end,1:2),'VariableNames',formattedData(1,1:2));
    tempData = outerjoin(tempData,SwiftBvDIDNumberLinkingTable,...
        'Type','Left',...
        'LeftKeys','SWIFT',...
        'RightKeys','SWIFT',...
        'RightVariables','BvDIDNumbers');
    tempData = sortrows(tempData,'SNLInstitutionKey','ascend');
    if sum(~strcmpi(tempData.SNLInstitutionKey,formattedData(3:end,1)))>0
        error('Please check the merging of BvDIDNumbers into formattedData')
    end
    formattedData = [[formattedData(1:2,1:3);table2array(tempData)] formattedData];formattedData{1,3} = 'BvDIDNumbers';
    
    % Only keep the BvDIDNumbers and transpose
    formattedData = formattedData (:,[3 7:end])';
end
formattedData{1,1} = 'Variables';
formattedData{1,2} = 'Year';
formattedData(:,strcmpi(formattedData(1,:),''))=[];

if strcmpi(identifier,'ISIN')
    % Form a table
    formattedData = array2table(formattedData(2:end,:),'VariableNames',formattedData(1,:));
elseif strcmpi(identifier,'SWIFT')
    % Add duplicates of RecordedInvestmentOfImpairedLoans and TotalGrossLoans
    N_Years = size(unique(formattedData(:,2)),1)-1;
    RIOIL = formattedData(strcmpi(formattedData(:,1),'226937'),:);
    RIOIL(:,1) = repmat({'226937_2'},N_Years,1);
    TGL = formattedData(strcmpi(formattedData(:,1),'132210'),:);
    TGL(:,1) = repmat({'132210_2'},N_Years,1);
    formattedData = [formattedData; RIOIL;TGL];
    
    % For entities sharing BvDIDNumbers (due to SWIFTs being shared),
    % aggregate level data and average ratios using TA as weights
    N_varsRatio = size(varsRatio,2);
    N_varsLevel = size(varsLevel,2) +2; % +2 because of duplicates made above
    IndexTA = ismember(formattedData(:,1),TA);
    duplicateBvDIDNumbers = cellfun(@(x) find(strcmp(formattedData(1,:), x)==1), unique(formattedData(1,3:end)), 'UniformOutput', false);
    AggregatedData = [formattedData(:,1:2) cell(1+(N_varsRatio+N_varsLevel)*N_Years,size(duplicateBvDIDNumbers,2))];
    counter=3;
    for d = duplicateBvDIDNumbers
        N_d = size(d{1},2);
%         if sum(d{1}==76)>0
%             d
%         end
        if N_d==1
            AggregatedData(1,counter) = formattedData(1,d{1});
            AggregatedData(2:end,counter) = num2cell(cellfun(@(x) str2double(x),formattedData(2:end,d{1})));
        else
            % Set BvDIDNumber in first row
            AggregatedData(1,counter) = unique(formattedData(1,d{1}));
            
            % Average ratios using TA as weights
            for r = varsRatio
                IndexR = strcmpi(formattedData(:,1),r);
                tempData = cellfun(@(x) str2double(x),formattedData(IndexR,d{1}));
                tempDataTA = cellfun(@(x) str2double(x),formattedData(IndexTA,d{1}));
                tempDataTA(isnan(tempData)) = NaN;
                tempDataTAsum = nansum(tempDataTA,2)*ones(1,N_d);
                tempWeights = tempDataTA./tempDataTAsum;
                tempWeightedAverage = tempData.*tempWeights;
                IndexIsNaN = sum(isnan(tempWeightedAverage),2)==size(tempData,2);
                AggregatedData(IndexR,counter) = num2cell(nansum(tempWeightedAverage,2));
                AggregatedDataTemp = AggregatedData(IndexR,counter);
                AggregatedDataTemp(IndexIsNaN) = {NaN};
                AggregatedData(IndexR,counter) = AggregatedDataTemp;
            end
            
            % Aggregate level variables
            IndexL = ismember(formattedData(:,1),varsLevel);
            tempData = cellfun(@(x) str2double(x),formattedData(IndexL,d{1}));
            IndexIsNaN = sum(isnan(tempData),2)==size(tempData,2);
            AggregatedData(IndexL,counter) = num2cell(nansum(tempData,2));
            AggregatedDataTemp = AggregatedData(IndexL,counter);
            AggregatedDataTemp(IndexIsNaN) = {NaN};
            AggregatedData(IndexL,counter) = AggregatedDataTemp;
            
            % Aggregate duplicate variables
            Index226937_2 = strcmpi(formattedData(:,1),'226937_2');
            Index132210_2 = strcmpi(formattedData(:,1),'132210_2');
            tempData226937_2 = cellfun(@(x) str2double(x),formattedData(Index226937_2,d{1}));
            tempData132210_2 = cellfun(@(x) str2double(x),formattedData(Index132210_2,d{1}));
            IndexIsNaN = isnan(tempData226937_2) | isnan(tempData132210_2);
            tempData226937_2(IndexIsNaN) = NaN;
            tempData132210_2(IndexIsNaN) = NaN;
            IndexIsNaN = sum(isnan(tempData226937_2),2)==size(tempData226937_2,2);
            AggregatedData(Index226937_2,counter) = num2cell(nansum(tempData226937_2,2));
            AggregatedData(Index132210_2,counter) = num2cell(nansum(tempData132210_2,2));
            AggregatedDataTemp = AggregatedData(Index226937_2,counter);
            AggregatedDataTemp(IndexIsNaN) = {NaN};
            AggregatedData(Index226937_2,counter) = AggregatedDataTemp;
            AggregatedDataTemp = AggregatedData(Index132210_2,counter);
            AggregatedDataTemp(IndexIsNaN) = {NaN};
            AggregatedData(Index132210_2,counter) = AggregatedDataTemp;
        end
        counter = counter+1;
    end
    formattedData = array2table(AggregatedData(2:end,:),'VariableNames',AggregatedData(1,:));
end

formattedData = stack(formattedData,formattedData.Properties.VariableNames(3:end));
if strcmpi(identifier,'SWIFT')
    formattedData.Properties.VariableNames(3) = {'BvDIDNumbers'};
    formattedData.Properties.VariableNames(4) = {'value'};
    formattedData.value = cell2mat(formattedData.value);
else
    formattedData.Properties.VariableNames(3) = {identifier};
    formattedData.Properties.VariableNames(4) = {'value'};
    formattedData.value = str2double(formattedData.value);
end

% Unstack the variables to get a regular long format
formattedData = unstack(formattedData,'value','Variables');
formattedData = formattedData(:,[2 1 3:end]);
if strcmpi(identifier,'SWIFT')
    formattedData = sortrows(formattedData,'BvDIDNumbers','ascend');
else
    formattedData = sortrows(formattedData,identifier,'ascend');
end

% Replace variableNames with something readable
for l = varsLevel
    IndexL = strcmpi(varsLevel,l);
    variableNameReadable = strrep(varsLevelNames(IndexL),' ','');
    IndexXL = strcmpi(formattedData.Properties.VariableNames,strcat('x',l));
    formattedData.Properties.VariableNames(IndexXL) = variableNameReadable;
end
for r = varsRatio
    IndexR = strcmpi(varsRatio,r);
    variableNameReadable = strrep(varsRatioNames(IndexR),' ','');
    IndexXR = strcmpi(formattedData.Properties.VariableNames,strcat('x',r));
    formattedData.Properties.VariableNames(IndexXR) = variableNameReadable;
end

% Add variable impaired loans over gross loans
if strcmpi(identifier,'SWIFT')
    formattedData.ImpairedLoansOverGrossLoans = 100 * formattedData.x226937_2./formattedData.x132210_2;
    formattedData.x132210_2=[];
    formattedData.x226937_2=[];
else
    formattedData.ImpairedLoansOverGrossLoans = 100 * formattedData.RecordedInvestmentOfImpairedLoans./formattedData.TotalGrossLoans;
end


end