function [TA,varsLevel,varsLevelNames,varsRatio,varsRatioNames] = loadVariableClassifications()

TA = {'132264'};
varsLevel = {'132264','132367','132319','226937','131958','132210'};
varsLevelNames = {'TotalAssets','TotalLiabilities','TotalDebt','RecordedInvestmentOfImpairedLoans','ProvisionForLoanLosses','TotalGrossLoans'};
varsRatio = {'133222'};
varsRatioNames = {'LoanLossReservesOverGrossLoans'};



end