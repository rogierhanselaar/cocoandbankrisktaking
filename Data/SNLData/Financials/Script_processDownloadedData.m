clear

% Read downloaded data
pathname = 'C:\Users\rogie\Dropbox\PhD\Data\ProjectsWithProfPurnanandam_RogierHanselaar\CoCosAndRiskTaking\Data\SNLData\Financials\';
filename = strcat(pathname,'DataDownloads\20170724TableFilledWithTSDataDirectlyFromDataWizard.csv');
temp = perl('countlines.pl',filename);
temp = strsplit(temp,',');
n_rows = str2num(temp{1});
n_cols = str2num(temp{2});
FormatSpec = repmat('%s',1,n_cols);
FileID = fopen(filename);
C = textscan(FileID,FormatSpec,n_rows,'Delimiter',',');
fclose(FileID);
Data = C{1};
for i = 2:size(C,2)
    Data = [Data,C{i}];
end

% Select only those firms for which we have an ISIN or SWIFT in Bankscope
filename = 'C:\Users\rogie\Dropbox\PhD\Data\ProjectsWithProfPurnanandam_RogierHanselaar\CoCosAndRiskTaking\Data\DataForCoAuthors\20170622DataConsolidated_2000_2016.csv';
CodesBankscope = importISINsAndSWIFTCodes(filename);
IndexMatchISIN = findMatchInSNLAndBankscope(Data,CodesBankscope,'ISIN');
IndexMatchSWIFT = findMatchInSNLAndBankscope(Data,CodesBankscope,'SWIFT');
Data(~(IndexMatchISIN | IndexMatchSWIFT),:)=[];

% Reformat data
formattedDataISIN = reformatData(Data, 'ISIN');

% There are different entities with the same SWIFT code (e.g., ACARIT22);
% most likely because they are part of the same group or cooperative. In
% the consolidated Bankscope data, each BvDIDNumber has a unique SWIFT
% code. I propose to provide each SWIFT with the corresponding BvDIDNumber
% here, use simple aggregation for variables expressed in level, and TA
% weighted aggregation for variables expressed as ratios.
formattedDataSWIFT = reformatData(Data, 'SWIFT'); 

% Write formatted
save(strcat(pathname,'ProcessedDataISIN.mat'),'formattedDataISIN')
save(strcat(pathname,'ProcessedDataSWIFT.mat'),'formattedDataSWIFT')