function tableout = importCoCoIssueData(workbookFile,sheetName,startRow,endRow)
%IMPORTFILE Import data from a spreadsheet
%   DATA = IMPORTFILE(FILE) reads data from the first worksheet in the
%   Microsoft Excel spreadsheet file named FILE and returns the data as a
%   table.
%
%   DATA = IMPORTFILE(FILE,SHEET) reads from the specified worksheet.
%
%   DATA = IMPORTFILE(FILE,SHEET,STARTROW,ENDROW) reads from the specified
%   worksheet for the specified row interval(s). Specify STARTROW and
%   ENDROW as a pair of scalars or vectors of matching size for
%   dis-contiguous row intervals. To read to the end of the file specify an
%   ENDROW of inf.
%
%	Non-numeric cells are replaced with: NaN
%
% Example:
%   BloombergDataCocosS1 =
%   importfile('20161121BloombergDataCocos.xlsx','CoCo Bonds',2,379);
%
%   See also XLSREAD.

% Auto-generated by MATLAB on 2016/11/21 23:31:03

%% Input handling

% If no sheet is specified, read first sheet
if nargin == 1 || isempty(sheetName)
    sheetName = 2;
end

% If row start and end points are not specified, define defaults
if nargin <= 3
    startRow = 2;
    endRow = 379;
end

%% Import the data
[~, ~, raw] = xlsread(workbookFile, sheetName, sprintf('A%d:AA%d',startRow(1),endRow(1)));
for block=2:length(startRow)
    [~, ~, tmpRawBlock] = xlsread(workbookFile, sheetName, sprintf('A%d:AA%d',startRow(block),endRow(block)));
    raw = [raw;tmpRawBlock]; %#ok<AGROW>
end
raw(cellfun(@(x) ~isempty(x) && isnumeric(x) && isnan(x),raw)) = {''};
cellVectors = raw(:,[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,18,19,21,22,23,24,25,26]);
raw = raw(:,[17,20,27]);

%% Replace non-numeric cells with NaN
R = cellfun(@(x) ~isnumeric(x) && ~islogical(x),raw); % Find non-numeric cells
raw(R) = {NaN}; % Replace non-numeric cells

%% Create output variable
data = reshape([raw{:}],size(raw));

%% Create table
tableout = table;

%% Allocate imported array to column variable names
tableout.IssuerName = cellVectors(:,1);
tableout.Ticker = cellVectors(:,2);
tableout.BBTickerFirmBasedonCUSIP = cellVectors(:,3);
tableout.FirmISINpastevalue = cellVectors(:,4);
tableout.FirmISIN = cellVectors(:,5);
tableout.Ownership = cellVectors(:,6);
tableout.BBCoCoTickeroption1 = cellVectors(:,7);
tableout.BBCoCoTickeroption2 = cellVectors(:,8);
tableout.IssueDate = cellVectors(:,9);
tableout.FirstCallDate = cellVectors(:,10);
tableout.Maturity = cellVectors(:,11);
tableout.MaturityCorrected = cellVectors(:,12);
tableout.CUSIP = cellVectors(:,13);
tableout.CUSIPBBready = cellVectors(:,14);
tableout.ISIN = cellVectors(:,15);
tableout.SEDOL = cellVectors(:,16);
tableout.Cpn = data(:,1);
tableout.Curr = cellVectors(:,17);
tableout.CapTypeCoCoAction = cellVectors(:,18);
tableout.COCOTriggerLevel = data(:,2);
tableout.CapitalTriggerType = cellVectors(:,19);
tableout.CoCoInitialTrigger = cellVectors(:,20);
tableout.BaselIIIDesignation = cellVectors(:,21);
tableout.CoCoTriggerMet = cellVectors(:,22);
tableout.IsCapContingentCoco = cellVectors(:,23);
tableout.Exchanges = cellVectors(:,24);
tableout.AmountIssued = data(:,3);

% Fix missing values ISIN
IndexMissingValuesISIN = strcmpi(tableout.FirmISINpastevalue,'ActiveX VT_ERROR: ') | strcmpi(tableout.FirmISINpastevalue,'#N/A N/A');
tableout.FirmISINpastevalue(IndexMissingValuesISIN) = {'-'};