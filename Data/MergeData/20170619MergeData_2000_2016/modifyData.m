function Data = modifyData(Data)

% Only include banks in countries for which there is information on fiscal
% treatment
Data(isnan(Data.DummyFiscalTreatment),:)=[];

% % Ditch firms with more than ... missing observations
% minNumNoNMissing = 3;
% varToFilterOn = 'TotalAssets_EUR';
% Data = removeFirmsWithTooManyMissingObs(Data,minNumNoNMissing,varToFilterOn);

% Remove obs with missing value for both 'ImpairedLoansSlashGrossLoans','LoanLossResSlashGrossLoans'
Data(isnan(Data.ImpairedLoansSlashGrossLoans) & isnan(Data.LoanLossResSlashGrossLoans),:) = [];

% Remove obs with fewer than 3 longitudinal obs
DataTemp = varfun(@nansum, Data(:,{'BvDIDNumbers','TotalAssets_EUR'}),'GroupingVariables',{'BvDIDNumbers'});
BvDIDNumbersToRemove = DataTemp.BvDIDNumbers(DataTemp.GroupCount<=2);
Data.Remove = NaN(size(Data,1),1);
for b = BvDIDNumbersToRemove'
    Data.Remove(Data.BvDIDNumbers==b)=1;
end
Data(Data.Remove==1,:)=[];

% % Remove obs with missing LoanLossResSlashGrossLoans
% IndexNaN = isnan(Data.LoanLossResSlashGrossLoans) | isnan(Data.ImpairedLoansSlashGrossLoans);
% Data(IndexNaN,:) = [];

% Reformat the CountryISOcode variable
Data.CountryISOcode = categorical(Data.CountryISOcode);

% Create some additional variables
Data.LogTotalAssets_EUR = log(Data.TotalAssets_EUR);
Data.Leverage3 = (Data.TotalLiabilities_EUR)./Data.TotalAssets_EUR;
Data.Leverage2 = (Data.TotalAssets_EUR-Data.TotalLiabilities_EUR)./Data.TotalAssets_EUR;
Data.Leverage = Data.TotalAssets_EUR./(Data.TotalAssets_EUR-Data.TotalLiabilities_EUR); Data.Leverage(isinf(Data.Leverage))=NaN;Data.Leverage(Data.Leverage<=0)=NaN;
Data.NetInterestIncome_EUR(Data.NetInterestIncome_EUR<=0)=NaN;
Data.LogNetInterestIncome_EUR = log(Data.NetInterestIncome_EUR);
Data.LoanLossProvSlashGrossLoans = Data.LoanLossProvisions_EUR ./ Data.GrossLoans1_EUR;
try
    Data.CashAndDueFromBanksOverTotalAssets = Data.CashandDueFromBanks_EUR./Data.TotalAssets_EUR;
    Data.LogCashandDueFromBanks_EUR = log(Data.CashandDueFromBanks_EUR);
    Data.LogCashandDueFromBanks_EUR(isinf(Data.LogCashandDueFromBanks_EUR))=NaN;
catch
    
end
Data.DummyBaselIII = Data.FiscalYear>=2011+0;
Data.DummyImplementationEU = Data.FiscalYear>=2013;
Data.DummyInteractionBaselIIIFiscalTreatment = Data.DummyBaselIII.*Data.DummyFiscalTreatment;
Data.DummyInteractionImplementationEUFiscalTreatment = Data.DummyImplementationEU.*Data.DummyFiscalTreatment;

% Process TaxesSlashPreDashtaxProfit 
Data.TaxesSlashPreDashtaxProfit = Data.TaxesSlashPreDashtaxProfit/100;

% Create equity-to-assets ration
Data.EquityToAssetsRatio = Data.Equity1_EUR./Data.TotalAssets_EUR;

% Winsorize and lag variables
Data = sortrows(Data,'FiscalYear','ascend');
Data = sortrows(Data,'BvDIDNumbers','ascend');
IndexSetToNaN = logical([1;Data.BvDIDNumbers(2:end)~=Data.BvDIDNumbers(1:end-1)]);
for v = {'ImpairedLoansSlashGrossLoans','LoanLossResSlashGrossLoans','LogTotalAssets_EUR','Leverage3','TaxesSlashPreDashtaxProfit','ReturnOnAvgAssetsROAA','AnnualVol_pct','EquityToAssetsRatio'}
    Data.(strcat(v{1},'_wp1p99')) = winsor(Data.(v{1}), [1 99]);
    Data.(strcat(v{1},'_wp1p99_Lag1')) = [NaN;Data.(strcat(v{1},'_wp1p99'))(1:end-1)];
    Data.(strcat(v{1},'_wp1p99_Lag1'))(IndexSetToNaN) = NaN;
end

% Add interaction of fiscaltreatment and lagged profitability
Data.InteractionFiscalTreatmentAndTax = Data.TaxesSlashPreDashtaxProfit_wp1p99_Lag1.*Data.DummyFiscalTreatment;


end