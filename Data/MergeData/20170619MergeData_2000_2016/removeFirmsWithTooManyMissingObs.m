function Data = removeFirmsWithTooManyMissingObs(Data,minNumNoNMissing,varToFilterOn)

DataUnstacked = unstack(Data(:,{varToFilterOn,'BvDIDNumbers','FiscalYear'}),varToFilterOn,'BvDIDNumbers');

IndexFirmsToRemove = sum(~isnan(table2array(DataUnstacked)))<minNumNoNMissing;
FirmsToRemove = DataUnstacked.Properties.VariableNames(IndexFirmsToRemove);

Data.Remove = zeros(size(Data,1),1);
for f = FirmsToRemove
    IndexF = Data.BvDIDNumbers==f;
    Data.Remove(IndexF) = true;
end
Data(logical(Data.Remove),:)=[];




end