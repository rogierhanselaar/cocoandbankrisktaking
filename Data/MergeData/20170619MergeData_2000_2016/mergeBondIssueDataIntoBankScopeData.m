function [Data,DataBondIssues] = mergeBondIssueDataIntoBankScopeData(Data,DataBondIssues,NameBondIssueVar)

% Sort the data
Data = sortrows(Data,'CloseDateDN','ascend');
Data = sortrows(Data,'BvDIDNumbers','ascend');

% For each firm in the bond issue data, find the corresponding firm in
% the BankScope data based on ISIN, and set the indicator BondIssued to 1
MatcVars.Bond = 'ISIN';
MatcVars.BS = 'ISINNumber';
[Data,DataBondIssues,~] = matchBondIssuesToBanksBasedOnISIN(Data,DataBondIssues,MatcVars,NameBondIssueVar);


end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [Data,DataBondIssues,IdentifierNotInBS] = matchBondIssuesToBanksBasedOnISIN(Data,DataBondIssues,MatcVars,NameBondIssueVar)
% Each bond has a unique ticker, so I will use tickers to identify individual
% bond issues. I will only use those bond issues that have a firm ISIN
DataBondIssues(strcmpi(DataBondIssues.ISIN,'#N/A N/A'),:)=[];
DataBondIssues(strcmpi(DataBondIssues.ISIN,'#N/A Invalid Security'),:)=[];
DataBondIssues(strcmpi(DataBondIssues.ISIN,'#N/A Limit: Monthly'),:)=[];
BondIssues = categorical(unique(DataBondIssues.Bondidentifier));

% Intialize countvariables
countBondIssuesNotMatchedButIdentifierMatched=0; 
countBondIssuesMatched=0; 
IdentifierNotInBS = categorical({});

% Initialize a variable showing which BondIssues are not yet matched
DataBondIssues.Matched = zeros(size(DataBondIssues,1),1);

% Initialize variables of interest
Data.(NameBondIssueVar{1})=zeros(size(Data,1),1);
Data.(strcat(NameBondIssueVar{1},'AmountIssued_EUR'))=zeros(size(Data,1),1);

% Loop over BondIssues; match each BondIssue to its corresponding bank
for BondIssue = BondIssues'
    % Info on BondIssue
    IndexBondIssue = DataBondIssues.Bondidentifier==BondIssue;
    IssueDateDN = DataBondIssues.IssueDateDN(IndexBondIssue);
    
    % Info on Corresponding firm
    FirmIdentifier = DataBondIssues.(MatcVars.Bond)(IndexBondIssue);
    IndexFirmIdentifierInBS = Data.(MatcVars.BS)==FirmIdentifier;
    
    % Set variable Data.(NameBondIssueVar{1}) and set counters
    LocFirmAndDateRangeMatch = find(Data.CloseDateDN>IssueDateDN & IndexFirmIdentifierInBS,1,'first'); 
    if size(LocFirmAndDateRangeMatch,1)~=0 
        Data.(NameBondIssueVar{1})(LocFirmAndDateRangeMatch)= Data.(NameBondIssueVar{1})(LocFirmAndDateRangeMatch)+1;
        Data.(strcat(NameBondIssueVar{1},'AmountIssued_EUR'))(LocFirmAndDateRangeMatch)= Data.(strcat(NameBondIssueVar{1},'AmountIssued_EUR'))(LocFirmAndDateRangeMatch)+DataBondIssues.Amountissued_EUR(IndexBondIssue);
        countBondIssuesMatched = countBondIssuesMatched+1;
        DataBondIssues.Matched = DataBondIssues.Matched | IndexBondIssue;
    else
        if sum(IndexFirmIdentifierInBS)>0
            countBondIssuesNotMatchedButIdentifierMatched = countBondIssuesNotMatchedButIdentifierMatched+1;
        else
            IdentifierNotInBS = [IdentifierNotInBS;FirmIdentifier];
        end
    end
    
end

end