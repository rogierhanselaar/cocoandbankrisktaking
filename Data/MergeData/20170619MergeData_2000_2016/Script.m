clear
%% Load data
pcinfo=java.net.InetAddress.getLocalHost;
if ~strcmpi(pcinfo.getHostName,'LAPTOP-6P6G4FB7')
    pathname = 'C:\Users\54167rha\Dropbox\PhD\Data\ProjectsWithProfPurnanandam_RogierHanselaar\CoCosAndRiskTaking\Data\';
else
    pathname = 'C:\Users\rogie\Dropbox\PhD\Data\ProjectsWithProfPurnanandam_RogierHanselaar\CoCosAndRiskTaking\Data\';
end

% Load processed BankScope data
load(strcat(pathname,'BankScopeData\20170728ProcessedBankScopeDataUnconsolidated2000_2016.mat'));
load(strcat(pathname,'BankScopeData\20170920ProcessedBankScopeDataConsolidated2000_2016.mat'));
load(strcat(pathname,'BankScopeData\ProcessedBankScopeData.mat'));
DataOld = Data;clear Data;
% Prepare data tables
DataU = prepareDataTables(DataU);
DataC = prepareDataTables(DataC);
DataOld = prepareDataTables(DataOld);

% Load additional financial data from SNL
load(strcat(pathname,'\SNLData\Financials\ProcessedDataISIN.mat'));SNLFinancialDataISIN = formattedDataISIN;formattedDataISIN = [];
load(strcat(pathname,'\SNLData\Financials\ProcessedDataSWIFT.mat'));SNLFinancialDataSWIFT = formattedDataSWIFT;formattedDataSWIFT = [];
% All amounts are already downloaded as euro's 

% Load CoCo issue data
DataCoCoIssue = importCoCoIssueData(strcat(pathname,'BloombergData\Issues\20161121BloombergDataCocos.xlsx'));
addpath('C:\Users\rogie\Git\cocoandbankrisktaking\Data')
FXData = loadFXData();
[DataCoCoIssue] = convertCurrenciesToEUR(DataCoCoIssue,FXData,{'AmountIssued'});
% Match firm BvDIDNumbers to cocos that do not have a firm isin, or cocos
% that have a non-matching firm isin
DataCoCoIssue = addFirmBvDIDNumbers(DataCoCoIssue);
% Make both ISINs and BvDIDNumbers categorical variables
DataCoCoIssue.FirmISIN = categorical(DataCoCoIssue.FirmISIN);
DataCoCoIssue.BvDIDNumber = categorical(DataCoCoIssue.BvDIDNumber);
DataCoCoIssue.CUSIP = categorical(DataCoCoIssue.CUSIP);

% Load SDC Equity issues data
DataEquityIssuesOld = readtable('C:\Users\rogie\Dropbox\PhD\Data\ProjectsWithProfPurnanandam_RogierHanselaar\CoCosAndRiskTaking\Data\SDCData\FollowOnData\ProcessedDataFOs.csv');
dataTemp = load('C:\Users\rogie\Dropbox\PhD\Data\ProjectsWithProfPurnanandam_RogierHanselaar\CoCosAndRiskTaking\Data\SDCData\FollowOnData\20170621ProcessedDataFOs.mat');
DataEquityIssues = dataTemp.Data; 
clear dataTemp;
DataEquityIssues.ISIN = categorical(DataEquityIssues.ISIN);
DataEquityIssues.DealNumber = categorical(DataEquityIssues.DealNumber);
DataEquityIssues.IssueDateDN = DataEquityIssues.IssueDateDATENUM;
DataEquityIssues.AmountIssued_EUR = DataEquityIssues.OfferPrice_EUR.*DataEquityIssues.PrimarySharesOfferedinthisMkt;
DataEquityIssues(isnan(DataEquityIssues.AmountIssued_EUR),:)=[];

% Load bullit bond issue data
DataBondIssues = readtable('C:\Users\rogie\Dropbox\PhD\Data\ProjectsWithProfPurnanandam_RogierHanselaar\CoCosAndRiskTaking\Data\BloombergData\BulletBondIssues\20170815BankBondsWesternEurope2009_2016_Processed.csv');
DataBondIssues(strcmpi(DataBondIssues.FirmISIN,'#N/A N/A'),:)=[];
DataBondIssues(strcmpi(DataBondIssues.FirmISIN,'#N/A Invalid Security'),:)=[];
DataBondIssues(strcmpi(DataBondIssues.FirmISIN,'#N/A Limit: Monthly'),:)=[];
DataBondIssues.Bondidentifier = categorical(DataBondIssues.Bondidentifier);
DataBondIssues.Properties.VariableNames('FirmISIN') = {'ISIN'};

% Load Datastream data
DataDS = readtable(strcat(pathname,'DatastreamData\ProcessedDatastreamData.csv'));


%% Merge data
% DataU = mergeData(DataU,DataCoCoIssue,DataEquityIssues,DataDS);
DataC = mergeData(DataC,DataCoCoIssue,DataEquityIssues,DataBondIssues,DataDS,SNLFinancialDataISIN);
% DataOld = mergeData(DataOld,DataCoCoIssue,DataEquityIssues,DataDS);

%% Make some final modifications and additions
DataC = modifyData(DataC);

%% Write data away
todaysDate = year(datetime('today'))*10000+month(datetime('today'))*100+day(datetime('today'));
% save(strcat(pathname,num2str(todaysDate),'MergedAndProcessedDataUnconsolidated2000_2016.mat'),'DataU');
save(strcat(pathname,num2str(todaysDate),'MergedAndProcessedDataConsolidated2000_2016.mat'),'DataC');
% writetable(DataU,strcat(pathname,num2str(todaysDate),'MergedAndProcessedDataUnconsolidated2000_2016.csv'))
% writetable(DataC,strcat(pathname,num2str(todaysDate),'MergedAndProcessedDataConsolidated2000_2016.csv'))



%% Other Notes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Some notes on the CoCo issue data:
% - The amount of CoCos issued per bank varies from ... to ..., with one
% exception: Lloyds Banking Group issued 52(!) CoCos in 2009 for a total of
% 4.6 bilion euros, 13.9 bilion pounds, 0.4 bilion Japanes Yen, and 7.1
% bilion US dollar. According to wikipedia, Lloyds was at that time for
% 40-50% in the hands of the government as a result of government
% interventions due to the financial crisis and apparently had to choose
% between more state aid (Asset Protection Scheme) or some other radical
% move.
% 
% Some notes on the matching results:
% - We have matched 216 CoCos, issued by 63 companies, in 16 countries.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% To select all banks that issued CoCos in a particular country:
% Data(strcmpi(Data.CountryName,'Italy') & Data.CoCoIssued>0,{'NAME','YearpartofCLOSDATE','CoCoIssued'});