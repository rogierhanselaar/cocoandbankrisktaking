function Data = createCumulativeCoCoIssuance(Data,varNameCoCoIssuance)
% Calculate the cumulative number of cocos issued per firm per year
DataUnstacked = unstack(Data(:,{'BvDIDNumbers','YearpartofCLOSDATE',varNameCoCoIssuance}),varNameCoCoIssuance,'YearpartofCLOSDATE');
DataUnstackedArray = table2array(DataUnstacked(:,2:end));
IndexNaN = isnan(DataUnstackedArray);
DataUnstackedArray(IndexNaN) = 0;
DataUnstackedCumSum = cumsum(DataUnstackedArray,2);
DataUnstackedCumSum(IndexNaN)=NaN;

% Link it back to the BvDIDNumbers
count=0;
Years = unique(Data.YearpartofCLOSDATE);
Vars = strcat('x',cellstr(num2str(Years)))';
for var = Vars
    count=count+1;
    DataUnstacked.(var{1}) = DataUnstackedCumSum(:,count);
end

% Stack the unstacked data again and remove rows with missing values
% generated due to the unstacking and stacking
DataRestacked = stack(DataUnstacked,Vars);
DataRestacked.Properties.VariableNames(2) = {'YearpartofCLOSDATE'};
DataRestacked.Properties.VariableNames(3) = {strcat(varNameCoCoIssuance,'_cumsum')};
DataRestacked(isnan(DataRestacked.(strcat(varNameCoCoIssuance,'_cumsum'))),:)=[];

% Merge back into Data (it is actually not necessary to merge, but doesn't
% harm)
oldCats = categories(DataRestacked.YearpartofCLOSDATE);
newCats = cellstr(num2str(Years));
DataRestacked.YearpartofCLOSDATE = renamecats(DataRestacked.YearpartofCLOSDATE,oldCats,newCats);
DataRestacked.YearpartofCLOSDATE = cell2mat(cellfun(@str2double,cellstr(DataRestacked.YearpartofCLOSDATE),'UniformOutput',false));
Data = outerjoin(Data,DataRestacked,...
    'Type','Left',...
    'LeftKeys',{'BvDIDNumbers','YearpartofCLOSDATE'},...
    'RightKeys',{'BvDIDNumbers','YearpartofCLOSDATE'},...
    'RightVariables',{strcat(varNameCoCoIssuance,'_cumsum')});


end