function [Data] = getTriggerInfo(Data,DataCoCoIssue)

% Initialize table
LinkingTableFirmYearCoCo = table();
LinkingTableFirmYearCoCo.BvDIDNumbers = categorical();
tempMat = []; LinkingTableFirmYearCoCo.FiscalYear = tempMat;
LinkingTableFirmYearCoCo.CoCoCusip = categorical();
LinkingTableFirmYearCoCo.CoCoTriggerLevel = tempMat;

% For each firm in the CoCo data, find the corresponding firm in the
% BankScope data based on ISIN, and set the indicator CoCoIssued to 1
MatcVars.CoCo = 'FirmISIN';
MatcVars.BS = 'ISINNumber';
[LinkingTableFirmYearCoCo,DataCoCoIssue,FirmIdentifierNotMatchedISIN] = getMinMaxMedianAndMean(Data,DataCoCoIssue,MatcVars,LinkingTableFirmYearCoCo);

% For each firm in the CoCo data, find the corresponding firm in the BS
% data based on BvDIDNumber, and set the indicator CoCoIssued to 1 
MatcVars.CoCo = 'BvDIDNumber';
MatcVars.BS = 'BvDIDNumbers';
[LinkingTableFirmYearCoCo,DataCoCoIssue,FirmIdentifierNotMatchedManual] = getMinMaxMedianAndMean(Data,DataCoCoIssue,MatcVars,LinkingTableFirmYearCoCo);

% Generate summary statistics
funcs = {@nanmin,@nanmax,@nanmedian,@nanmean};
func_descr = {'min','max','median','mean'};
Data = generateSumStats(Data,LinkingTableFirmYearCoCo,funcs, func_descr);

end

function [LinkingTableFirmYearCoCo,DataCoCoIssue,IdentifierNotInBS] = getMinMaxMedianAndMean(Data,DataCoCoIssue,MatcVars,LinkingTableFirmYearCoCo)
% Each coco has a unique CUSIP, so I will use CUSIPs to identify individual
% CoCos. I will only use those CoCos that have a firm ISIN
CoCosWithMissingIdentifier = DataCoCoIssue.(MatcVars.CoCo)==categorical(cellstr('#N/A N/A'))...
    | DataCoCoIssue.(MatcVars.CoCo)==categorical(cellstr('ActiveX VT_ERROR: '))...
    | isundefined(DataCoCoIssue.(MatcVars.CoCo));
if sum(strcmpi(DataCoCoIssue.Properties.VariableNames,'Matched'))==1
    CoCos = categorical(unique(DataCoCoIssue.CUSIP(~CoCosWithMissingIdentifier & ~DataCoCoIssue.Matched)));
else
    CoCos = categorical(unique(DataCoCoIssue.CUSIP(~CoCosWithMissingIdentifier)));
end

% Intialize countvariables
countCoCosNotMatched = sum(CoCosWithMissingIdentifier); 
countCoCosNotMatchedButIdentifierMatched=0; 
countCoCosMatched=0; 
IdentifierNotInBS = categorical({});

% Initialize a variable showing which CoCos are not yet matched
if sum(strcmpi(DataCoCoIssue.Properties.VariableNames,'Matched'))~=1
    DataCoCoIssue.Matched = zeros(size(DataCoCoIssue,1),1);
end

% Loop over CoCos; match each coco to its corresponding bank and year
for CoCo = CoCos'
    % Info on CoCo
    IndexCoCo = DataCoCoIssue.CUSIP==CoCo;
    IssueDateDN = DataCoCoIssue.IssueDateDN(IndexCoCo);
    
    % Info on Corresponding firm
    FirmIdentifier = DataCoCoIssue.(MatcVars.CoCo)(IndexCoCo);
    IndexFirmIdentifierInBS = Data.(MatcVars.BS)==FirmIdentifier;
    
    % Set variable Data.(NameCoCoVar{1}) and set counters
    LocFirmAndDateRangeMatch = find(Data.CloseDateDN>IssueDateDN & IndexFirmIdentifierInBS,1,'first');
    if size(LocFirmAndDateRangeMatch,1)~=0 
        TempTable = table(Data.BvDIDNumbers(LocFirmAndDateRangeMatch),...
            Data.FiscalYear(LocFirmAndDateRangeMatch),...
            DataCoCoIssue.CUSIP(IndexCoCo),...
            DataCoCoIssue.COCOTriggerLevel(IndexCoCo),...
            'VariableNames',{'BvDIDNumbers','FiscalYear','CoCoCusip','CoCoTriggerLevel'});
        LinkingTableFirmYearCoCo = [LinkingTableFirmYearCoCo;TempTable];
        
        countCoCosMatched = countCoCosMatched+1;
        DataCoCoIssue.Matched = DataCoCoIssue.Matched | IndexCoCo;
    else
        if sum(IndexFirmIdentifierInBS)>0
            countCoCosNotMatchedButIdentifierMatched = countCoCosNotMatchedButIdentifierMatched+1;
        else
            countCoCosNotMatched = countCoCosNotMatched+1;
            IdentifierNotInBS = [IdentifierNotInBS;FirmIdentifier];
        end
    end
    
end



end

function Data = generateSumStats(Data,LinkingTableFirmYearCoCo,funcs, func_descr)

count = 1;
for f = funcs
    results = varfun(f{1},LinkingTableFirmYearCoCo(:,{'BvDIDNumbers','FiscalYear','CoCoTriggerLevel'}),'GroupingVariables',{'BvDIDNumbers','FiscalYear'});
    results.Properties.VariableNames{end} = strcat('CoCoTriggerLevel_',func_descr{count});
    Data = outerjoin(Data,results,...
        'Type','Left',...
        'LeftKeys',{'BvDIDNumbers','FiscalYear'},...
        'RightKeys',{'BvDIDNumbers','FiscalYear'},...
        'RightVariables',strcat('CoCoTriggerLevel_',func_descr{count}));
    count = count+1;
end



end