function SNLData = makeVariableNamesMatch(SNLData)

% Load link between variable names in Data and SNLData
load('VariableNameLink')

% Rename variables in SNLData and make them of similar type
for v = VariableNameLink.SNLName'
    IndexV = strcmpi(VariableNameLink.SNLName,v);
    if sum(strcmpi(SNLData.Properties.VariableNames,v))>0
        SNLData.Properties.VariableNames(v{1}) = strcat(VariableNameLink.BSName(IndexV),'_SNLData');
    end
end
SNLData.FiscalYear_SNLData = cellfun(@(x) str2double(x(1:4)),SNLData.FiscalYear_SNLData);
SNLData.TotalDebt=[];

end