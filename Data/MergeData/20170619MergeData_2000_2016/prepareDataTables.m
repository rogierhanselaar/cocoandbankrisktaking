function Data = prepareDataTables(Data)

% Create fiscal year variable
Data.FiscalYear=NaN(size(Data,1),1);
Data.FiscalYearMonth = month(Data.CloseDateDN);
Data.FiscalYear(Data.FiscalYearMonth>=7) = year(Data.CloseDateDN(Data.FiscalYearMonth>=7));
Data.FiscalYear(Data.FiscalYearMonth<7) = year(Data.CloseDateDN(Data.FiscalYearMonth<7))-1;

% Make ISIN and BvDIDNumbers categorical
Data.ISINNumber= categorical(Data.ISINNumber);
Data.BvDIDNumbers = categorical(Data.BvDIDNumbers);



end