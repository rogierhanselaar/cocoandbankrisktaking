function [Data,DataPointsGained] = plugHolesWithSNLData(Data,SNLData,DataPointsGained)

% Initialize identifier
identifier_SNL = SNLData.Properties.VariableNames{1};
SNLData.(identifier_SNL) = categorical(SNLData.(identifier_SNL));
identifier_Data = SNLData.Properties.VariableNames{1}(1:end-8);
identifier = identifier_Data;

% Merge the SNLData into Data using a full outer join (creates extra rows
% for identifier-fiscalyear combinations in SNLData not in Data) 
Data = outerjoin(Data,SNLData,...
    'Type','full',...
    'LeftKeys',{identifier_Data,'FiscalYear'},...
    'RightKeys',{identifier_SNL,'FiscalYear_SNLData'},...
    'RightVariables',SNLData.Properties.VariableNames);
% fill up identifier and fiscalyear
IndexNewRows = ~isundefined(Data.(identifier_SNL)) & isundefined(Data.(identifier_Data));
Data.(identifier_Data)(IndexNewRows) = Data.(identifier_SNL)(IndexNewRows);
Data.FiscalYear(IndexNewRows) = Data.FiscalYear_SNLData(IndexNewRows);

% Check for which firms Data and SNLData are close in terms of TA, by
% calculating whether the average percentage difference in TA is smaller
% than 5% for those observations for which there are no missing TA values
% in both datasets
Data.PercentageDifferenceInTA = abs(Data.TotalAssets_EUR - Data.TotalAssets_EUR_SNLData)./Data.TotalAssets_EUR;
IdentifiersCloseInTermsOfTA = varfun(@nanmean,Data(:,{'ISINNumber','PercentageDifferenceInTA'}),'GroupingVariables',{'ISINNumber'});
IdentifiersCloseInTermsOfTA(isnan(IdentifiersCloseInTermsOfTA.nanmean_PercentageDifferenceInTA),:)=[];
IdentifiersCloseInTermsOfTA(IdentifiersCloseInTermsOfTA.nanmean_PercentageDifferenceInTA>0.05,:)=[];

% Fill up the holes
for i = unique(IdentifiersCloseInTermsOfTA.ISINNumber)' % For the firms close in terms of TA
    IndexI = Data.(identifier_Data)==i;
    
    for varName_SNL = SNLData.Properties.VariableNames(3:end) % For each variable
        varName_Data = varName_SNL{1}(1:end-8);
        
        % Create index potential replacement: missing values in Data &
        % non-missing values in SNLData.
        IndexPotentialReplacement = isnan(Data.(varName_Data)) & ~isnan(Data.(varName_SNL{1}));
        
        % Check closeness of variable (difference < 5%)        
        AvgPercentageDifference = nanmean(abs(Data.(varName_Data)(IndexI) - Data.(varName_SNL{1})(IndexI))./Data.(varName_Data)(IndexI));
        
        if AvgPercentageDifference<0.05
            % Copy values of SNLData into Data
            Data.(varName_Data)(IndexI & IndexPotentialReplacement) = Data.(varName_SNL{1})(IndexI & IndexPotentialReplacement);
            
            % Calculate datapoints gained
            DataPointsGained.N_pointsGained(strcmpi(DataPointsGained.VarNames,varName_SNL{1})) =...
                DataPointsGained.N_pointsGained(strcmpi(DataPointsGained.VarNames,varName_SNL{1})) + sum(IndexI & IndexPotentialReplacement);
        end
    end
end

% Clean up variablenames
for varName_SNL = SNLData.Properties.VariableNames
    varName = varName_SNL{1}(1:end-8);
    Data.Properties.VariableNames(strcmpi(Data.Properties.VariableNames,varName_SNL)) = {strcat(varName,'_SNLData_',identifier)};
end

% Set CloseDate and CloseDateDN for observations that where added, by using
% closing months and days from surrounding observations
Data = sortrows(Data,'FiscalYear','ascend');
Data = sortrows(Data,(identifier),'ascend');
Data.ClosingMonth = month(Data.CloseDateDN);
Data.ClosingDay = day(Data.CloseDateDN);
Data.IndexMissingClosingDate = isnan(Data.ClosingDate);
IndexMissingClosingDate = Data.IndexMissingClosingDate;
N_missingClosingDates = NaN;
while sum(Data.IndexMissingClosingDate)~=N_missingClosingDates
    N_missingClosingDates = sum(Data.IndexMissingClosingDate);
    Data.Index1AboveMissingClosingDate = [Data.IndexMissingClosingDate(2:end); false];
    Data.IndexSameIdentifierAbove = [false;Data.(identifier)(2:end)==Data.(identifier)(1:end-1)];
    Data.IndexSameIdentifierBelow = [Data.(identifier)(1:end-1)==Data.(identifier)(2:end); false];
    
    Data.ClosingMonth(Data.IndexMissingClosingDate & Data.IndexSameIdentifierAbove) = ...
        Data.ClosingMonth(Data.Index1AboveMissingClosingDate & Data.IndexSameIdentifierBelow);
    Data.ClosingDay(Data.IndexMissingClosingDate & Data.IndexSameIdentifierAbove) = ...
        Data.ClosingDay(Data.Index1AboveMissingClosingDate & Data.IndexSameIdentifierBelow);
    Data.ClosingDate(Data.IndexMissingClosingDate) = ...
        Data.FiscalYear(Data.IndexMissingClosingDate)*10000 + ...
        Data.ClosingMonth(Data.IndexMissingClosingDate)*100 + ...
        Data.ClosingDay(Data.IndexMissingClosingDate);
    
    % also copy BvDIDNumbers
    Data.BvDIDNumbers(Data.IndexMissingClosingDate & Data.IndexSameIdentifierAbove) = ...
        Data.BvDIDNumbers(Data.Index1AboveMissingClosingDate & Data.IndexSameIdentifierBelow);
    
    Data.IndexMissingClosingDate = isnan(Data.ClosingDate);
end

N_missingClosingDates = NaN;
while sum(Data.IndexMissingClosingDate)~=N_missingClosingDates
    N_missingClosingDates = sum(Data.IndexMissingClosingDate);
    Data.Index1BelowMissingClosingDate = [false; Data.IndexMissingClosingDate(1:end-1)];
    Data.IndexSameIdentifierAbove = [false;Data.(identifier)(2:end)==Data.(identifier)(1:end-1)];
    Data.IndexSameIdentifierBelow = [Data.(identifier)(1:end-1)==Data.(identifier)(2:end); false];
    
    Data.ClosingMonth(Data.IndexMissingClosingDate & Data.IndexSameIdentifierBelow) = ...
        Data.ClosingMonth(Data.Index1BelowMissingClosingDate & Data.IndexSameIdentifierAbove);
    Data.ClosingDay(Data.IndexMissingClosingDate & Data.IndexSameIdentifierBelow) = ...
        Data.ClosingDay(Data.Index1BelowMissingClosingDate & Data.IndexSameIdentifierAbove);
    Data.ClosingDate(Data.IndexMissingClosingDate) = ...
        Data.FiscalYear(Data.IndexMissingClosingDate)*10000 + ...
        Data.ClosingMonth(Data.IndexMissingClosingDate)*100 + ...
        Data.ClosingDay(Data.IndexMissingClosingDate);
    
    % also copy BvDIDNumbers
    Data.BvDIDNumbers(Data.IndexMissingClosingDate & Data.IndexSameIdentifierBelow) = ...
        Data.BvDIDNumbers(Data.Index1BelowMissingClosingDate & Data.IndexSameIdentifierAbove);
    
    Data.IndexMissingClosingDate = isnan(Data.ClosingDate);
end


% Set CloseDateDN
Data.CloseDateDN(IndexMissingClosingDate) = ...
    datenum(Data.FiscalYear(IndexMissingClosingDate),...
    Data.ClosingMonth(IndexMissingClosingDate),...
    Data.ClosingDay(IndexMissingClosingDate));

% Set YearpartofCLOSDATE
Data.YearpartofCLOSDATE(IndexMissingClosingDate) = year(Data.CloseDateDN(IndexMissingClosingDate));

% Remove now redundant variables
Data.IndexMissingClosingDate = [];
Data.Index1BelowMissingClosingDate = [];
Data.IndexSameIdentifierAbove = [];
Data.IndexSameIdentifierBelow =[];
Data.Index1AboveMissingClosingDate=[];
Data.ClosingMonth = [];
Data.ClosingDay = [];

% Remove firms that were orininally not in Data
Data(Data.ISINNumber=='DE0006223407' | Data.ISINNumber=='DK0060854669' | Data.ISINNumber=='DK0060670776',:)=[];

end