function Data = mergeData(Data,DataCoCoIssue,DataEquityIssues,DataBondIssues,DataDS,SNLFinancialDataISIN)

%% Use SNL financial data to plug holes where necessary
SNLFinancialDataISIN = makeVariableNamesMatch(SNLFinancialDataISIN);
DataPointsGained = array2table(SNLFinancialDataISIN.Properties.VariableNames(3:end)','VariableNames',{'VarNames'});
DataPointsGained.N_pointsGained = zeros(size(DataPointsGained,1),1);
[Data,DataPointsGained] = plugHolesWithSNLData(Data,SNLFinancialDataISIN,DataPointsGained);


%% Merge CoCo issuance data
% Merge coco issue data into BankScope data, with as a result for each firm
% fiscal year combination the number of cocos, as well as
% the dollar amount of cocos that is issued.
[Data,DataCoCoIssueCoCoIssued]= mergeCoCoDataIntoBankScopeData(Data,DataCoCoIssue,{'CoCoIssued'});

[Data] = getTriggerInfo(Data,DataCoCoIssue);

% Merge coco issue data of cocos of the "equity conversion"-type into
% BankScope data, with as a result for each firm fiscal year combination
% the number of cocos, as well as the dollar amount of cocos that is issued.
IndexEquityConversionCoCo = strcmpi(DataCoCoIssue.CapTypeCoCoAction,'Equity Conversion');
[Data,DataCoCoIssueEquityConversion]= mergeCoCoDataIntoBankScopeData(Data,DataCoCoIssue(IndexEquityConversionCoCo,:),{'CoCoIssuedEquityConversionType'});

% Merge coco issue data of cocos of the "write-down"-type into
% BankScope data, with as a result for each firm fiscal year combination
% the number of cocos, as well as the dollar amount of cocos that is issued.
IndexWriteDownCoCo = strcmpi(DataCoCoIssue.CapTypeCoCoAction,'Permanent Write Down') |...
    strcmpi(DataCoCoIssue.CapTypeCoCoAction,'Partial Permanent Write Down') |...
    strcmpi(DataCoCoIssue.CapTypeCoCoAction,'Temporary Write Down');
[Data,DataCoCoIssueWriteDown]= mergeCoCoDataIntoBankScopeData(Data,DataCoCoIssue(IndexWriteDownCoCo,:),{'CoCoIssuedWriteDownType'});

% Merge coco issue data of cocos with trigger level above median into
% BankScope data, with as a result for each firm fiscal year combination
% the number of cocos, as well as the dollar amount of cocos that is issued.
IndexTriggerAboveMedianCoCo = DataCoCoIssue.COCOTriggerLevel>nanmedian(DataCoCoIssue.COCOTriggerLevel);
[Data,DataCoCoIssueTriggerAboveMed]= mergeCoCoDataIntoBankScopeData(Data,DataCoCoIssue(IndexTriggerAboveMedianCoCo,:),{'CoCoIssuedAboveMedianTrigger'});

% Merge coco issue data of cocos with trigger level below median into
% BankScope data, with as a result for each firm fiscal year combination
% the number of cocos, as well as the dollar amount of cocos that is issued.
IndexTriggerBelowMedianCoCo = DataCoCoIssue.COCOTriggerLevel<nanmedian(DataCoCoIssue.COCOTriggerLevel);
[Data,DataCoCoIssueTriggerBelowMed]= mergeCoCoDataIntoBankScopeData(Data,DataCoCoIssue(IndexTriggerBelowMedianCoCo,:),{'CoCoIssuedBelowMedianTrigger'});

% Merge coco issue data of cocos with trigger level at median into
% BankScope data, with as a result for each firm fiscal year combination
% the number of cocos, as well as the dollar amount of cocos that is issued.
IndexTriggerAtMedianCoCo = DataCoCoIssue.COCOTriggerLevel==nanmedian(DataCoCoIssue.COCOTriggerLevel);
[Data,DataCoCoIssueTriggerAtMed]= mergeCoCoDataIntoBankScopeData(Data,DataCoCoIssue(IndexTriggerAtMedianCoCo,:),{'CoCoIssuedAtMedianTrigger'});

% Merge coco issue data of cocos with trigger level at or below median into
% BankScope data, with as a result for each firm fiscal year combination
% the number of cocos, as well as the dollar amount of cocos that is issued.
IndexTriggerAtOrBelowMedianCoCo = DataCoCoIssue.COCOTriggerLevel<=nanmedian(DataCoCoIssue.COCOTriggerLevel);
[Data,DataCoCoIssueTriggerAtOrBelowMed]= mergeCoCoDataIntoBankScopeData(Data,DataCoCoIssue(IndexTriggerAtOrBelowMedianCoCo,:),{'CoCoIssuedAtOrBelowMedianTrigger'});

varNames = {'','EquityConversionType','WriteDownType','AboveMedianTrigger','BelowMedianTrigger','AtMedianTrigger','AtOrBelowMedianTrigger'};
for v = varNames
    % Create a variable that equals the cumulative number of cocos issued by a
    % firm for each firm year combination
    varnameCoCoIssued = strcat('CoCoIssued',v{1});
    Data = createCumulativeCoCoIssuance(Data,varnameCoCoIssued);
    
    % Create a dummy that indicates whether a firm has issued one or more CoCos
    Data.(strcat('DummyHasIssuedCoCo',v{1})) = Data.(strcat('CoCoIssued',v{1},'_cumsum'))>0;
    
    % Create a variable that equals the cumulative dollar amount of cocos
    % issued by a firm for each firm year combination
    varnameCoCoIssued = strcat('CoCoIssued',v{1},'AmountIssued_EUR');
    Data = createCumulativeCoCoIssuance(Data,varnameCoCoIssued);
end
% Create a variable that indicates whether interest payments on cocos can
% be deducted from income at a specific point in time in a specific country
Data = createFiscalTreatmentVariable(Data);

%% Merge the SDC Equity issuance data
[Data,~] = mergeEquityIssueDataIntoBankScopeData(Data,DataEquityIssues,{'EquityIssued'});
% Create a variable that equals the cumulative number of equity issues
% undertaken by a firm for each firm year combination
Data = createCumulativeCoCoIssuance(Data,'EquityIssued');

% Create a dummy that indicates whether a firm has undertaken one or more
% equity issues
Data.DummyHasIssuedEquity = Data.EquityIssued_cumsum>0;

% Create a variable that equals the cumulative dollar amount of equity
% issued by a firm for each firm year combination
Data = createCumulativeCoCoIssuance(Data,'EquityIssuedAmountIssued_EUR');


%% Merge the Bloomberg bullit bond issuance data
[Data,~] = mergeBondIssueDataIntoBankScopeData(Data,DataBondIssues,{'BondIssued'});
% Create a variable that equals the cumulative number of bond issues
% undertaken by a firm for each firm year combination
Data = createCumulativeCoCoIssuance(Data,'BondIssued');

% Create a dummy that indicates whether a firm has undertaken one or more
% bond issues
Data.DummyHasIssuedBond = Data.BondIssued_cumsum>0;

% Create a variable that equals the cumulative dollar amount of bond
% issued by a firm for each firm year combination
Data = createCumulativeCoCoIssuance(Data,'BondIssuedAmountIssued_EUR');



%% Merge Datastream data
DataDS.ISIN = categorical(DataDS.ISIN);
Data = outerjoin(Data,DataDS,...
    'Type','Left',...
    'LeftKeys',{'ISINNumber','FiscalYear'},...
    'RightKeys',{'ISIN','FiscalYear'},...
    'RightVariables','AnnualVol_pct');

%% Additional filters on the bankscope data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Lowest TA: 'Zuercher Kantonalbank Oesterreich AG' with 93 million EUR in
% Total Assets ---> Investigate further: It should be a bank with more than 150
% Billion CHF in total Assets 
% Note: overwegen Zuercher Kantonalbank Oesterreich AG te verwijderen op
% basis van het feit dat de CoCo issue een veelvoud is van de total assets;
% of overwegen alle government banks te identificeren en te verwijderen
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Remove 'Zuercher Kantonalbank Oesterreich AG' from the data
Data(Data.BvDIDNumbers==categorical({'AT38589'}),:)=[];

% Remove Lloyds from the Data
Data(Data.ISINNumber==categorical({'GB0008706128'}),:)=[];

% Find issuing firms
% Separate firms into those that have issued cocos and those that have not
FirmsThatIssuedCoCos = array2table(unique(Data.BvDIDNumbers(Data.CoCoIssued>0)),'VariableNames',{'BvDIDNumbers2'});
Data = outerjoin(Data,FirmsThatIssuedCoCos,...
    'Type','Left',...
    'LeftKeys',{'BvDIDNumbers'},...
    'RightKeys',{'BvDIDNumbers2'},...
    'RightVariables',{'BvDIDNumbers2'});
IndexNonIssuers = isundefined(Data.BvDIDNumbers2);
Data.DummyIsIssuer = ~IndexNonIssuers;
Data.BvDIDNumbers2=[];

% Keep only those non-issuers that are larger than the smallest issuer in
% terms of total assets in the period 2009-present (for the purpose of
% consistency with previous results.
varsToSelectOn = {'TotalAssets_EUR'};
Data = removeDeviatingNonIssuers(Data,varsToSelectOn);


end

function Data = removeDeviatingNonIssuers(Data,varsToSelectOn)
NonIssuersToBeRemoved = categorical();
IndexIssuer = Data.DummyIsIssuer;
IndexNonIssuer = ~Data.DummyIsIssuer;
for v = varsToSelectOn
    Minimum = min(Data.(v{1})(IndexIssuer & Data.YearpartofCLOSDATE>=2009)); 
    NonIssuersToBeRemoved = [NonIssuersToBeRemoved;unique(Data.BvDIDNumbers(Data.(v{1})<Minimum & IndexNonIssuer))];
end
IndexRemove = zeros(size(Data,1),1);
for i = 1:size(NonIssuersToBeRemoved,1)
    IndexRemove = IndexRemove | Data.BvDIDNumbers==NonIssuersToBeRemoved(i);
end
Data(IndexRemove,:)=[];
end
