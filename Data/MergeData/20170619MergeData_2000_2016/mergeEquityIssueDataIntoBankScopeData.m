function [Data,DataEquityIssues] = mergeEquityIssueDataIntoBankScopeData(Data,DataEquityIssues,NameEquityIssueVar)

% Sort the data
Data = sortrows(Data,'CloseDateDN','ascend');
Data = sortrows(Data,'BvDIDNumbers','ascend');

% For each firm in the equity issue data, find the corresponding firm in
% the BankScope data based on ISIN, and set the indicator EquityIssued to 1
MatcVars.Equity = 'ISIN';
MatcVars.BS = 'ISINNumber';
[Data,DataEquityIssues,~] = matchEquityIssuesToBanksBasedOnISIN(Data,DataEquityIssues,MatcVars,NameEquityIssueVar);


end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [Data,DataEquityIssues,IdentifierNotInBS] = matchEquityIssuesToBanksBasedOnISIN(Data,DataEquityIssues,MatcVars,NameEquityIssueVar)
% Each coco has a unique DealNumber, so I will use DealNumbers to identify individual
% Equiy issues. I will only use those Equity issues that have a firm ISIN
EquityIssuesWithMissingIdentifier = DataEquityIssues.(MatcVars.Equity)==categorical(cellstr('#N/A N/A'))...
    | DataEquityIssues.(MatcVars.Equity)==categorical(cellstr('ActiveX VT_ERROR: '))...
    | isundefined(DataEquityIssues.(MatcVars.Equity));
if sum(strcmpi(DataEquityIssues.Properties.VariableNames,'Matched'))==1
    EquityIssues = categorical(unique(DataEquityIssues.DealNumber(~EquityIssuesWithMissingIdentifier & ~DataEquityIssues.Matched)));
else
    EquityIssues = categorical(unique(DataEquityIssues.DealNumber(~EquityIssuesWithMissingIdentifier)));
end

% Intialize countvariables
countEquityIssuesNotMatched = sum(EquityIssuesWithMissingIdentifier); 
countEquityIssuesNotMatchedButIdentifierMatched=0; 
countEquityIssuesMatched=0; 
IdentifierNotInBS = categorical({});

% Initialize a variable showing which EquityIssues are not yet matched
if sum(strcmpi(DataEquityIssues.Properties.VariableNames,'Matched'))~=1
    DataEquityIssues.Matched = zeros(size(DataEquityIssues,1),1);
end

% Initialize variables of interest
if sum(strcmpi(Data.Properties.VariableNames,NameEquityIssueVar{1}))~=1
    Data.(NameEquityIssueVar{1})=zeros(size(Data,1),1);
    Data.(strcat(NameEquityIssueVar{1},'AmountIssued_EUR'))=zeros(size(Data,1),1);
end

% Loop over EquityIssues; match each EquityIssue to its corresponding bank
for EquityIssue = EquityIssues'
    % Info on EquityIssue
    IndexEquityIssue = DataEquityIssues.DealNumber==EquityIssue;
    IssueDateDN = DataEquityIssues.IssueDateDN(IndexEquityIssue);
    
    % Info on Corresponding firm
    FirmIdentifier = DataEquityIssues.(MatcVars.Equity)(IndexEquityIssue);
    IndexFirmIdentifierInBS = Data.(MatcVars.BS)==FirmIdentifier;
    
    % Set variable Data.(NameEquityIssueVar{1}) and set counters
    LocFirmAndDateRangeMatch = find(Data.CloseDateDN>IssueDateDN & IndexFirmIdentifierInBS,1,'first'); % Hier nog even naar kijken
    if size(LocFirmAndDateRangeMatch,1)~=0 
        Data.(NameEquityIssueVar{1})(LocFirmAndDateRangeMatch)= Data.(NameEquityIssueVar{1})(LocFirmAndDateRangeMatch)+1;
        Data.(strcat(NameEquityIssueVar{1},'AmountIssued_EUR'))(LocFirmAndDateRangeMatch)= Data.(strcat(NameEquityIssueVar{1},'AmountIssued_EUR'))(LocFirmAndDateRangeMatch)+DataEquityIssues.AmountIssued_EUR(IndexEquityIssue);
        countEquityIssuesMatched = countEquityIssuesMatched+1;
        DataEquityIssues.Matched = DataEquityIssues.Matched | IndexEquityIssue;
    else
        if sum(IndexFirmIdentifierInBS)>0
            countEquityIssuesNotMatchedButIdentifierMatched = countEquityIssuesNotMatchedButIdentifierMatched+1;
        else
            countEquityIssuesNotMatched = countEquityIssuesNotMatched+1;
            IdentifierNotInBS = [IdentifierNotInBS;FirmIdentifier];
        end
    end
    
end

end