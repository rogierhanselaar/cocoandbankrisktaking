function DataCoCoIssue = addFirmBvDIDNumbers(DataCoCoIssue)

pcinfo=java.net.InetAddress.getLocalHost;
if ~strcmpi(pcinfo.getHostName,'LAPTOP-6P6G4FB7')
    pathname = 'C:\Users\54167rha\Dropbox\PhD\Data\ProjectsWithProfPurnanandam_RogierHanselaar\CoCosAndRiskTaking\Data\BloombergData\Issues\';
else
    pathname = 'C:\Users\rogie\Dropbox\PhD\Data\ProjectsWithProfPurnanandam_RogierHanselaar\CoCosAndRiskTaking\Data\BloombergData\Issues\';
end



%% Load and merge link LinkCoCosWithoutFirmISINToBvDIDNumber
LinkCoCosWithoutFirmISIN = readtable(strcat(pathname,'LinkCoCosWithoutFirmISINToBvDIDNumber.xlsx'));
LinkCoCosWithoutFirmISIN(strcmpi(LinkCoCosWithoutFirmISIN.BvDIDNumber,'-'),:)=[];
DataCoCoIssue = outerjoin(DataCoCoIssue,LinkCoCosWithoutFirmISIN,...
    'Type','Left',...
    'LeftKeys',{'Ticker'},...
    'RightKeys',{'BBTicker'},...
    'RightVariables',{'BvDIDNumber'});


%% Load and merge link LinkCoCosWithoutMatchingFirmISINToBvDIDNumber
LinkCoCosWithoutMatchingFirmISIN = readtable(strcat(pathname,'LinkCoCosWithoutMatchingFirmISINToBvDIDNumber.xlsx'));
LinkCoCosWithoutMatchingFirmISIN(strcmpi(LinkCoCosWithoutMatchingFirmISIN.BvDIDNumber,'-'),:)=[];
LinkCoCosWithoutMatchingFirmISIN.Properties.VariableNames{'BvDIDNumber'} = 'BvDIDNumber2';
DataCoCoIssue = outerjoin(DataCoCoIssue,LinkCoCosWithoutMatchingFirmISIN,...
    'Type','Left',...
    'LeftKeys',{'BBTickerFirmBasedonCUSIP'},...
    'RightKeys',{'BBTicker'},...
    'RightVariables',{'BvDIDNumber2'});
DataCoCoIssue.IndexReplace = strcmpi(DataCoCoIssue.BvDIDNumber,'') & ~strcmpi(DataCoCoIssue.BvDIDNumber2,'');
DataCoCoIssue.BvDIDNumber(DataCoCoIssue.IndexReplace) = ...
    DataCoCoIssue.BvDIDNumber2(DataCoCoIssue.IndexReplace);
DataCoCoIssue.BvDIDNumber2 = [];DataCoCoIssue.IndexReplace=[];

%% Add the firm BvDIDNumber for the HSBC cocos manualy.
DataCoCoIssue.BvDIDNumber(strcmpi(DataCoCoIssue.BBTickerFirmBasedonCUSIP,'MID LN')) = repmat({'GB00014259'},3,1);


end