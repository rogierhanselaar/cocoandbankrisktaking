clear
%% Load data
pcinfo=java.net.InetAddress.getLocalHost;
if ~strcmpi(pcinfo.getHostName,'LAPTOP-6P6G4FB7')
    pathname = 'C:\Users\54167rha\Dropbox\PhD\Data\ProjectsWithProfPurnanandam_RogierHanselaar\CoCosAndRiskTaking\Data\';
else
    pathname = 'C:\Users\rogie\Dropbox\PhD\Data\ProjectsWithProfPurnanandam_RogierHanselaar\CoCosAndRiskTaking\Data\';
end

% Load processed BankScope data
load(strcat(pathname,'BankScopeData\ProcessedBankScopeData.mat'));

% Load CoCo issue data
DataCoCoIssue = importCoCoIssueData(strcat(pathname,'BloombergData\Issues\20161121BloombergDataCocos.xlsx'));
addpath('C:\Users\rogie\Git\cocoandbankrisktaking\Data')
FXData = loadFXData();
[DataCoCoIssue] = convertCurrenciesToEUR(DataCoCoIssue,FXData,{'AmountIssued'});

% Load SDC Equity issues data
DataEquityIssues = readtable('C:\Users\rogie\Dropbox\PhD\Data\ProjectsWithProfPurnanandam_RogierHanselaar\CoCosAndRiskTaking\Data\SDCData\ProcessedDataFOs.csv');

% Load Datastream data
DataDS = readtable(strcat(pathname,'DatastreamData\ProcessedDatastreamData.csv'));

% Create fiscal year variable
Data.FiscalYear=NaN(size(Data,1),1);
Data.FiscalYearMonth = month(Data.CloseDateDN);
Data.FiscalYear(Data.FiscalYearMonth>=7) = year(Data.CloseDateDN(Data.FiscalYearMonth>=7));
Data.FiscalYear(Data.FiscalYearMonth<7) = year(Data.CloseDateDN(Data.FiscalYearMonth<7))-1;

% Match firm BvDIDNumbers to cocos that do not have a firm isin, or cocos
% that have a non-matching firm isin
DataCoCoIssue = addFirmBvDIDNumbers(DataCoCoIssue);

% Make both ISINs and BvDIDNumbers categorical variables
DataCoCoIssue.FirmISIN = categorical(DataCoCoIssue.FirmISIN);
DataCoCoIssue.BvDIDNumber = categorical(DataCoCoIssue.BvDIDNumber);
DataCoCoIssue.CUSIP = categorical(DataCoCoIssue.CUSIP);
Data.ISINNumber= categorical(Data.ISINNumber);
Data.BvDIDNumbers = categorical(Data.BvDIDNumbers);

%% Merge data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Consider merging seperately for Tier 1 and Tier 2 Cocos. Also consider
% focusing on the differences in tax treatment of these types of cocos
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Merge coco issue data into BankScope data, with as a result for each firm
% fiscal year combination the number of cocos, as well as
% the dollar amount of cocos that is issued.
[Data,DataCoCoIssueCoCoIssued]= mergeCoCoDataIntoBankScopeData(Data,DataCoCoIssue,{'CoCoIssued'});

% Merge coco issue data of cocos of the "equity conversion"-type into
% BankScope data, with as a result for each firm fiscal year combination
% the number of cocos, as well as the dollar amount of cocos that is issued.
IndexEquityConversionCoCo = strcmpi(DataCoCoIssue.CapTypeCoCoAction,'Equity Conversion');
[Data,DataCoCoIssueEquityConversion]= mergeCoCoDataIntoBankScopeData(Data,DataCoCoIssue(IndexEquityConversionCoCo,:),{'CoCoIssuedEquityConversionType'});

% Merge coco issue data of cocos of the "write-down"-type into
% BankScope data, with as a result for each firm fiscal year combination
% the number of cocos, as well as the dollar amount of cocos that is issued.
IndexWriteDownCoCo = strcmpi(DataCoCoIssue.CapTypeCoCoAction,'Permanent Write Down') |...
    strcmpi(DataCoCoIssue.CapTypeCoCoAction,'Partial Permanent Write Down') |...
    strcmpi(DataCoCoIssue.CapTypeCoCoAction,'Temporary Write Down');
[Data,DataCoCoIssueWriteDown]= mergeCoCoDataIntoBankScopeData(Data,DataCoCoIssue(IndexWriteDownCoCo,:),{'CoCoIssuedWriteDownType'});

% Merge coco issue data of cocos with trigger level above median into
% BankScope data, with as a result for each firm fiscal year combination
% the number of cocos, as well as the dollar amount of cocos that is issued.
IndexTriggerAboveMedianCoCo = DataCoCoIssue.COCOTriggerLevel>nanmedian(DataCoCoIssue.COCOTriggerLevel);
[Data,DataCoCoIssueTriggerAboveMed]= mergeCoCoDataIntoBankScopeData(Data,DataCoCoIssue(IndexTriggerAboveMedianCoCo,:),{'CoCoIssuedAboveMedianTrigger'});

% Merge coco issue data of cocos with trigger level below median into
% BankScope data, with as a result for each firm fiscal year combination
% the number of cocos, as well as the dollar amount of cocos that is issued.
IndexTriggerBelowMedianCoCo = DataCoCoIssue.COCOTriggerLevel<nanmedian(DataCoCoIssue.COCOTriggerLevel);
[Data,DataCoCoIssueTriggerBelowMed]= mergeCoCoDataIntoBankScopeData(Data,DataCoCoIssue(IndexTriggerBelowMedianCoCo,:),{'CoCoIssuedBelowMedianTrigger'});

% Merge coco issue data of cocos with trigger level at median into
% BankScope data, with as a result for each firm fiscal year combination
% the number of cocos, as well as the dollar amount of cocos that is issued.
IndexTriggerAtMedianCoCo = DataCoCoIssue.COCOTriggerLevel==nanmedian(DataCoCoIssue.COCOTriggerLevel);
[Data,DataCoCoIssueTriggerAtMed]= mergeCoCoDataIntoBankScopeData(Data,DataCoCoIssue(IndexTriggerAtMedianCoCo,:),{'CoCoIssuedAtMedianTrigger'});

% Merge coco issue data of cocos with trigger level at or below median into
% BankScope data, with as a result for each firm fiscal year combination
% the number of cocos, as well as the dollar amount of cocos that is issued.
IndexTriggerAtOrBelowMedianCoCo = DataCoCoIssue.COCOTriggerLevel<=nanmedian(DataCoCoIssue.COCOTriggerLevel);
[Data,DataCoCoIssueTriggerAtOrBelowMed]= mergeCoCoDataIntoBankScopeData(Data,DataCoCoIssue(IndexTriggerAtOrBelowMedianCoCo,:),{'CoCoIssuedAtOrBelowMedianTrigger'});

varNames = {'','EquityConversionType','WriteDownType','AboveMedianTrigger','BelowMedianTrigger','AtMedianTrigger','AtOrBelowMedianTrigger'};
for v = varNames
    % Create a variable that equals the cumulative number of cocos issued by a
    % firm for each firm year combination
    varnameCoCoIssued = strcat('CoCoIssued',v{1});
    Data = createCumulativeCoCoIssuance(Data,varnameCoCoIssued);
    
    % Create a dummy that indicates whether a firm has issued one or more CoCos
    Data.(strcat('DummyHasIssuedCoCo',v{1})) = Data.(strcat('CoCoIssued',v{1},'_cumsum'))>0;
    
    % Create a variable that equals the cumulative dollar amount of cocos
    % issued by a firm for each firm year combination
    varnameCoCoIssued = strcat('CoCoIssued',v{1},'AmountIssued_EUR');
    Data = createCumulativeCoCoIssuance(Data,varnameCoCoIssued);
end
% Create a variable that indicates whether interest payments on cocos can
% be deducted from income at a specific point in time in a specific country
Data = createFiscalTreatmentVariable(Data);

%% Merge the SDC Equity issuance data


%% Merge Datastream data
DataDS.ISIN = categorical(DataDS.ISIN);
Data = outerjoin(Data,DataDS,...
    'Type','Left',...
    'LeftKeys',{'ISINNumber','FiscalYear'},...
    'RightKeys',{'ISIN','FiscalYear'},...
    'RightVariables','AnnualVol_pct');

%% Write data away
save(strcat(pathname,'MergedAndProcessedData.mat'),'Data');


%% Other Notes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Some notes on the CoCo issue data:
% - The amount of CoCos issued per bank varies from ... to ..., with one
% exception: Lloyds Banking Group issued 52(!) CoCos in 2009 for a total of
% 4.6 bilion euros, 13.9 bilion pounds, 0.4 bilion Japanes Yen, and 7.1
% bilion US dollar. According to wikipedia, Lloyds was at that time for
% 40-50% in the hands of the government as a result of government
% interventions due to the financial crisis and apparently had to choose
% between more state aid (Asset Protection Scheme) or some other radical
% move.
% 
% Some notes on the matching results:
% - We have matched 216 CoCos, issued by 63 companies, in 16 countries.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% To select all banks that issued CoCos in a particular country:
% Data(strcmpi(Data.CountryName,'Italy') & Data.CoCoIssued>0,{'NAME','YearpartofCLOSDATE','CoCoIssued'});