function [Data,DataCoCoIssue] = mergeCoCoDataIntoBankScopeData(Data,DataCoCoIssue,NameCoCoVar)

% For each firm in the CoCo data, find the corresponding firm in the
% BankScope data based on ISIN, and set the indicator CoCoIssued to 1
MatcVars.CoCo = 'FirmISIN';
MatcVars.BS = 'ISINNumber';
[Data,DataCoCoIssue,FirmIdentifierNotMatchedISIN] = matchCoCosToBanksBasedOnISIN(Data,DataCoCoIssue,MatcVars,NameCoCoVar);

% For each firm in the CoCo data, find the corresponding firm in the BS
% data based on BvDIDNumber, and set the indicator CoCoIssued to 1 
MatcVars.CoCo = 'BvDIDNumber';
MatcVars.BS = 'BvDIDNumbers';
[Data,DataCoCoIssue,FirmIdentifierNotMatchedManual] = matchCoCosToBanksBasedOnISIN(Data,DataCoCoIssue,MatcVars,NameCoCoVar);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% After merging based on firm ISIN, we have out of 378 CoCos:
% - 181 CoCos not matched
% - 37 CoCos matched on ISIN but not on date
% - 160 CoCos matched
% 
% Out of the 181 CoCos that are not matched, we have:
% - 73 CoCos for which the firm ISIN is missing in the CoCo dataset 
% - 108 CoCos for which there is no matching firm ISIN in the BS dataset.
% This may be due to the fact that the cocos were issued by non-European
% institutions. 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% After merging based on manual link (on name), we have out of 378 CoCos:
% - 18 CoCos matched on firm name but not on date
% - 56 CoCos matched
% 
% So in total we have:
% - 160+56 = 216 CoCos matched
% - 37+18 = 55 CoCos matched on firm name but not on date
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [Data,DataCoCoIssue,IdentifierNotInBS] = matchCoCosToBanksBasedOnISIN(Data,DataCoCoIssue,MatcVars,NameCoCoVar)
% Each coco has a unique CUSIP, so I will use CUSIPs to identify individual
% CoCos. I will only use those CoCos that have a firm ISIN
CoCosWithMissingIdentifier = DataCoCoIssue.(MatcVars.CoCo)==categorical(cellstr('#N/A N/A'))...
    | DataCoCoIssue.(MatcVars.CoCo)==categorical(cellstr('ActiveX VT_ERROR: '))...
    | isundefined(DataCoCoIssue.(MatcVars.CoCo));
if sum(strcmpi(DataCoCoIssue.Properties.VariableNames,'Matched'))==1
    CoCos = categorical(unique(DataCoCoIssue.CUSIP(~CoCosWithMissingIdentifier & ~DataCoCoIssue.Matched)));
else
    CoCos = categorical(unique(DataCoCoIssue.CUSIP(~CoCosWithMissingIdentifier)));
end

% Intialize countvariables
countCoCosNotMatched = sum(CoCosWithMissingIdentifier); 
countCoCosNotMatchedButIdentifierMatched=0; 
countCoCosMatched=0; 
IdentifierNotInBS = categorical({});

% Initialize a variable showing which CoCos are not yet matched
if sum(strcmpi(DataCoCoIssue.Properties.VariableNames,'Matched'))~=1
    DataCoCoIssue.Matched = zeros(size(DataCoCoIssue,1),1);
end

% Initialize variables of interest
if sum(strcmpi(Data.Properties.VariableNames,NameCoCoVar{1}))~=1
    Data.(NameCoCoVar{1})=zeros(size(Data,1),1);
    Data.(strcat(NameCoCoVar{1},'AmountIssued_EUR'))=zeros(size(Data,1),1);
end

% Loop over CoCos; match each coco to its corresponding bank
for CoCo = CoCos'
    % Info on CoCo
    IndexCoCo = DataCoCoIssue.CUSIP==CoCo;
    IssueDateDN = DataCoCoIssue.IssueDateDN(IndexCoCo);
    
    % Info on Corresponding firm
    FirmIdentifier = DataCoCoIssue.(MatcVars.CoCo)(IndexCoCo);
    IndexFirmIdentifierInBS = Data.(MatcVars.BS)==FirmIdentifier;
    
    % Set variable Data.(NameCoCoVar{1}) and set counters
    LocFirmAndDateRangeMatch = find(Data.CloseDateDN>IssueDateDN & IndexFirmIdentifierInBS,1,'first'); % Hier nog even naar kijken
    if size(LocFirmAndDateRangeMatch,1)~=0 
        Data.(NameCoCoVar{1})(LocFirmAndDateRangeMatch)= Data.(NameCoCoVar{1})(LocFirmAndDateRangeMatch)+1;
        Data.(strcat(NameCoCoVar{1},'AmountIssued_EUR'))(LocFirmAndDateRangeMatch)= Data.(strcat(NameCoCoVar{1},'AmountIssued_EUR'))(LocFirmAndDateRangeMatch)+DataCoCoIssue.AmountIssued_EUR(IndexCoCo);
        countCoCosMatched = countCoCosMatched+1;
        DataCoCoIssue.Matched = DataCoCoIssue.Matched | IndexCoCo;
    else
        if sum(IndexFirmIdentifierInBS)>0
            countCoCosNotMatchedButIdentifierMatched = countCoCosNotMatchedButIdentifierMatched+1;
        else
            countCoCosNotMatched = countCoCosNotMatched+1;
            IdentifierNotInBS = [IdentifierNotInBS;FirmIdentifier];
        end
    end
    
end

end