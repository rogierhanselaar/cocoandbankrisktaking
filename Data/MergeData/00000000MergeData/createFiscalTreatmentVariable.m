function Data = createFiscalTreatmentVariable(Data)

% Load dates from which point onwards interest payments are deductible from
% income for tax purposes.
FiscalTreatment = createFiscalTreatmentOverview();

% Merge the dates on which fiscal treatment changes with Data
Data = outerjoin(Data,FiscalTreatment,...
    'Type','Left',...
    'LeftKeys',{'CountryISOcode'},...
    'RightKeys',{'Country'},...
    'RightVariables','DateFiscalTreatmentChange');

% Create a variable that equals 1 if interest payments are deductible 
Data = createDummyFiscalTreatment(Data);

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Helper functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function FiscalTreatment = createFiscalTreatmentOverview()

FiscalTreatment = [{'NL'},{20141002};{'DE'},{20140410};{'GB'},{20130716};...
    {'IT'},{NaN};{'FR'},{NaN};{'BE'},{20131022};{'CH'},{19000101};...
    {'NO'},{NaN};{'DK'},{20040101};{'ES'},{19850101};{'SE'},{NaN};...
    {'CY'},{NaN};{'IE'},{NaN};{'PT'},{NaN};{'AT'},{99999999}];
FiscalTreatment = array2table(FiscalTreatment,'VariableNames',{'Country','DateFiscalTreatmentChange'});
FiscalTreatment.DateFiscalTreatmentChange = cell2mat(FiscalTreatment.DateFiscalTreatmentChange);
FiscalTreatment.Year = floor(FiscalTreatment.DateFiscalTreatmentChange/10000);

end

function Data = createDummyFiscalTreatment(Data)

Data.DummyFiscalTreatment = NaN(size(Data,1),1);
Data.DummyFiscalTreatment(~isnan(Data.DateFiscalTreatmentChange))=0;
Data.DummyFiscalTreatment(Data.ClosingDate>Data.DateFiscalTreatmentChange)=1;

end