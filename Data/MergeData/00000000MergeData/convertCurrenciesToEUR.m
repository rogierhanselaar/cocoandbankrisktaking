function [DataCoCoIssue] = convertCurrenciesToEUR(DataCoCoIssue,FXData,vars_level)

%% Transform currencies to EUR for level variables in other currencies 
% Get correct exchange rate
DataCoCoIssue = outerjoin(DataCoCoIssue,FXData,...
    'Type','Left',...
    'LeftKeys',{'Curr','IssueDate'},...
    'RightKeys',{'Currency','date'},...
    'RightVariables',{'ExchangeRate'});

% Convert into euros
for v = vars_level
    varName = strcat(v,'_EUR');
    DataCoCoIssue.(varName{1}) = DataCoCoIssue.(v{1}).*DataCoCoIssue.ExchangeRate;
end

end