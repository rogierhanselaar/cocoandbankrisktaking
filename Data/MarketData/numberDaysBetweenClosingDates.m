function DataDS = numberDaysBetweenClosingDates(DataDS)

DataDS = sortrows(DataDS,'date','ascend');
DataDS = sortrows(DataDS,'ISIN','ascend');
DataDS.Days = NaN(size(DataDS,1),1);
DataDS.ClosingYear = NaN(size(DataDS,1),1);

N = size(DataDS,1);
IndexClosingDates = ~isnan(DataDS.ClosingDate);
Loc_ClosingDates = find(IndexClosingDates);
N_days = NaN(sum(IndexClosingDates),1);
BankISIN = {};
for CD = 1:sum(IndexClosingDates)
    IndexPriorToClosingDate = [ones(Loc_ClosingDates(CD),1); zeros(N-Loc_ClosingDates(CD),1)];
    IndexNaN = isnan(DataDS.Days);
    N_days(CD) = sum(IndexNaN & IndexPriorToClosingDate);
    DataDS.Days(IndexNaN & IndexPriorToClosingDate) = 1:N_days(CD);
    DataDS.ClosingYear(IndexNaN & IndexPriorToClosingDate) = floor(DataDS.ClosingDate(Loc_ClosingDates(CD))/10000);
%     if sum(strcmpi(unique(DataDS.ISIN(IndexNaN & IndexPriorToClosingDate)),'GRS323003012'))==1
%         CD;
%     end
    if N_days(CD)>500 && N_days(CD)<550 % i.e. about two years
        DataDS.Days(IndexNaN & IndexPriorToClosingDate) = DataDS.Days(IndexNaN & IndexPriorToClosingDate)-265;
    elseif N_days(CD)>270
%     if N_days(CD)>270
        BankISIN = [BankISIN; unique(DataDS.ISIN(IndexNaN & IndexPriorToClosingDate))];
        DataDS.Days(IndexNaN & IndexPriorToClosingDate,:)=-99999;
%         error('Even nakijken wat hier gebeurt; hoe groot zijn de gaten die vallen tussen reporting dates?')
    end
end
DataDS(DataDS.Days<=0,:)=[];

end