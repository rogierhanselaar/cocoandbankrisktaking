function DataBS = findClosestTradingDay(DataBS,DataDS)

ClosingDatesAndTradingDays = table();
ClosingDatesAndTradingDays.ClosingDates = unique(DataBS.ClosingDate);
ClosingDatesAndTradingDays.TradingDate = NaN(size(ClosingDatesAndTradingDays,1),1);
TradingDates = table();
TradingDates.TradingDate = unique(DataDS.date);

% Find closing dates that are also a trading day. For closing dates that
% are not a trading day, go back one day at the time until you hit a
% trading day 
ClosingDates = ClosingDatesAndTradingDays.ClosingDates;
for c = ClosingDates'
    CD = c;
    while sum(CD==TradingDates.TradingDate)==0
        CD = CD-1;
    end
    ClosingDatesAndTradingDays.TradingDate(ClosingDatesAndTradingDays.ClosingDates==c) = CD;
end

DataBS= outerjoin(DataBS,ClosingDatesAndTradingDays,...
    'Type','Left',...
    'LeftKeys',{'ClosingDate'},...
    'RightKeys',{'ClosingDates'},...
    'RightVariables','TradingDate');

DataBS = sortrows(DataBS,'ISINNumber','ascend');

end