function DataDS = createFiscalYearVariable(DataDS)

% For each firm 
ISINs = unique(DataDS.ISIN);
DataDS.FiscalYear=NaN(size(DataDS,1),1);
for i = ISINs'
%     if strcmpi(i,'DE0008063306')
%         i;
%     end
    IndexISIN = strcmpi(DataDS.ISIN,i{1});
    ClosingDates = unique(DataDS.ClosingDate(IndexISIN & ~isnan(DataDS.ClosingDate)));
    ClosingMonths = unique(floor((ClosingDates - floor(ClosingDates/10000)*10000)/100));
    if size(ClosingMonths,1)>1
        error(strcat('Company: ',i{1},' seems to have changing fiscal closing months'))
    elseif ClosingMonths>=7
        DataDS.FiscalYear(IndexISIN) = DataDS.ClosingYear(IndexISIN);
    else
        DataDS.FiscalYear(IndexISIN) = DataDS.ClosingYear(IndexISIN)-1;
    end
    
end



end