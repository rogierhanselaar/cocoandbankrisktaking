clear
%% Load data
pcinfo=java.net.InetAddress.getLocalHost;
if ~strcmpi(pcinfo.getHostName,'LAPTOP-6P6G4FB7')
    pathname = '';
    error('The pathname for the Dell laptop still needs setting')
else
    pathname = 'C:\Users\rogie\Dropbox\PhD\Data\ProjectsWithProfPurnanandam_RogierHanselaar\CoCosAndRiskTaking\Data\DatastreamData\';
    pathnameBankScopeData = 'C:\Users\rogie\Dropbox\PhD\Data\ProjectsWithProfPurnanandam_RogierHanselaar\CoCosAndRiskTaking\Data\BankScopeData\';
end

% Load Datastream data
DataDS = importBankMarketDataDS(strcat(pathname,'20170221MarketDataBanksFromDS.xlsx'));
DataDS.CURRENCY=[];
DataDS(strcmpi(DataDS.Variable,'ActiveX VT_ERROR: '),:)=[];

% Reset Datastream data variable names
Dates = importDates(strcat(pathname,'Dates.xlsx'));
Dates.Dates = year(Dates.Dates)*10000+month(Dates.Dates)*100+day(Dates.Dates);
Dates = cellstr(strcat('D',num2str(Dates.Dates)));
DataDS.Properties.VariableNames(3:end) = Dates';

% Reformat data
DataDS = stack(DataDS,Dates,'NewDataVariableName','value','IndexVariableName','date');
DataDS = unstack(DataDS,'value','Variable');

% Transform date variable and create Year and MonthDay variables
DataDS.date = str2double(cellfun(@(x) x(2:end),cellstr(DataDS.date),'UniformOutput',false));
DataDS.Year = floor(DataDS.date/10000);
% DataDS.MonthDay = categorical(DataDS.date-DataDS.Year*10000);
% DataDS.MonthDay = renamecats(DataDS.MonthDay,categories(DataDS.MonthDay),strcat('MD',categories(DataDS.MonthDay)));

% Discard data in 2017
DataDS(DataDS.Year==2017,:)=[];

% Load closing dates from Bankscope
load(strcat(pathnameBankScopeData,'ProcessedBankScopeData.mat'))
DataBS = Data(~cellfun(@isempty,Data.ISINNumber),{'ISINNumber','ClosingDate'}); Data =[];
DataBS = findClosestTradingDay(DataBS,DataDS);
DataDS = outerjoin(DataDS,DataBS,...
    'Type','Left',...
    'LeftKeys',{'ISIN','date'},...
    'RightKeys',{'ISINNumber','TradingDate'},...
    'RightVariables','TradingDate');
DataDS.Properties.VariableNames{'TradingDate'} = 'ClosingDate';

% Discard datastream data after last closing date (is necessary for
% numbering the days between closing dates in next function)
DataDS = discardDSDataBeforeFirstAndAfterLastCD(DataDS,DataBS);

% Delete 'GB0001452795', and 'GB0002228152' as it has varying length fiscal
% years (both firms are nationalised in 2008, split up, etc.)
DataDS(strcmpi(DataDS.ISIN,'GB0001452795'),:)=[];
DataDS(strcmpi(DataDS.ISIN,'GB0002228152'),:)=[];
DataDS(strcmpi(DataDS.ISIN,'GB0030221864'),:)=[];

% Number days between closingdates
DataDS = numberDaysBetweenClosingDates(DataDS);
DataDS.Days = categorical(DataDS.Days);
DataDS.Days = renamecats(DataDS.Days,categories(DataDS.Days),strcat('D',categories(DataDS.Days)));

% Create Fiscal Year variable
DataDS = createFiscalYearVariable(DataDS);

%% Calculate variables of interest
% Calculate returns
DataDS.Return_pct = NaN(size(DataDS,1),1);
DataDS.Return_pct(2:end) = (DataDS.RI(2:end)./DataDS.RI(1:end-1)-1)*100;
DataDS.Return_pct([true;~strcmpi(DataDS.ISIN(2:end),DataDS.ISIN(1:end-1))])=NaN;

% Calculate annualized volatility
DataWide = unstack(DataDS(:,{'ISIN','FiscalYear','Return_pct','Days'}),{'Return_pct'},{'Days'});
DataWideArray = table2array(DataWide(:,3:end));
DataDS.NoVolumeDays = isnan(DataDS.VO) | DataDS.VO==0;
DataWideNoVolumeDays = unstack(DataDS(:,{'ISIN','FiscalYear','NoVolumeDays','Days'}),{'NoVolumeDays'},{'Days'});
DataWideNoVolumeDaysArray = table2array(DataWideNoVolumeDays(:,3:end));
T = size(DataWideArray,2);
IndexNaN = isnan(DataWideArray);
IndexNaNNoVolumeDays = isnan(DataWideNoVolumeDaysArray);
DataWide.AnnualVol_pct = nanstd(DataWideArray,0,2)*sqrt(250);
DataWide.FracZeroRet = sum(DataWideArray==0,2)./(T - sum(IndexNaN,2));
DataWide.FracZeroVol = nansum(DataWideNoVolumeDaysArray==1,2)./(T - sum(IndexNaNNoVolumeDays,2));
DataWide.AnnualVol_pct(DataWide.FracZeroRet>0.9)=NaN; % --> Amounts to removing stocks for which 90% or more of the days have zero return
DataWide.AnnualVol_pct(DataWide.FracZeroVol>0.9)=NaN; % --> Amounts to removing stocks for which 90% or more of the days have zero volume
DataFinal = DataWide(:,{'ISIN','FiscalYear','AnnualVol_pct'});

%% Write annualized volatility away
writetable(DataFinal,strcat(pathname,'ProcessedDatastreamData.csv'))


%% Other old stuff

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%% Old loading of bloomberg data %%%%%%%%%%%%%%%%%%%%%%
% % Load formatted Bloomberg market data
% Data = readtable(strcat(pathname,'Formatted20170217AllISINsAfterFilters.xlsx'));
% 
% % Convert to numeric format
% variablesToConvert = Data.Properties.VariableNames(3:end);
% for v = variablesToConvert
%     Data.(v{1}) = cellfun(@str2num, Data.(v{1}),'UniformOutput',false);
%     Data.(v{1})(cellfun(@isempty,Data.(v{1})))={NaN};
%     Data.(v{1}) = cell2mat(Data.(v{1}));
% end
% 
% % Add Year, YearMonth, MonthDay, and Day variables
% Data.Year = floor(Data.date/10000);
% Data.YearMonth = floor(Data.date/100);
% Data.MonthDay = Data.date-Data.Year*10000;
% Data.Day = Data.date-Data.YearMonth*100;
% Data.Day = categorical(Data.Day);
% Data.Day = renamecats(Data.Day,categories(Data.Day),strcat('D',categories(Data.Day)));
% 
% % Load ISIN Codes
% ISINCodes = importISINs(strcat(pathname,'20170217AllISINsAfterFilters.xlsx'));
% 
% % Merge ISIN codes into data
% Data = outerjoin(Data,ISINCodes,...
%     'Type','Left',...
%     'RightKeys','BBTicker',...
%     'LeftKeys','Ticker',...
%     'RightVariables','ISIN');
% Data = Data(:,[end 1:end-1]);
% 
% % Identify banks for which there is missing data
% BanksWithoutData = {};
% for i = unique(Data.ISIN)'
%     if sum(~isnan(Data.PX_LAST(strcmpi(Data.ISIN,i{1}))))==0
%         BanksWithoutData = [BanksWithoutData;i unique(Data.Ticker(strcmpi(Data.ISIN,i{1})))];
%     end
% end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%