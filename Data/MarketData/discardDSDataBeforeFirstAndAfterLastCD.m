function DataDS = discardDSDataBeforeFirstAndAfterLastCD(DataDS,DataBS)

ISINs = unique(DataBS.ISINNumber);
DataBS.LastClosingDate = NaN(size(DataBS,1),1);
DataDS.Remove = NaN(size(DataDS,1),1);
for i = ISINs'
%     if strcmpi(i{1},'GRS323003012')==1
%         i
%     end
    IndexISIN_BS = strcmpi(DataBS.ISINNumber,i{1});
    LastClosingDate = max(DataBS.TradingDate(IndexISIN_BS));
    FirstClosingDate = min(DataBS.TradingDate(IndexISIN_BS));
    
    % Per firm, remove obs after last closing date
    IndexISIN_DS = strcmpi(DataDS.ISIN,i{1});
    DataDS.Remove(IndexISIN_DS & DataDS.date>LastClosingDate,:)=1;
    
    % Per firm, remove obs before (the first closing date - 1 year)
    BeginFirstFinancialYear = FirstClosingDate-10000;
    if BeginFirstFinancialYear - floor(BeginFirstFinancialYear/100)*100 ~= 31
        BeginFirstFinancialYear = floor(BeginFirstFinancialYear/100)*100+31;
    end
    DataDS.Remove(IndexISIN_DS & DataDS.date<=(BeginFirstFinancialYear),:)=1;
end
DataDS(DataDS.Remove==1,:)=[];
DataDS.Remove=[];
end