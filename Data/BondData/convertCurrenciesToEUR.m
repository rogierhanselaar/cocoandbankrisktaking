function Data = convertCurrenciesToEUR(Data,FXData,varsToConvert)

%% Transform currencies to EUR for level variables in other currencies 
% Get correct exchange rate
Data = outerjoin(Data,FXData,...
    'Type','Left',...
    'LeftKeys',{'Currency','Issuedateformatted'},...
    'RightKeys',{'Currency','date'},...
    'RightVariables',{'ExchangeRate'});

% Convert into euros
for v = varsToConvert
    varName = strcat(v,'_EUR');
    Data.(varName{1}) = Data.(v{1}).*Data.ExchangeRate;
end

end