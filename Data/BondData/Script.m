clear

% Load SDC data
pathname = 'C:\Users\rogie\Dropbox\PhD\Data\ProjectsWithProfPurnanandam_RogierHanselaar\CoCosAndRiskTaking\Data\BloombergData\BulletBondIssues\';
filename = '20170815BankBondsWesternEurope2009_2016_Combined.csv';
Data = readtable(strcat(pathname,filename));

% Convert currencies to EUR
FXData = loadFXData();
varsToConvert = {'Amountissued'};
Data = convertCurrenciesToEUR(Data,FXData,varsToConvert);
% Note: some bonds are issued in currencies for which we don't have
% exchange rates. We have exchange rates for:
% BGN,CAD,CHF,CZK,DKK,EEK,EUR,GBP,GEL,HKD,HRK,HUF,INR,JPY,LTL,LVL,NOK,PLN,
% RON,SEK,SKK,TRY,USD,XPF
% For the bonds for which we don't have exchange rates, the amounts issued
% are set to missing instead of converted to EUR. We don't have exchange
% rates for:
% ARS,AUD,BRL,BWP,BYR,CLP,CNY,COP,EGP,GHS,IDR,ILS,KES,KRW,KZT,LBP,LKR,MXN,
% NGN,NZD,PEN,PHP,RSD,RUB,SAR,SGD,THB,UAH,UDI,UGX,VEF,VND,XAU,ZAR,ZMW
% !!!!!!!!!!!!!!! Download additional exchange rate data !!!!!!!!!!!!!!!!!!
% --> On the other hand, we currently have 97% of all observations covered.

% Remove obs for which there is no amount issued
Data(isnan(Data.Amountissued_EUR),:)=[];

% Write data away
filename = 'BankBondsWesternEurope2009_2016_Processed.csv';
todaysDate = datestr(today(),'yyyymmdd');
writetable(Data,strcat(pathname,num2str(todaysDate),filename))
