function CurSDCtoISO = LoadCurrencyLinkSDCtoISO()
[~,~,Temp] = xlsread('C:\Users\rogie\Dropbox\PhD\Data\FXData\SDCCurrencyLinkToISOCurrency.xlsx',1,'A1:E129');
CurSDCtoISO = cell2table([Temp(2:end,1) Temp(2:end,5)],'VariableNames',{'SDCCurrency','ISOCurrency'});


end