clear

% Load SDC data
pathname = 'C:\Users\rogie\Dropbox\PhD\Data\ProjectsWithProfPurnanandam_RogierHanselaar\CoCosAndRiskTaking\Data\SDCData\FollowOnData\';
filename = '20170524EuropeanFinancialsFOs2000_present.csv';
Data = importSDCData(strcat(pathname,filename));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SDC data is already filtered on SIC, Nation (Western Europe), Issue date
% (2000 - begin 2017), issue type (FO), and security type (Common stock,
% ord shares,...). 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Data(strcmpi(Data.MAINTRANCHEINPACKAGE,'N'),:)=[];
Data(strcmpi(Data.IPOFlagYN,'Y'),:)=[];
Data(strcmpi(Data.ISIN,'-'),:)=[];
Data(isnan(Data.PrimarySharesOfferedinthisMkt) | Data.PrimarySharesOfferedinthisMkt<=0,:)=[];

% Convert currency codes SDC
CurSDCtoISO = LoadCurrencyLinkSDCtoISO();
Data = outerjoin(Data,CurSDCtoISO,...
    'Type','Left',...
    'LeftKeys',{'SDCCurrencyCode'},...
    'RightKeys',{'SDCCurrency'},...
    'RightVariables',{'ISOCurrency'});

% Convert currencies to EUR
FXData = loadFXData();
varsToConvert = {'ProceedsAmtincOverSoldsumofallMktsUSDmil',...
    'ProceedsAmtincOverSoldinthisMktUSDmil',...
    'ProceedsAmtinthisMktUSDmil',...
    'PrincipalAmtsumofallMktsUSDmil',...
    'OfferPrice'};
Data = convertCurrenciesToEUR(Data,FXData,varsToConvert);

% Write data away
pathname = 'C:\Users\rogie\Dropbox\PhD\Data\ProjectsWithProfPurnanandam_RogierHanselaar\CoCosAndRiskTaking\Data\SDCData\';
filename = 'ProcessedDataFOs.csv';
todaysDate = year(datetime('today'))*10000+month(datetime('today'))*100+day(datetime('today'));
writetable(Data,strcat(pathname,num2str(todaysDate),filename))
filename = 'ProcessedDataFOs.mat';
save(strcat(pathname,num2str(todaysDate),filename),'Data')
