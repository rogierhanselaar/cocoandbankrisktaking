function FXData = loadFXData()

pcinfo=java.net.InetAddress.getLocalHost;
if ~strcmpi(pcinfo.getHostName,'LAPTOP-6P6G4FB7')
    pathname = 'C:\Users\54167rha\Dropbox\PhD\Data\ProjectsWithProfPurnanandam_RogierHanselaar\CoCosAndRiskTaking\Data\FXData\';
else
    pathname = 'C:\Users\rogie\Dropbox\PhD\Data\ProjectsWithProfPurnanandam_RogierHanselaar\CoCosAndRiskTaking\Data\FXData\';
end
FXData = readtable(strcat(pathname,'FormattedFXDataBloomberg2009_2016.csv'));
FXData.FromISOCodeCurrency = cellfun(@(x) x(1:3),FXData.Ticker,'UniformOutput',false);
FXData = FXData(:,[end 1:end-1]);
FXData.Properties.VariableNames{'PX_LAST'} = 'ExchangeRate';
FXData.date(FXData.date==0) = 20090101;

%% Fix the fact that for HUF, JPY, and XPF the exchange rates are too large
% Deflate by factor 100
IndexDeflate = strcmpi(FXData.FromISOCodeCurrency,'HUF') | strcmpi(FXData.FromISOCodeCurrency,'JPY') | strcmpi(FXData.FromISOCodeCurrency,'XPF');
FXData.ExchangeRate(IndexDeflate) = FXData.ExchangeRate(IndexDeflate)/100;

%% Fix the fact there is no exchange rate on non-trading dates (e.g. 20111231)
% Create framework (this creates non-existent dates but that is not an
% issue)
MonthsKron = kron([1:12]',ones(31,1))*100 + repmat([1:31]',12,1);
DatesFrameWork = kron([2009:2016]',ones(size(MonthsKron,1),1))*10000+repmat(MonthsKron,8,1);
DatesFrameWork(DatesFrameWork>20161115) = [];
DatesFrameWork = array2table(DatesFrameWork);
DatesFrameWork.Properties.VariableNames{'DatesFrameWork'} = 'date';
Currencies = unique(FXData.FromISOCodeCurrency)';
Dates = table();
for c = Currencies
    TempDates = DatesFrameWork;
    TempDates.Currency = repmat(c,size(TempDates,1),1);
    Dates = [Dates;TempDates];
end

% Merge FXData into framework
FXData = outerjoin(Dates,FXData,...
    'Type','Left',...
    'LeftKeys',{'Currency','date'},...
    'RightKeys',{'FromISOCodeCurrency','date'},...
    'RightVariables',{'ExchangeRate'});

% Replace missing observations with the first non-missing preceding value
% (note: there is no missing value at 20090101)
FXData = sortrows(FXData,'date','ascend');
FXData = sortrows(FXData,'Currency','ascend');
while sum(isnan(FXData.ExchangeRate))>0
    FXData.ExchangeRate_lag = NaN(size(FXData,1),1);
    FXData.ExchangeRate_lag(2:end) = FXData.ExchangeRate(1:end-1);
    IndexNaN = isnan(FXData.ExchangeRate) & ~isnan(FXData.ExchangeRate_lag);
    FXData.ExchangeRate(IndexNaN) = FXData.ExchangeRate_lag(IndexNaN);
    FXData.ExchangeRate_lag=[];
end

end