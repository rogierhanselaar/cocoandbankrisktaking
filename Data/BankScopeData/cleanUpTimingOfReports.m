function Data = cleanUpTimingOfReportsOLD(Data,EndMonth,BeginMonth)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INPUTS:
%   - Data: Table with BankScope data
%   - EndMonth: the number of the month up to which reports are still
%   counted as the december fiscal year end of the previous year
%   - BeginMonth: the number of the month after which reports are still
%   counted as the december fiscal year end of the current year
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Categorize the reportdates for each index year combination into the
% correct fiscalyear, i.e. the months smaller than EndMonth ==> previous
% year, the months larger than StartMonth==>current year, rest of the
% months==> delete
Data.FiscalYear = NaN(size(Data,1),1); Data = Data(:,[1:5 end 6:end-1]);
Data.FiscalYear(month(Data.CloseDateDN)<=EndMonth) = year(Data.CloseDateDN(month(Data.CloseDateDN)<=EndMonth))-1;
Data.FiscalYear(month(Data.CloseDateDN)>=BeginMonth) = year(Data.CloseDateDN(month(Data.CloseDateDN)>=BeginMonth));
Data(month(Data.CloseDateDN)>EndMonth & month(Data.CloseDateDN)<BeginMonth,:) = [];

% Find observations that have more than one report per fiscal year
Data = findIndexWithMoreThan1ReportPerFiscalYear(Data,EndMonth,BeginMonth);

% For each firm (determined by BankScopeIndexNumber) with more than one report per fiscal
% year, find the report that is closest to 31 Dec, and remove the others
Data = removeDuplicateReportsPerYear(Data);

% Double check whether there are no observations with more than one report
% per fiscal year anymore
Data = findIndexWithMoreThan1ReportPerFiscalYear(Data,EndMonth,BeginMonth);
if sum(Data.MoreThanOneReportPerFiscalYear)>0
    error('There still are Index with more than 1 report per fiscal year after cleaning up')
end
Data.MoreThanOneReportPerFiscalYear=[];


end


function Data = findIndexWithMoreThan1ReportPerFiscalYear(Data,EndMonth,BeginMonth)

Data.CloseMonth = cellstr(num2str(month(Data.CloseDateDN)));
Data.CloseMonth = strrep(Data.CloseMonth,' ','');
Data.CloseMonth = categorical(strcat('x',Data.CloseMonth));
DataUnstacked = Data(:,{'BankScopeIndexNumber','ClosingDate','FiscalYear','CloseMonth'});
DataUnstacked = unstack(DataUnstacked,'ClosingDate','CloseMonth');
DataUnstacked.MoreThanOneReportPerFiscalYear = sum(~isnan(table2array(DataUnstacked(:,3:end))),2)>1;
varsToStack = cellstr(strcat('x',num2str([1:EndMonth BeginMonth:12]')))';
varsToStack = strrep(varsToStack,' ','');
DataUnstacked = stack(DataUnstacked,varsToStack);
DataUnstacked.Properties.VariableNames(4:5) = {'CloseMonth','ClosingDate'};
DataUnstacked(isnan(DataUnstacked.ClosingDate),:)=[];
Data = outerjoin(Data,DataUnstacked,...
    'Type','Left',...
    'LeftKeys',{'BankScopeIndexNumber','FiscalYear','CloseMonth'},...
    'RightKeys',{'BankScopeIndexNumber','FiscalYear','CloseMonth'},...
    'RightVariables','MoreThanOneReportPerFiscalYear');
Data.CloseMonth=[];



end