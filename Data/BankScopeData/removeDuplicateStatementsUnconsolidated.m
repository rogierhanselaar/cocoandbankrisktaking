function Data = removeDuplicateStatementsUnconsolidated(Data,yearVar)

% Keep only U1/U2/U*/C1: use the unconsolidated balance sheet data. We
% are interested in balance sheet sensitivy. We make sure that the effects
% we are after are not diluted by using consolidated statements. Moreover,
% looking at unconsolidated statements keeps the sample large.
Data(~(strcmpi(Data.ConsolidationCode,'U*') | strcmpi(Data.ConsolidationCode,'U1') | strcmpi(Data.ConsolidationCode,'U2') | strcmpi(Data.ConsolidationCode,'C1')),:)=[];


% Remove BvDIDNumbers FiscalYear duplicates due to consolidation duplicates,
% favoring U1/U2 over U*.
Data.ConsolidationCode(strcmpi(Data.ConsolidationCode,'U*')) = {'Ustar'};
DataUnstacked = Data(:,{'BvDIDNumbers',yearVar{1},'ConsolidationCode','BankScopeIndexNumber'});
DataUnstacked = unstack(DataUnstacked,'BankScopeIndexNumber','ConsolidationCode');
DataUnstacked.RemoveUstarInFavorOfU1 = ~isnan(DataUnstacked.U1) & ~isnan(DataUnstacked.Ustar);
DataUnstacked.RemoveUstarInFavorOfU2 = ~isnan(DataUnstacked.U2) & ~isnan(DataUnstacked.Ustar);
Data = outerjoin(Data,DataUnstacked,...
    'Type','Left',...
    'LeftKeys',{'BvDIDNumbers',yearVar{1}},...
    'RightKeys',{'BvDIDNumbers',yearVar{1}},...
    'RightVariables',{'RemoveUstarInFavorOfU1','RemoveUstarInFavorOfU2'});
Data(Data.RemoveUstarInFavorOfU1==1 & strcmpi(Data.ConsolidationCode,'Ustar'),:)=[];
Data(Data.RemoveUstarInFavorOfU2==1 & strcmpi(Data.ConsolidationCode,'Ustar'),:)=[];
Data.RemoveUstarInFavorOfU1 = [];
Data.RemoveUstarInFavorOfU2 = [];

end