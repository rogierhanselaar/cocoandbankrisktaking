clear

%% Load Data
pcinfo=java.net.InetAddress.getLocalHost;
if ~strcmpi(pcinfo.getHostName,'LAPTOP-6P6G4FB7')
    pathname = 'C:\Users\54167rha\Dropbox\PhD\Data\ProjectsWithProfPurnanandam_RogierHanselaar\CoCosAndRiskTaking\Data\';
else
    pathname = 'C:\Users\rogie\Dropbox\PhD\Data\ProjectsWithProfPurnanandam_RogierHanselaar\CoCosAndRiskTaking\Data\';
end
[Data,vars_level,vars_ratio] = importBankScopeData(strcat(pathname,'BankScopeData\20161116BankScopeBanks2009_2016.xlsx'),1,2,36000);
[Data2,~,~] = importBankScopeData(strcat(pathname,'BankScopeData\20161116BankScopeBanks2009_2016.xlsx'),1,36001,72000);
[Data3,~,~] = importBankScopeData(strcat(pathname,'BankScopeData\20161116BankScopeBanks2009_2016.xlsx'),1,72001,108000);
[Data4,~,~] = importBankScopeData(strcat(pathname,'BankScopeData\20161116BankScopeBanks2009_2016.xlsx'),1,108001,150121);
Data = [Data;Data2;Data3;Data4]; Data2=[];Data3=[];Data4=[];

% Load FX data
addpath('C:\Users\rogie\Git\cocoandbankrisktaking\Data')
FXData = loadFXData();

% Make CLOSDATE into a serial date number
Data.CloseDateDN = datenum(cellstr(num2str(Data.ClosingDate)),'yyyymmdd');
Data = Data(:,[1:3 end 4:end-1]);

%% Restrict sample to only include banks (based on column 'Specialisation')
IndexRemove = strcmpi(Data.Specialisation,'Central banks')...
    | strcmpi(Data.Specialisation,'Clearing & Custody institutions')...
    | strcmpi(Data.Specialisation,'Investment & Trust corporations')...
    | strcmpi(Data.Specialisation,'Other non banking credit institutions')...
    | strcmpi(Data.Specialisation,'Specialized governmental credit institutions');
Data(IndexRemove,:) = [];

%% Remove supranational institutions (e.g. WorldBank)
Data(strcmpi(Data.CountryISOcode,'II'),:)=[];

%% Select only European Banks (i.e. EU-28 + Norway + Switzerland)
ISOKey = readtable(strcat(pathname,'EuropeanAndNorwayAndSwitzerlandISOCodes.xlsx'));
Data = outerjoin(Data,ISOKey,...
    'Type','Left',...
    'LeftKeys',{'CountryISOcode'},...
    'RightKeys',{'ISOcode'},...
    'RightVariables',{'ISOcode'});
Data(strcmpi(Data.ISOcode,''),:)=[];

%% Fix timing issues statements
% %% Fix getting yearly observation only
% Data = cleanUpTimingOfReports(Data,3,10);
% --> It is not necessary to align the fiscal years of all companies around
% 31 December. 
% --> What is necessary though is to remove firms that switch fiscal years
% or are late with reporting (about 36 firms, or 0.5%); otherwise Coco
% issuance might be double counted. An example: a firm has a report in June
% and in December and the firm issues a coco in April --> the coco would be
% counted as being issued in the years preceding both reports.
% --> Removing one of the duplicate statement in a particular year would
% not do the trick as a CoCo runs the risk of not being counted at all or
% an effect of the issuance of a CoCo might not be measured correctly. For
% example: a firm has a report in March and switches to reporting in
% December in a particular year, having both a report in March and in
% December in the same year. If the firm issues a coco in April the year
% prior to the switch, the coco would not be counted; if the firm issues a
% coco in februari in the year prior to the switch, any effect the issuance
% might have on the balance sheet in the period April-December of the year
% would not be measured.
Data = removeFirmsWithMoreStatementsPerYear(Data);

%% Remove non-relevant types of statement: RF, BR, DD, NA
Data(strcmpi(Data.TypeofFormat,'RF') | strcmpi(Data.TypeofFormat,'BR') | strcmpi(Data.TypeofFormat,'DD') | strcmpi(Data.TypeofFormat,'NA'),:)=[];

%% Fix consolidation issues
Data = removeDuplicateStatementsUnconsolidated(Data,{'YearpartofCLOSDATE'});

%% Remove further duplicates
% Look for each firm whether it reports in multiple currencies. Next,
% convert level variables to EUR.
[Data,~] = assureUniformCurrenciesForEachCompany(Data,{'YearpartofCLOSDATE'},FXData,vars_level);

% Remove duplicates based due to different accounting standards.
Data = removeDuplicatesDueToAccountingStandards(Data,{'YearpartofCLOSDATE'});

% Of the remaining BvDIDNumbers FiscalYear duplicates, pick the one with the
% largest total asset size.
Data = remolveRemainingDuplicatesBasedOnTA(Data,{'YearpartofCLOSDATE'});

%% Add additional data if necessary from full bankscope dataset
variableNames = {'data2080','data2000','data11270','data11550','data11620','data18150','data18155','data18157','data2130','data2125','data10090','data11150','data2086','data29190','data11750'};
vars_level = {'data10090','data11150','data11270','data11550','data11620','data11750','data2000','data2080','data2086','data29190'};
Data = addData(Data,FXData,variableNames,vars_level);
Data.Properties.VariableNames{'data2080_EUR'} = 'NetInterestRevenue_EUR';
Data.Properties.VariableNames{'data2000_EUR'} = 'Loans_EUR';
Data.Properties.VariableNames{'data11270_EUR'} = 'CashAndDueFromBanks_EUR';
Data.Properties.VariableNames{'data11550_EUR'} = 'TotalCustomerDeposits_EUR';
Data.Properties.VariableNames{'data11620_EUR'} = 'TotalLongTermFunding_EUR';
Data.Properties.VariableNames{'data10090_EUR'} = 'NetGainsOnTradingAndDerivatives_EUR';
Data.Properties.VariableNames{'data11150_EUR'} = 'TradingSecuritiesAndAtFVThroughIncome_EUR';
Data.Properties.VariableNames{'data2086_EUR'} = 'NetGainsOnTradingAndDerivatives2_EUR';
Data.Properties.VariableNames{'data29190_EUR'} = 'TotalTradingAssetsAtFVThroughIncomeStatement_EUR';
Data.Properties.VariableNames{'data11750_EUR'} = 'TotalLiabilities_EUR';
Data.Properties.VariableNames{'data18150'} = 'Tier1RegulatoryCapitalRatio';
Data.Properties.VariableNames{'data18155'} = 'TotalRegulatoryCapitalRatio';
Data.Properties.VariableNames{'data18157'} = 'CoreTier1RegulatoryCapitalRatio'; % Common Equity and retained earnings; should be larger than 4.5%
Data.Properties.VariableNames{'data2130'} = 'Tier1Ratio'; % Tier 1 and 2; should be larger than 8%
Data.Properties.VariableNames{'data2125'} = 'TotalCapitalRatio'; % Tier 1 and 2; should be larger than 8%

%% Write table away
save(strcat(pathname,'ProcessedBankScopeData.mat'),'Data');



%% Other stuff
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% On correcting for M&A via creating pre-M&A pro-forma banks: 
% - An advantage would be less jumpy balance sheet levels (e.g. total
% assets that jumps up after merger). This advantage is limited, as we will
% be focussing on ratios
% - A disadvatage would be that a bank issuing CoCo's would have its risk
% taking response reduced by the averaging with a bank's balance
% sheet with which there will be a merger in the future
% - Another disadvantage would be that the sample of banks would be much
% smaller
% --> I decided not to create pro-forma banks. 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% For liquidity paper
% countries = {'ARG','AUS','AUT','BEL','BRA','CAN','CHL','CHN','COL','DNK','EGY','FIN','FRA','DEU','GRC','HKG','IND','IDN','IRL','ISR','ITA','MYS','MEX','NLD','NZL','NOR','PHL','POL','PRT','RUS','SGP','ZAF','KOR','ESP','SWE','CHE','THA','GBR','JPN','USANYSE','USANASDAQ'}';
% countries = array2table(countries);
% ISOKey2 = readtable('C:\Users\54167rha\Dropbox\PhD\Data\ProjectsWithProfPurnanandam_RogierHanselaar\CoCosAndRiskTaking\Data\BankScopeData\ISO2D3DKey.xlsx');
% countries = outerjoin(countries,ISOKey2,...
% 'Type','Left',...
% 'LeftKeys',{'countries'},...
% 'RightKeys',{'CountryCode3D'},...
% 'RightVariables',{'CountryCode2D'});
% Data = outerjoin(Data,countries,...
% 'Type','Left',...
% 'LeftKeys',{'CountryISOcode'},...
% 'RightKeys',{'CountryCode2D'},...
% 'RightVariables',{'countries'});
% ISINsWithCountryCode = unique(Data(~strcmpi(Data.CountryCode2D,'') & ~strcmpi(Data.SD_ISIN,''),{'SD_ISIN','countries'}));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%