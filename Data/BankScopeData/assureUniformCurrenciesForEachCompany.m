function [Data,vars_level] = assureUniformCurrenciesForEachCompany(Data,yearVar,FXData,vars_level)
% Should work with BvDIDNumber or Index? --> BvDIDNumber is fine. The
% duplicates arise because of multiple reports in different currencies or
% using different accounting standards.

%% Remove duplicates because of reporting in multiple currencies
[~,ind] = unique(Data(:,{yearVar{1},'BvDIDNumbers'}));
duplicates = setdiff(1:size(Data,1),ind);
Data.Remove = NaN(size(Data,1),1);
for i = duplicates
    IndexBvDIDNumbers = Data.BvDIDNumbers==Data.BvDIDNumbers(i);
    IndexYearBvDIDNumbers = IndexBvDIDNumbers & Data.(yearVar{1})==Data.(yearVar{1})(i);
    if size(unique(Data.Currency(IndexYearBvDIDNumbers)),1)>1 % if duplicate exist because of reporting in multiple currencies
        Currencies = unique(Data.Currency(IndexBvDIDNumbers));
        CurFreq = NaN(size(Currencies,1),1);
        for cur = Currencies'
            CurFreq(strcmpi(Currencies,cur{1}),1) = sum(strcmpi(Data.Currency(IndexBvDIDNumbers),cur{1}));
        end
        MainCurrency = Currencies(CurFreq(:,1)==max(CurFreq),1);
        Data.Remove(IndexYearBvDIDNumbers & ~strcmpi(Data.Currency,MainCurrency{1}))=1;
    end
end
Data(Data.Remove==1,:)=[];
Data.Remove=[];


%% Transform currencies to EUR for level variables in other currencies 
% Get correct exchange rate
Data = outerjoin(Data,FXData,...
    'Type','Left',...
    'LeftKeys',{'Currency','ClosingDate'},...
    'RightKeys',{'Currency','date'},...
    'RightVariables',{'ExchangeRate'});
Data = sortrows(Data,'ClosingDate','ascend');
Data = sortrows(Data,'BvDIDNumbers','ascend');
% Convert into euros
for v = vars_level
    varName = strcat(v,'_EUR');
    Data.(varName{1}) = Data.(v{1}).*Data.ExchangeRate;
end
vars_level = strcat(vars_level','_EUR')';

% Convert all units
Data = assureUniformUnit(Data,vars_level);
end