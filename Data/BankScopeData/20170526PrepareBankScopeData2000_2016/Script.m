clear

%% Load Data
pathname = 'C:\Users\rogie\Dropbox\PhD\Data\ProjectsWithProfPurnanandam_RogierHanselaar\CoCosAndRiskTaking\Data\';
Data = loadData();

% Designate which variables are in levels and which in ratios
% vars_level = {'TotalAssets','TotalLiabilities','CashandDueFromBanks','TradingLiabilities','OtherOperatingIncome','NetInterestIncome','TradingAssetsDashDebtSecuritieswherenoissuerbreakdown','TradingAssetsDashDebtSecuritiesDashGovernments','TradingAssetsDashDebtSecuritiesDashBanks','TradingAssetsDashDebtSecuritiesDashCorporates','TradingAssetsDashDebtSecuritiesDashStructured','TradingAssetsDashEquities','TradingAssetsDashCommodities'};
vars_level = {'TotalAssets','TotalLiabilities','CashandDueFromBanks','TradingLiabilities','OtherOperatingIncome','NetInterestIncome','TradingAssetsDashDebtSecuritieswherenoissuerbreakdown','TradingAssetsDashDebtSecuritiesDashGovernments','TradingAssetsDashDebtSecuritiesDashBanks','TradingAssetsDashDebtSecuritiesDashCorporates','TradingAssetsDashDebtSecuritiesDashStructured','TradingAssetsDashEquities','TradingAssetsDashCommodities','LoanLossProvisions','GrossLoans1','ImpairedLoansMemo','Equity1'};
vars_ratio = {'LoanLossResGrossLoans','LoanLossProvNetIntRev','LoanLossResImpairedLoans','ImpairedLoansGrossLoans','NCOAverageGrossLoans','NCONetIncBefLnLssProv','ImpairedLoansEquity','UnreservedImpairedLoansEquity','NetInterestMargin','PreTaxOpIncAvgAssets','NonOpItemsTaxesAvgAst','ReturnOnAvgAssetsROAA','ReturnOnAvgEquityROAE','DividendPayOut','IncNetOfDistAvgEquity','NonOpItemsNetIncome','CostToIncomeRatio','RecurringEarningPower','InterbankRatio','NetLoansTotAssets','NetLoansDepSTFunding','NetLoansTotDepBor','LiquidAssetsDepSTFunding','LiquidAssetsTotDepBor','NetInterestIncome','NetInterestIncomeAverageEarningAssets','NonInterestExpenseAverageAssets'};

% Load FX data
FXData = loadFXData();

%% Restrict sample to 2000-present (i.e. 2016)
Data(Data.YearpartofCLOSDATE<2000,:)=[];

%% Restrict sample to only include banks (based on column 'Specialisation')
IndexRemove = strcmpi(Data.Specialisation,'Central banks')...
    | strcmpi(Data.Specialisation,'Clearing & Custody institutions')...
    | strcmpi(Data.Specialisation,'Investment & Trust corporations')...
    | strcmpi(Data.Specialisation,'Other non banking credit institutions')...
    | strcmpi(Data.Specialisation,'Specialized governmental credit institutions');
Data(IndexRemove,:) = [];

%% Remove supranational institutions (e.g. WorldBank)
Data(strcmpi(Data.CountryISOcode,'II'),:)=[];

%% Select only European Banks (i.e. EU-28 + Norway + Switzerland)
ISOKey = readtable(strcat(pathname,'BankScopeData\EuropeanAndNorwayAndSwitzerlandISOCodes.xlsx'));
Data = outerjoin(Data,ISOKey,...
    'Type','Left',...
    'LeftKeys',{'CountryISOcode'},...
    'RightKeys',{'ISOcode'},...
    'RightVariables',{'ISOcode'});
Data(strcmpi(Data.ISOcode,''),:)=[];

%% Fix timing issues statements
% --> It is not necessary to align the fiscal years of all companies around
% 31 December. 
% --> What is necessary though is to remove firms that switch fiscal years
% or are late with reporting (about 36 firms, or 0.5%); otherwise Coco
% issuance might be double counted. An example: a firm has a report in June
% and in December and the firm issues a coco in April --> the coco would be
% counted as being issued in the years preceding both reports.
% --> Removing one of the duplicate statement in a particular year would
% not do the trick as a CoCo runs the risk of not being counted at all or
% an effect of the issuance of a CoCo might not be measured correctly. For
% example: a firm has a report in March and switches to reporting in
% December in a particular year, having both a report in March and in
% December in the same year. If the firm issues a coco in April the year
% prior to the switch, the coco would not be counted; if the firm issues a
% coco in februari in the year prior to the switch, any effect the issuance
% might have on the balance sheet in the period April-December of the year
% would not be measured.
% --> On second thought, I'll also keep firms that switch fiscal year. The
% issues with matching coco issuance to the correct year are not that
% important after all; I just accept that some fiscal years are shorter or
% longer than others and assign the coco to the fiscal year anyway

% Data = removeFirmsWithMoreStatementsPerYear(Data);

%% Remove non-relevant types of statement: RF, BR, DD, NA
Data(strcmpi(Data.TypeofFormat,'RF') | strcmpi(Data.TypeofFormat,'BR') | strcmpi(Data.TypeofFormat,'DD') | strcmpi(Data.TypeofFormat,'NA'),:)=[];

%% Fix consolidation issues
[DataU,DataC] = splitDataIntoConsolidatedAndUnconsolidated(Data,{'YearpartofCLOSDATE'});

%% Remove further duplicates
% Look for each firm whether it reports in multiple currencies. Next,
% convert level variables to EUR.
[DataU,~] = assureUniformCurrenciesForEachCompany(DataU,{'YearpartofCLOSDATE'},FXData,vars_level);
[DataC,~] = assureUniformCurrenciesForEachCompany(DataC,{'YearpartofCLOSDATE'},FXData,vars_level);

% Remove duplicates based due to different accounting standards.
DataU = removeDuplicatesDueToAccountingStandards(DataU,{'YearpartofCLOSDATE'});
DataC = removeDuplicatesDueToAccountingStandards(DataC,{'YearpartofCLOSDATE'});

% Of the remaining BvDIDNumbers FiscalYear duplicates, pick the one with the
% largest total asset size.
DataU = remolveRemainingDuplicatesBasedOnTA(DataU,{'YearpartofCLOSDATE'});
DataC = remolveRemainingDuplicatesBasedOnTA(DataC,{'YearpartofCLOSDATE'});

%% Write table away
todaysDate = year(datetime('today'))*10000+month(datetime('today'))*100+day(datetime('today'));
save(strcat(pathname,'BankScopeData\',num2str(todaysDate),'ProcessedBankScopeDataUnconsolidated2000_2016.mat'),'DataU');
save(strcat(pathname,'BankScopeData\',num2str(todaysDate),'ProcessedBankScopeDataConsolidated2000_2016.mat'),'DataC');
% writetable(DataU,strcat(pathname,num2str(todaysDate),'BankScopeData\ProcessedBankScopeDataUnconsolidated2000_2016.csv'))
% writetable(DataC,strcat(pathname,num2str(todaysDate),'BankScopeData\ProcessedBankScopeDataConsolidated2000_2016.csv'))


%% Other stuff
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% On correcting for M&A via creating pre-M&A pro-forma banks: 
% - An advantage would be less jumpy balance sheet levels (e.g. total
% assets that jumps up after merger). This advantage is limited, as we will
% be focussing on ratios
% - A disadvatage would be that a bank issuing CoCo's would have its risk
% taking response reduced by the averaging with a bank's balance
% sheet with which there will be a merger in the future
% - Another disadvantage would be that the sample of banks would be much
% smaller
% --> I decided not to create pro-forma banks. 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% For liquidity paper
% countries = {'ARG','AUS','AUT','BEL','BRA','CAN','CHL','CHN','COL','DNK','EGY','FIN','FRA','DEU','GRC','HKG','IND','IDN','IRL','ISR','ITA','MYS','MEX','NLD','NZL','NOR','PHL','POL','PRT','RUS','SGP','ZAF','KOR','ESP','SWE','CHE','THA','GBR','JPN','USANYSE','USANASDAQ'}';
% countries = array2table(countries);
% ISOKey2 = readtable('C:\Users\54167rha\Dropbox\PhD\Data\ProjectsWithProfPurnanandam_RogierHanselaar\CoCosAndRiskTaking\Data\BankScopeData\ISO2D3DKey.xlsx');
% countries = outerjoin(countries,ISOKey2,...
% 'Type','Left',...
% 'LeftKeys',{'countries'},...
% 'RightKeys',{'CountryCode3D'},...
% 'RightVariables',{'CountryCode2D'});
% Data = outerjoin(Data,countries,...
% 'Type','Left',...
% 'LeftKeys',{'CountryISOcode'},...
% 'RightKeys',{'CountryCode2D'},...
% 'RightVariables',{'countries'});
% ISINsWithCountryCode = unique(Data(~strcmpi(Data.CountryCode2D,'') & ~strcmpi(Data.SD_ISIN,''),{'SD_ISIN','countries'}));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%