function Data = removeDuplicateStatements(Data)
% Remove BvDIDNumbers FiscalYear duplicates due to consolidation duplicates,
% favoring C1/C2 over C*.
Data.ConsolidationCode(strcmpi(Data.ConsolidationCode,'C*')) = {'Cstar'};
DataUnstacked = Data(:,{'BvDIDNumbers','FiscalYear','ConsolidationCode','BankScopeIndexNumber'});
DataUnstacked = unstack(DataUnstacked,'BankScopeIndexNumber','ConsolidationCode');
DataUnstacked.RemoveCstarInFavorOfC1 = ~isnan(DataUnstacked.C1) & ~isnan(DataUnstacked.Cstar);
DataUnstacked.RemoveCstarInFavorOfC2 = ~isnan(DataUnstacked.C2) & ~isnan(DataUnstacked.Cstar);
Data = outerjoin(Data,DataUnstacked,...
    'Type','Left',...
    'LeftKeys',{'BvDIDNumbers','FiscalYear'},...
    'RightKeys',{'BvDIDNumbers','FiscalYear'},...
    'RightVariables',{'RemoveCstarInFavorOfC1','RemoveCstarInFavorOfC2'});
Data(Data.RemoveCstarInFavorOfC1==1 & strcmpi(Data.ConsolidationCode,'Cstar'),:)=[];
Data(Data.RemoveCstarInFavorOfC2==1 & strcmpi(Data.ConsolidationCode,'Cstar'),:)=[];
Data.RemoveCstarInFavorOfC1 = [];
Data.RemoveCstarInFavorOfC2 = [];

% Of the remaining BvDIDNumbers FiscalYear duplicates, pick the one with the
% largest total asset size.
[~,ind] = unique(Data(:,{'FiscalYear','BvDIDNumbers'}));
duplicates = setdiff(1:size(Data,1),ind);
Data.Remove = NaN(size(Data,1),1);
for i = duplicates
    IndexBvdIdNumFiscalYear = Data.BvDIDNumbers==Data.BvDIDNumbers(i) & Data.FiscalYear==Data.FiscalYear(i);
    minTotalAssets = min(Data.TotalAssets(IndexBvdIdNumFiscalYear));
    IndexMinTotalAssets = Data.TotalAssets==minTotalAssets;
    if sum(IndexBvdIdNumFiscalYear & IndexMinTotalAssets)==1
        Data.Remove(IndexBvdIdNumFiscalYear & IndexMinTotalAssets)=1;
    else 
        N_duplicates = sum(IndexBvdIdNumFiscalYear & IndexMinTotalAssets);
        Data.Remove(find(IndexBvdIdNumFiscalYear & IndexMinTotalAssets,N_duplicates-1))=1;
    end
end
Data(Data.Remove==1,:)=[];

end