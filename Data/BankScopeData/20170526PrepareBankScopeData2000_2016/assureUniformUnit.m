function Data = assureUniformUnit(Data,vars_level)
% For each variable correct the level for the unit used
for s = unique(Data.StatementUnit)'
    IndexS = strcmpi(Data.StatementUnit,s{1});
    for v = vars_level
        if strcmpi(s,'bil')
            Data.(v{1})(IndexS) = Data.(v{1})(IndexS)*10^9;
        elseif strcmpi(s,'mil')
            Data.(v{1})(IndexS) = Data.(v{1})(IndexS)*10^6;
        elseif strcmpi(s,'th')
            Data.(v{1})(IndexS) = Data.(v{1})(IndexS)*10^3;
        elseif strcmpi(s,'0')
            Data.(v{1})(IndexS) = Data.(v{1})(IndexS)*10^0;
        else
            error('There is a unit other than bil,mil,th that needs to be taken into account')
        end
    end
end



end