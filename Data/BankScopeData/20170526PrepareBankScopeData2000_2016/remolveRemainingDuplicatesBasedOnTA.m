function Data = remolveRemainingDuplicatesBasedOnTA(Data,yearVar) 
% Should work with BvDIDNumber or Index? --> BvDIDNumber is fine. The
% duplicates arise because of multiple reports in different currencies or
% using different accounting standards.
Data = sortrows(Data,'ClosingDate','ascend');
Data = sortrows(Data,'BvDIDNumbers','ascend');
[~,ind] = unique(Data(:,{(yearVar{1}),'BvDIDNumbers'}));
duplicates = setdiff(1:size(Data,1),ind);
Data.Remove = NaN(size(Data,1),1);
for i = duplicates
    [Data,FiscalYearChange] = checkWhetherFiscalYearChange(Data,i);
    if FiscalYearChange==0
        IndexBvdIdNumYear = Data.BvDIDNumbers==Data.BvDIDNumbers(i) & Data.(yearVar{1})==Data.(yearVar{1})(i);
        minTotalAssets = min(Data.TotalAssets(IndexBvdIdNumYear));
        IndexMinTotalAssets = Data.TotalAssets==minTotalAssets;
        if sum(IndexBvdIdNumYear & IndexMinTotalAssets)==1
            Data.Remove(IndexBvdIdNumYear & IndexMinTotalAssets)=1;
        else
            N_duplicates = sum(IndexBvdIdNumYear & IndexMinTotalAssets);
            Data.Remove(find(IndexBvdIdNumYear & IndexMinTotalAssets,N_duplicates-1))=1;
        end
    end
end
Data(Data.Remove==1,:)=[];
Data.Remove=[];

end

function [Data,FiscalYearChange] = checkWhetherFiscalYearChange(Data,i)
% Check whether it is a fiscal year change, by calculating whether the
% number of days and the number of months seem reasonable. If there are
% less than approximately 30 days per month, the reporting overlaps and it
% is not a fiscal year change but just double reporting.

N_days_per_month = (Data.CloseDateDN(i)-Data.CloseDateDN(i-1))/Data.Numberofmonths(i);
N_days_per_month_wrt_i_minus2 = (Data.CloseDateDN(i)-Data.CloseDateDN(i-2))/Data.Numberofmonths(i);
if ~(N_days_per_month<25 || N_days_per_month>34)
    FiscalYearChange=1;
elseif ~(N_days_per_month_wrt_i_minus2<25 || N_days_per_month_wrt_i_minus2>34)
    FiscalYearChange=1;
    Data.Remove(i-1)= 1;
else
    FiscalYearChange=0;
end

end