function Data = removeFirmsWithMoreStatementsPerYear(Data)
% This function removes BankscopeIndexNumbers for which the reporting date
% switches 

% Find indices that have more than 1 report per year
Indices = findIndexWithMoreThan1ReportPerYear(Data);

% Remove indices that switch fiscal year 
Data(ismember(Data.BankScopeIndexNumber,Indices),:)=[];

end

function Indices = findIndexWithMoreThan1ReportPerYear(DataTemp)

%  Find Fiscal Year switches defined by obs: 
% - being less than 1 year apart OR
% - (being more than 1 year AND less than 2 years apart) AND 
% - having the same BankscopeIndexNumber
DataTemp = sortrows(DataTemp,'CloseDateDN','ascend');
DataTemp = sortrows(DataTemp,'BankScopeIndexNumber','ascend');
DataTemp.CloseDateDN_LastYear = [NaN;DataTemp.CloseDateDN(1:end-1)];
DataTemp.DaysBetweenCloseDates = DataTemp.CloseDateDN-DataTemp.CloseDateDN_LastYear;
DataTemp.FiscalYearSwitch = (DataTemp.DaysBetweenCloseDates<360 | (DataTemp.DaysBetweenCloseDates>370 & DataTemp.DaysBetweenCloseDates<720)) & DataTemp.BankScopeIndexNumber==[NaN;DataTemp.BankScopeIndexNumber(1:end-1)];

% Find indices
Indices = DataTemp.BankScopeIndexNumber(DataTemp.FiscalYearSwitch==1);



end