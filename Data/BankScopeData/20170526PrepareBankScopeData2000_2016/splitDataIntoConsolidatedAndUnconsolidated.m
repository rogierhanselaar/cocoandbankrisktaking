function [DataU,DataC] = splitDataIntoConsolidatedAndUnconsolidated(Data,yearVar)

% Create a Dataset with unconsolidated data
DataU = removeDuplicateStatementsUnconsolidated(Data,yearVar);

% Create a Dataset with consolidated data
DataC = removeDuplicateStatementsConsolidated(Data,yearVar);


end



function Data = removeDuplicateStatementsUnconsolidated(Data,yearVar)

% Keep only U1/U2/U*/C1: use the unconsolidated balance sheet data. We
% are interested in balance sheet sensitivy. We make sure that the effects
% we are after are not diluted by using consolidated statements. Moreover,
% looking at unconsolidated statements keeps the sample large.
Data(~(strcmpi(Data.ConsolidationCode,'U*') | strcmpi(Data.ConsolidationCode,'U1') | strcmpi(Data.ConsolidationCode,'U2') | strcmpi(Data.ConsolidationCode,'C1')),:)=[];


% Remove BvDIDNumbers FiscalYear duplicates due to consolidation duplicates,
% favoring U1/U2 over U*.
Data.ConsolidationCode(strcmpi(Data.ConsolidationCode,'U*')) = {'Ustar'};
DataUnstacked = Data(:,{'BvDIDNumbers',yearVar{1},'ConsolidationCode','BankScopeIndexNumber'});
DataUnstacked = unstack(DataUnstacked,'BankScopeIndexNumber','ConsolidationCode');
DataUnstacked.RemoveUstarInFavorOfU1 = ~isnan(DataUnstacked.U1) & ~isnan(DataUnstacked.Ustar);
DataUnstacked.RemoveUstarInFavorOfU2 = ~isnan(DataUnstacked.U2) & ~isnan(DataUnstacked.Ustar);
Data = outerjoin(Data,DataUnstacked,...
    'Type','Left',...
    'LeftKeys',{'BvDIDNumbers',yearVar{1}},...
    'RightKeys',{'BvDIDNumbers',yearVar{1}},...
    'RightVariables',{'RemoveUstarInFavorOfU1','RemoveUstarInFavorOfU2'});
Data(Data.RemoveUstarInFavorOfU1==1 & strcmpi(Data.ConsolidationCode,'Ustar'),:)=[];
Data(Data.RemoveUstarInFavorOfU2==1 & strcmpi(Data.ConsolidationCode,'Ustar'),:)=[];
Data.RemoveUstarInFavorOfU1 = [];
Data.RemoveUstarInFavorOfU2 = [];

end


function Data = removeDuplicateStatementsConsolidated(Data,yearVar)

% Keep only C1/C2/C*/U1: use the consolidated balance sheet data. As CoCo's
% are issued by headquarters, the relevant balance sheet to look at for an
% effect is the balance sheet the headquarter looks at, i.e. the
% consolidated balance sheet
Data(~(strcmpi(Data.ConsolidationCode,'C*') | strcmpi(Data.ConsolidationCode,'C1') | strcmpi(Data.ConsolidationCode,'C2') | strcmpi(Data.ConsolidationCode,'U1')),:)=[];

% Remove BvDIDNumbers FiscalYear duplicates due to Consolidation duplicates,
% favoring C1/C2 over C*.
Data.ConsolidationCode(strcmpi(Data.ConsolidationCode,'C*')) = {'Cstar'};
DataUnstacked = Data(:,{'BvDIDNumbers',yearVar{1},'ConsolidationCode','BankScopeIndexNumber'});
DataUnstacked = unstack(DataUnstacked,'BankScopeIndexNumber','ConsolidationCode');
DataUnstacked.RemoveCstarInFavorOfC1 = ~isnan(DataUnstacked.C1) & ~isnan(DataUnstacked.Cstar);
DataUnstacked.RemoveCstarInFavorOfC2 = ~isnan(DataUnstacked.C2) & ~isnan(DataUnstacked.Cstar);
Data = outerjoin(Data,DataUnstacked,...
    'Type','Left',...
    'LeftKeys',{'BvDIDNumbers',yearVar{1}},...
    'RightKeys',{'BvDIDNumbers',yearVar{1}},...
    'RightVariables',{'RemoveCstarInFavorOfC1','RemoveCstarInFavorOfC2'});
Data(Data.RemoveCstarInFavorOfC1==1 & strcmpi(Data.ConsolidationCode,'Cstar'),:)=[];
Data(Data.RemoveCstarInFavorOfC2==1 & strcmpi(Data.ConsolidationCode,'Cstar'),:)=[];
Data.RemoveCstarInFavorOfC1 = [];
Data.RemoveCstarInFavorOfC2 = [];

% Remove banks that do not have entitytype equal to GUO, single location,
% or independent company. Entitytype is a static variable, implying that
% this filter will also remove firms that were independent at one point
% before being acquired. As the firms issuing CoCo will be firms that are
% independent anyway, any selection bias in the control group because of
% this will not matter.
% !!! This one reduces the number of ISINs with about 30% !!!
Data(~(strcmpi(Data.Entitytype,'GUO') | strcmpi(Data.Entitytype,'Independent co') | strcmpi(Data.Entitytype,'Single location')),:)=[];


end