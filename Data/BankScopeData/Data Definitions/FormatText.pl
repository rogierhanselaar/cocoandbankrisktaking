#!/usr/bin/perl

use strict;
use warnings;
$| = 1;

open(DATA, "<RawUnformatted.txt") or die "Couldn't open file RawUnformatted.txt, $!";
open(my $DATAFORMATTED, '>','Formatted.txt') or die "Couldn't open file Formatted.txt, $!";

# Find location of data item codes
while(my $str = <DATA>){
    my @matches = ();
    my @startloc = ();
    my @endloc = ();
    my $counter = 0;
    while ($str =~ m/(\d{5})/g){
        $counter = $counter+1;
        $matches[$counter] = $1;
        $startloc[$counter] = $-[0];
        $endloc[$counter] = $+[0];        
        
        if ($counter<11){
            print "$counter\n";
            print "$matches[$counter] is found\n";
            print "It is found at location: $startloc[$counter]-$endloc[$counter]\n";
        }
    }
    my $numberofmatches = $counter;
    my $inext = 0;
    my $stringtoinsert = ();
    for (my $i=1; $i <= $numberofmatches; $i++) {
        $inext = $i+1;
        if ($inext>$numberofmatches) {
            $stringtoinsert = substr($str,$endloc[$i]);
        }
        else{
            $stringtoinsert = substr($str,$endloc[$i],$startloc[$inext]-$endloc[$i]);
        }
        if ($stringtoinsert =~ m/Includes/) {
            my $stringtoinsertpart1 = substr($stringtoinsert,0,$-[0]);
            my $stringtoinsertpart2 = substr($stringtoinsert,$-[0]);
            $stringtoinsert = $stringtoinsertpart1."\t".$stringtoinsertpart2;
        }
        print $DATAFORMATTED "$matches[$i]\t$stringtoinsert\n";  
    }
}
close $DATAFORMATTED;
