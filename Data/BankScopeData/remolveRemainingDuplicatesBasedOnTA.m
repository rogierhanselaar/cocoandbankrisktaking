function Data = remolveRemainingDuplicatesBasedOnTA(Data,yearVar) 
% Should work with BvDIDNumber or Index? --> BvDIDNumber is fine. The
% duplicates arise because of multiple reports in different currencies or
% using different accounting standards.
[~,ind] = unique(Data(:,{(yearVar{1}),'BvDIDNumbers'}));
duplicates = setdiff(1:size(Data,1),ind);
Data.Remove = NaN(size(Data,1),1);
for i = duplicates
    IndexBvdIdNumYear = Data.BvDIDNumbers==Data.BvDIDNumbers(i) & Data.(yearVar{1})==Data.(yearVar{1})(i);
    minTotalAssets = min(Data.TotalAssets(IndexBvdIdNumYear));
    IndexMinTotalAssets = Data.TotalAssets==minTotalAssets; 
    if sum(IndexBvdIdNumYear & IndexMinTotalAssets)==1
        Data.Remove(IndexBvdIdNumYear & IndexMinTotalAssets)=1;
    else 
        N_duplicates = sum(IndexBvdIdNumYear & IndexMinTotalAssets);
        Data.Remove(find(IndexBvdIdNumYear & IndexMinTotalAssets,N_duplicates-1))=1;
    end
end
Data(Data.Remove==1,:)=[];




end