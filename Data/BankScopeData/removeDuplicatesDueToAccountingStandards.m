function Data = removeDuplicatesDueToAccountingStandards(Data,yearVar)

[~,ind] = unique(Data(:,{yearVar{1},'BvDIDNumbers'}));
duplicates = setdiff(1:size(Data,1),ind);
Data.Remove = NaN(size(Data,1),1);
for i = duplicates
    IndexBvDIDNumbers = Data.BvDIDNumbers==Data.BvDIDNumbers(i);
    IndexYearBvDIDNumbers = IndexBvDIDNumbers & Data.(yearVar{1})==Data.(yearVar{1})(i);
    if size(unique(Data.AccountingStandards(IndexYearBvDIDNumbers)),1)>1 % if duplicate exist because of reporting in multiple currencies
        AccSt = unique(Data.AccountingStandards(IndexBvDIDNumbers));
        AccStFreq = NaN(size(AccSt,1),1);
        for AcS = AccSt'
            AccStFreq(strcmpi(AccSt,AcS{1}),1) = sum(strcmpi(Data.AccountingStandards(IndexBvDIDNumbers),AcS{1}));
        end
        MainAccSt = AccSt(AccStFreq(:,1)==max(AccStFreq),1);
        Data.Remove(IndexYearBvDIDNumbers & ~strcmpi(Data.AccountingStandards,MainAccSt{1}))=1;
    end
end
Data(Data.Remove==1,:)=[];
Data.Remove=[];

end