function Data = removeDuplicateReportsPerYear(Data)

% For each firm (determined by BankScopeIndexNumber) with more than one report per fiscal
% year, find the report that is closest to 31 Dec, and remove the others
Data.DaysAwayFromDec31 = NaN(size(Data,1),1);
Data.Remove = NaN(size(Data,1),1);
for indexFirm = unique(Data.BankScopeIndexNumber(Data.MoreThanOneReportPerFiscalYear==1))'
    for fiscalYear = unique(Data.FiscalYear(Data.BankScopeIndexNumber==indexFirm & Data.MoreThanOneReportPerFiscalYear==1))'
        IndexIndYear = Data.BankScopeIndexNumber==indexFirm & Data.FiscalYear==fiscalYear;
        Data.DaysAwayFromDec31(IndexIndYear) = ...
            Data.CloseDateDN(IndexIndYear)...
            - datenum(strcat(cellstr(num2str(Data.FiscalYear(IndexIndYear))),'1231'),'yyyymmdd'); % calculate the number of days between the dates and 31 dec
        if size(min(abs(Data.DaysAwayFromDec31(IndexIndYear))))~=[1 1]
            error('There seem to be two observations for a specific country and fiscal year exactly the same number of days away from 31 dec of the fiscal year')
        end
        Data.Remove(IndexIndYear & Data.DaysAwayFromDec31~=min(abs(Data.DaysAwayFromDec31(IndexIndYear))))=1; % Mark for removal all obs that are not the least days away
    end
end
Data(Data.Remove==1,:)=[];
Data.MoreThanOneReportPerFiscalYear=[];Data.Remove=[];Data.DaysAwayFromDec31=[];



end