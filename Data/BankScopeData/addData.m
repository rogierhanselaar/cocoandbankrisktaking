function Data = addData(Data,FXData,variableNames,vars_level)
filename = 'C:\Users\rogie\Dropbox\PhD\Data\ProjectsWithProfPurnanandam_RogierHanselaar\CoCosAndRiskTaking\Data\BankScopeData\FullDatasetBankscope\bankscopefinancials.csv';
numlines = str2double(perl('countlines.pl', filename));
batchSize = 10000;
iterations = floor(numlines/batchSize);
bankscopefinancials = table();
h = waitbar(0,'Starting with first iteration');
for i = 1:iterations
    waitbar(i/iterations,h,strcat('Iteration:',num2str(i),' of ',num2str(iterations+1)))
    startRow = 2 + (i-1)*batchSize;
    endRow = startRow+batchSize-1;
    tempData = loadBankscopeFinancialsFull(filename,startRow,endRow);
    tempData = fixDateVariables(tempData(tempData.closdate_year>=2009,[{'index','bvdidnum','closdate','closdate_year','unit','currency'} variableNames]));
    bankscopefinancials = [bankscopefinancials;tempData];
end
startRow = 2 + iterations*batchSize;
endRow = numlines;
waitbar(i/(iterations+1),h,'Starting with final iteration')
tempData = loadBankscopeFinancialsFull(filename,startRow,endRow);
tempData = fixDateVariables(tempData(tempData.closdate_year>=2009,[{'index','bvdidnum','closdate','closdate_year','unit','currency'} variableNames]));
bankscopefinancials = [bankscopefinancials;tempData];
close(h)

% Rename bvdidnum into BvDIDNumbers
bankscopefinancials.Properties.VariableNames{'bvdidnum'} = 'BvDIDNumbers';

% Rename currency into Currency
bankscopefinancials.Properties.VariableNames{'currency'} = 'Currency';

% Rename unit into StatementUnit
bankscopefinancials.Properties.VariableNames{'unit'} = 'StatementUnit';

% Only keep those observations that are also in Data
bankscopefinancials = outerjoin(bankscopefinancials,Data,...
    'Type','Left',...
    'LeftKeys',{'index','ClosingDate'},...
    'RightKeys',{'BankScopeIndexNumber','ClosingDate'},...
    'RightVariables','BankScopeIndexNumber');
bankscopefinancials(isnan(bankscopefinancials.BankScopeIndexNumber),:)=[];

% Convert cell to double for those variables that are numeric
bankscopefinancials = convertCellToDouble(bankscopefinancials,variableNames);

% Convert currency to EUR for level variables and adjust units
[bankscopefinancials,vars_level2] = assureUniformCurrenciesForEachCompany(bankscopefinancials,{'YearpartofCLOSDATE'},FXData,vars_level);
variableNames = [setdiff(variableNames,vars_level) vars_level2];
% Merge the additional items into Data
Data = outerjoin(Data,bankscopefinancials,...
    'Type','Left',...
    'LeftKeys',{'BankScopeIndexNumber','ClosingDate'},...
    'RightKeys',{'index','ClosingDate'},...
    'RightVariables',variableNames);

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%% Helper Functions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function bankscopefinancials = fixDateVariables(bankscopefinancials)

bankscopefinancials.CloseDateDN = datenum(bankscopefinancials.closdate,'ddmmmyyyy');
bankscopefinancials.ClosingDate = year(bankscopefinancials.CloseDateDN)*10000+month(bankscopefinancials.CloseDateDN)*100+day(bankscopefinancials.CloseDateDN);
bankscopefinancials.YearpartofCLOSDATE = year(bankscopefinancials.CloseDateDN);
bankscopefinancials.closdate=[];bankscopefinancials.closdate_year=[];


end

function bankscopefinancials = convertCellToDouble(bankscopefinancials,variableNames)

for v = variableNames
    if iscell(bankscopefinancials.(v{1}))
        bankscopefinancials.(v{1}) = str2double(bankscopefinancials.(v{1}));
    end
end

end