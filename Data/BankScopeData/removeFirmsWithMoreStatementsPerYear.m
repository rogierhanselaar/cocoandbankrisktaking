function Data = removeFirmsWithMoreStatementsPerYear(Data)
% Find indices that have more than 1 report per year
Data = findIndexWithMoreThan1ReportPerYear(Data);

% Remove indices that switch fiscal year
Indices  = Data.BankScopeIndexNumber(Data.MoreThanOneReportPerYear>0);
Data(ismember(Data.BankScopeIndexNumber,Indices),:)=[];
Data.MoreThanOneReportPerYear=[];

end

function Data = findIndexWithMoreThan1ReportPerYear(Data)

Data.CloseMonth = cellstr(num2str(month(Data.CloseDateDN)));
Data.CloseMonth = strrep(Data.CloseMonth,' ','');
Data.CloseMonth = categorical(strcat('x',Data.CloseMonth));
DataUnstacked = Data(:,{'BankScopeIndexNumber','ClosingDate','YearpartofCLOSDATE','CloseMonth'});
DataUnstacked = unstack(DataUnstacked,'ClosingDate','CloseMonth');
DataUnstacked.MoreThanOneReportPerYear = sum(~isnan(table2array(DataUnstacked(:,3:end))),2)>1;
varsToStack = cellstr(strcat('x',num2str([1:12]')))';
varsToStack = strrep(varsToStack,' ','');
DataUnstacked = stack(DataUnstacked,varsToStack);
DataUnstacked.Properties.VariableNames(4:5) = {'CloseMonth','ClosingDate'};
DataUnstacked(isnan(DataUnstacked.ClosingDate),:)=[];
Data = outerjoin(Data,DataUnstacked,...
    'Type','Left',...
    'LeftKeys',{'BankScopeIndexNumber','YearpartofCLOSDATE','CloseMonth'},...
    'RightKeys',{'BankScopeIndexNumber','YearpartofCLOSDATE','CloseMonth'},...
    'RightVariables','MoreThanOneReportPerYear');
Data.CloseMonth=[];



end