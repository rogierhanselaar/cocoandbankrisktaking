*USE NEW DATA PROVIDED BY ROGIER JUNE 22
*housekeep
	*set matsize 11000
	set more off
	global datax "C:\Users\rogie\Dropbox\PhD\Data\ProjectsWithProfPurnanandam_RogierHanselaar\CoCosAndRiskTaking\Data\DataForCoAuthors"
	global datay "C:\Users\rogie\Dropbox\PhD\Data\ProjectsWithProfPurnanandam_RogierHanselaar\CoCosAndRiskTaking\Data\"
	*	global datax "C:\Users\amiyatos\Dropbox\ProjectsWithProfPurnanandam_RogierHanselaar\CoCosAndRiskTaking\Data\20170224DataForCoAuthors"
	global bankid bvdidnumbers
	
**************************************************************************************************************************************************************************************************************************************
**************************************************************************************************************************************************************************************************************************************
**************************************************************************************************************************************************************************************************************************************
*read in control firms (constructed by Rogier, see description in Excel file in Sumstats folder)
	insheet using ${datax}\\DataMatchedWithoutTradingVars.csv, clear
	keep ${bankid} controlfirms_n_b3
	sort ${bankid}
	by ${bankid}: keep if _n==1
	rename controlfirms_n_b3 control_rogier

*read in data and merge in control firms
	insheet using ${datax}\\20170823DataConsolidated_2000_2016.csv, clear

*	gen year=yearpartofclosdate
	gen year=fiscalyear
	drop isocode
	rename countryisocode isocode
	
	*TREATMENT YEARS
		replace dummyfiscaltreatment=1 if isocode=="CH"&year>=1999
		replace dummyfiscaltreatment=1 if isocode=="ES"&year>=2000
		replace dummyfiscaltreatment=1 if isocode=="SE"&year>=2000
		replace dummyfiscaltreatment=1 if isocode=="NO"&year>=2000
		replace dummyfiscaltreatment=1 if isocode=="DK"&year>=2004
		replace dummyfiscaltreatment=1 if isocode=="GB"&year>=2013
		replace dummyfiscaltreatment=1 if isocode=="BE"&year>=2013
		replace dummyfiscaltreatment=1 if isocode=="NL"&year>=2014
		replace dummyfiscaltreatment=1 if isocode=="DE"&year>=2014
	
	*match controls
	sort ${bankid}
	merge m:1 ${bankid} using ${datax}\\controls
	keep if _merge!=2
	drop _merge

*create some more vars: dummy_coco a dummy equal one if firm has coco in year t or thereafter
	*first year of coco issuance
		sort ${bankid}
*		by ${bankid}: egen min_year=min(yearpartofclosdate) if cocoissued!=.&cocoissued>0
		by ${bankid}: egen min_year=min(fiscalyear) if cocoissued!=.&cocoissued>0
		by ${bankid}: egen min_year1=mean(min_year)

		gen dummy_coco=0
*		replace dummy_coco=1 if min_year1<=yearpartofclosdate&min_year1!=.
		replace dummy_coco=1 if min_year1<=fiscalyear&min_year1!=.
		rename cocoissued n_cocos
		drop min_year*
		
*additional LHS: relative size of coco issuance
*	rename totalassets_eur x
*	gen totalassets_eur=real(x)
*	drop x
	
	rename cocoissuedamountissued_eur_cumsu c
	gen cocoissuedamountissued_eur_cumsu=real(c)
	replace cocoissuedamountissued_eur_cumsu=0 if cocoissuedamountissued_eur_cumsu==.
	drop c

	global levlistx leverage3 leverage2 leverage
	foreach x of global levlistx{
		rename `x' x
		gen `x'=real(x)
		drop x
	}

	gen rel_coco_size=cocoissuedamountissued_eur_cumsu/totalassets_eur*100
*	gen rel_coco_size=cocoissuedamountissued_eur/totalassets_eur*100

	
		
*leverage defined as A/E ~ turn into D/A, knowing A
	gen equity=totalassets*(1-leverage3)
	gen debt=totalassets-equity
	gen leverage_D_over_A=debt/totalassets
	rename leverage leverage_A_over_E

*turn to real
		rename impairedloansslashgrossloans impairedloansgrossloans
		rename loanlossprovslashnetintrev loanlossprovnetintrev
		rename loanlossresslashgrossloans loanlossresgrossloans

		global varlistx impairedloansgrossloans loanlossprovnetintrev loanlossresgrossloans cocoissuedamountissued_eur 
		foreach x of global varlistx{
			rename `x' yyy
			gen `x'=real(yyy)
			drop yyy
		}
	
			
*logs
	gen ln_assets=ln(1+totalassets_eur)
	drop leverage2 leverage3
		
	*winsorize
		global varlistx impairedloansgrossloans loanlossprovnetintrev loanlossresgrossloans leverage_A_over_E leverage_D_over_A
		foreach x of global varlistx{
			egen p1=pctile(`x'), p(1)
			egen p99=pctile(`x'), p(99)
			gen `x'_w=`x' if `x'<p99&`x'>p1
			gen `x'_t=`x' if `x'<p99&`x'>p1
			replace `x'_w=p1 if `x'<=p1&`x'!=.
			replace `x'_w=p99 if `x'>=p99&`x'!=.
			drop p1 p99
		}
		
	*sanity checks
		replace leverage_D_over_A=. if leverage_D_over_A>1
