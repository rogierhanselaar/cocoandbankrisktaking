function calculateTSLSForAllRelevantRatios(Data,VarToBeFitted,Xvars,XvarToFitWith,FE,clustering)

% ASSET QUALITY
% Loan loss reserves / gross loans --> + **
Yvar = {'LoanLossResGrossLoans'};
[resultsIVLoanLossResGrossLoans, res, Rsqrd, Obs] = calculateTSLSRegression(Data,Yvar,VarToBeFitted,Xvars,XvarToFitWith,FE,clustering);

% Loan Loss Provision / Net Interest Revenue --> + *
Yvar = {'LoanLossProvNetIntRev'};
[resultsIVLoanLossProvNetIntRev, res, Rsqrd, Obs] = calculateTSLSRegression(Data,Yvar,VarToBeFitted,Xvars,XvarToFitWith,FE,clustering);

% Loan Loss Reserves / Impaired Loans --> ~
Yvar = {'LoanLossResImpairedLoans'};
[resultsIVLoanLossResImpairedLoans, res, Rsqrd, Obs] = calculateTSLSRegression(Data,Yvar,VarToBeFitted,Xvars,XvarToFitWith,FE,clustering);

% Impaired Loans / Gross Loans
Yvar = {'ImpairedLoansGrossLoans'};
[resultsIVImpairedLoansGrossLoans, res, Rsqrd, Obs] = calculateTSLSRegression(Data,Yvar,VarToBeFitted,Xvars,XvarToFitWith,FE,clustering);

% NCO / Average Gross Loans
Yvar = {'NCOAverageGrossLoans'};
[resultsIVNCOAverageGrossLoans, res, Rsqrd, Obs] = calculateTSLSRegression(Data,Yvar,VarToBeFitted,Xvars,XvarToFitWith,FE,clustering);

% NCO / Net Income Before Loan Loss Provision
Yvar = {'NCONetIncBefLnLssProv'};
[resultsIVNCONetIncBefLnLssProv, res, Rsqrd, Obs] = calculateTSLSRegression(Data,Yvar,VarToBeFitted,Xvars,XvarToFitWith,FE,clustering);


% OPERATIONS
% Net Interest Margin
Yvar = {'NetInterestMargin'};
[resultsIVNetInterestMargin, res, Rsqrd, Obs] = calculateTSLSRegression(Data,Yvar,VarToBeFitted,Xvars,XvarToFitWith,FE,clustering);

% Net Interest Income / Avg Assets
Yvar = {'NetInterestIncomeAverageEarningAssets'};
[resultsIVNetInterestIncomeAverageEarningAssets, res, Rsqrd, Obs] = calculateTSLSRegression(Data,Yvar,VarToBeFitted,Xvars,XvarToFitWith,FE,clustering);

% Other Operating Income / Average Assets
Data.OtherOperatingIncomeAssets = Data.OtherOperatingIncome./Data.TotalAssets;
Yvar = {'OtherOperatingIncomeAssets'};
[resultsIVOtherOperatingIncomeAssets, res, Rsqrd, Obs] = calculateTSLSRegression(Data,Yvar,VarToBeFitted,Xvars,XvarToFitWith,FE,clustering);

% Non Interest Expense / Average Assets --> + **
Yvar = {'NonInterestExpenseAverageAssets'};
[resultsIVNonInterestExpenseAverageAssets, res, Rsqrd, Obs] = calculateTSLSRegression(Data,Yvar,VarToBeFitted,Xvars,XvarToFitWith,FE,clustering);

% Pre-tax operating income / Average Assets
Yvar = {'PreTaxOpIncAvgAssets'};
[resultsIVPreTaxOpIncAvgAssets, res, Rsqrd, Obs] = calculateTSLSRegression(Data,Yvar,VarToBeFitted,Xvars,XvarToFitWith,FE,clustering);

% Non operating items and taxes / Average Assets
Yvar = {'NonOpItemsTaxesAvgAst'};
[resultsIVNonOpItemsTaxesAvgAst, res, Rsqrd, Obs] = calculateTSLSRegression(Data,Yvar,VarToBeFitted,Xvars,XvarToFitWith,FE,clustering);

% Return on Average Assets
Yvar = {'ReturnOnAvgAssetsROAA'};
[resultsIVReturnOnAvgAssetsROAA, res, Rsqrd, Obs] = calculateTSLSRegression(Data,Yvar,VarToBeFitted,Xvars,XvarToFitWith,FE,clustering);

% Dividend Pay-out
Yvar = {'DividendPayOut'};
[resultsIVDividendPayOut, res, Rsqrd, Obs] = calculateTSLSRegression(Data,Yvar,VarToBeFitted,Xvars,XvarToFitWith,FE,clustering);

% Non operating items / net income --> + *
Yvar = {'NonOpItemsNetIncome'};
[resultsIVNonOpItemsNetIncome, res, Rsqrd, Obs] = calculateTSLSRegression(Data,Yvar,VarToBeFitted,Xvars,XvarToFitWith,FE,clustering);

% Cost to income ratio
Yvar = {'CostToIncomeRatio'};
[resultsIVCostToIncomeRatio, res, Rsqrd, Obs] = calculateTSLSRegression(Data,Yvar,VarToBeFitted,Xvars,XvarToFitWith,FE,clustering);

% Recurring Earning Power
Yvar = {'RecurringEarningPower'};
[resultsIVRecurringEarningPower, res, Rsqrd, Obs] = calculateTSLSRegression(Data,Yvar,VarToBeFitted,Xvars,XvarToFitWith,FE,clustering);


% LIQUIDITY
% Interbank Ratio
Yvar = {'InterbankRatio'};
[resultsIVInterbankRatio, res, Rsqrd, Obs] = calculateTSLSRegression(Data,Yvar,VarToBeFitted,Xvars,XvarToFitWith,FE,clustering);

% Net Loans / Total Assets
Yvar = {'NetLoansTotAssets'};
[resultsIVNetLoansTotAssets, res, Rsqrd, Obs] = calculateTSLSRegression(Data,Yvar,VarToBeFitted,Xvars,XvarToFitWith,FE,clustering);

% Net Loans / Deposits and ...(?)
Yvar = {'NetLoansDepSTFunding'};
[resultsIVNetLoansDepSTFunding, res, Rsqrd, Obs] = calculateTSLSRegression(Data,Yvar,VarToBeFitted,Xvars,XvarToFitWith,FE,clustering);

% Net Loans / Deposits and Borrowings (but no capital instruments)
Yvar = {'NetLoansTotDepBor'};
[resultsIVNetLoansTotDepBor, res, Rsqrd, Obs] = calculateTSLSRegression(Data,Yvar,VarToBeFitted,Xvars,XvarToFitWith,FE,clustering);

% Liquid Assets / Deposits and ...(?)
Yvar = {'LiquidAssetsDepSTFunding'};
[resultsIVLiquidAssetsDepSTFunding, res, Rsqrd, Obs] = calculateTSLSRegression(Data,Yvar,VarToBeFitted,Xvars,XvarToFitWith,FE,clustering);

% Liquid Assets / Deposits and Borrowings (but no capital instruments)
Yvar = {'LiquidAssetsTotDepBor'};
[resultsIVLiquidAssetsTotDepBor, res, Rsqrd, Obs] = calculateTSLSRegression(Data,Yvar,VarToBeFitted,Xvars,XvarToFitWith,FE,clustering);



end