clear

%% Load data
pcinfo=java.net.InetAddress.getLocalHost;
if ~strcmpi(pcinfo.getHostName,'LAPTOP-6P6G4FB7')
    error('Havent set the path yet for the dell laptop');
%     pathname = '';
else
    pathname = 'C:\Users\rogie\Git\cocoandbankrisktaking\Code\';
end
addpath(pathname);
[Data,DataWithoutLloyds] = loadData();

%% Run simple first stage linear regressions
Yvar = {'DummyOneOrMoreCoCosIssued'};
Xvars = {'DummyFiscalTreatment'};
% FE = {'BankID'};
FE = {};

% Does tax deductibility affect whether a bank issues one or more CoCos?
[results,R2,Fstat,Pval,N] = runSimpleRegressions(Data,Yvar,Xvars,FE);
[results,R2,Fstat,Pval,N] = runSimpleRegressions(DataWithoutLloyds,Yvar,Xvars,FE);
% --> Yes, it does; see significant Tstats and Fstat.

% How many CoCos are issued on average per year due to tax deductibility?
Yvar = {'CoCoIssued'};
Xvars = {'DummyFiscalTreatment'};
[results,R2,Fstat,Pval,N] = runSimpleRegressions(Data,Yvar,Xvars,FE);
[results,R2,Fstat,Pval,N] = runSimpleRegressions(DataWithoutLloyds,Yvar,Xvars,FE);
% --> On average, 0.46 CoCo's per year are issued by the firms in the
% sample when interest payments become deductible. (note: Lloyds needs to
% be excluded as it messes up all analyses with its 46(!) CoCo issues;
% check histogram to see why this is necessary)

% How much in dollars is issued on average per year due to tax
% deductibility?
Yvar = {'CoCoAmountIssued'};
Xvars = {'DummyFiscalTreatment'};
[results,R2,Fstat,Pval,N] = runSimpleRegressions(Data,Yvar,Xvars,FE);
[results,R2,Fstat,Pval,N] = runSimpleRegressions(DataWithoutLloyds,Yvar,Xvars,FE);

DataWithoutLloyds.LogCoCoAmountIssued = log(DataWithoutLloyds.CoCoAmountIssued);
DataWithoutLloyds.LogCoCoAmountIssued(DataWithoutLloyds.LogCoCoAmountIssued<1)=0;
Yvar = {'LogCoCoAmountIssued'};
Xvars = {'DummyFiscalTreatment'};
[results,R2,Fstat,Pval,N] = runSimpleRegressions(DataWithoutLloyds,Yvar,Xvars,FE);
% --> On average, 440 million worth of CoCo's is issued per year by the
% firms in the sample when interest payments become deductible. (note:
% Lloyds needs to be excluded as it messes up all analyses with its 46(!)
% CoCo issues; check histogram to see why this is necessary). Results
% become stronger when using logs. 


%% Load IV regression function
pcinfo=java.net.InetAddress.getLocalHost;
if ~strcmpi(pcinfo.getHostName,'LAPTOP-6P6G4FB7')
    error('Pathname to tsls function not yet set for the dell laptop')
%     pathname2 = 'C:\Users\54167rha\Dropbox\PhD\Data\ProjectsWithProfPurnanandam_RogierHanselaar\CoCosAndRiskTaking\Data\';
else
    pathname2 = 'C:\Users\rogie\Git\usefulmatlabfunctions\';
end
addpath(pathname2)

%% IV settings 1:
display('IV Settings 1: Using a dummy that equals 1 when one or more CoCos have been issued')
VarToBeFitted = {'DummyOneOrMoreCoCosIssued'};
Xvars = {};
XvarToFitWith = {'DummyFiscalTreatment'};
FE = {};
% FE = {'BankID'};
clustering = {}; 
% clustering = {'BankID'};
calculateTSLSForAllRelevantRatios(DataWithoutLloyds,VarToBeFitted,Xvars,XvarToFitWith,FE,clustering)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Results:
% Loan loss reserves / gross loans --> + ***
% Loan Loss Provision / Net Interest Revenue --> + *
% Impaired Loans / Gross Loans --> + **
% Non Interest Expense / Average Assets --> + ***
% Non operating items / net income --> + *
%
% Results, with Firm FEs:
% Net Loans / Deposits and Borrowings (but no capital instruments) --> - *
% Non operating items and taxes / Average Assets --> + **
%
% Some comments:
% The effects that disappear when adding Firm FEs have a low number of data
% points (i.e. 143--189); perhaps results would be more robust if more data
% would be available.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% IV settings 2:
display('IV Settings 2: Using a variable that equals the number of CoCos issued in a specific year')
VarToBeFitted = {'CoCoIssued'};
Xvars = {};
XvarToFitWith = {'DummyFiscalTreatment'};
FE = {};
% FE = {'BankID'}; 
clustering = {}; 
% clustering = {'BankID'};
calculateTSLSForAllRelevantRatios(DataWithoutLloyds,VarToBeFitted,Xvars,XvarToFitWith,FE,clustering)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Results: 
% Loan loss reserves / gross loans --> + **
% Loan Loss Provision / Net Interest Revenue --> + *
% Non Interest Expense / Average Assets --> + **
% Non operating items / net income --> + *
%
% Results, with Firm FEs:
% Non operating items and taxes / Average Assets --> + **
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% IV settings 3:
display('IV Settings 3: Using a variable that equals the amount in dollars(?) of CoCos issued in a specific year')
VarToBeFitted = {'CoCoAmountIssued'};
Xvars = {};
XvarToFitWith = {'DummyFiscalTreatment'};
FE = {};
% FE = {'BankID'};
clustering = {}; 
% clustering = {'BankID'};
calculateTSLSForAllRelevantRatios(DataWithoutLloyds,VarToBeFitted,Xvars,XvarToFitWith,FE,clustering)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Results:
% Scaling issues according to Matlab
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% IV settings 4:
display('IV Settings 4: Using a variable that equals the logarithm o the amount in dollars(?) of CoCos issued in a specific year')
VarToBeFitted = {'LogCoCoAmountIssued'};
Xvars = {};
XvarToFitWith = {'DummyFiscalTreatment'};
FE = {};
% FE = {'BankID'};
clustering = {}; 
% clustering = {'BankID'};
calculateTSLSForAllRelevantRatios(DataWithoutLloyds,VarToBeFitted,Xvars,XvarToFitWith,FE,clustering)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Results:
% Loan loss reserves / gross loans --> + **
% Loan Loss Provision / Net Interest Revenue --> + *
% Non Interest Expense / Average Assets --> + **
% Non operating items / net income --> + *
%
% Results, with Firm FEs:
% Non operating items and taxes / Average Assets --> + **
% Net Loans / Deposits and Borrowings (but no capital instruments) --> - *
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

