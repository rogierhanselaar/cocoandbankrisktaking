function [OutputTableS1,OutputTableS2] = createIVTableWithResults(panelData,Yvar,EndogeneousVariables,Xvars,InstrumentalVariables)

%% Create table with a separate variable per iteration
% Create OutputTable
OutputTableS1 = createOutputTableStructureStage1(EndogeneousVariables,Xvars,InstrumentalVariables);
OutputTableS2 = createOutputTableStructureStage2(Yvar,EndogeneousVariables,Xvars,InstrumentalVariables);

%% Fill OutputTable with results
OutputTableS1 = fillOutputTableS1(panelData,OutputTableS1,Yvar,EndogeneousVariables,Xvars,InstrumentalVariables);
OutputTableS2 = fillOutputTableS2(panelData,OutputTableS2,Yvar,EndogeneousVariables,Xvars,InstrumentalVariables);

end

function OutputTable = createOutputTableStructureStage1(EndogeneousVariables,Xvars,Instruments)

% Initialize OutputTable
N_Xvars = size(Xvars,2);
N_EndVars = size(EndogeneousVariables,2);
N_Instruments = size(Instruments,2);
if size(EndogeneousVariables,2)>1
    error('OutputTable layout is not ready for more than EndogeneousVariable');
end
OutputTable = cell((11+N_Xvars+N_Instruments),2*N_Xvars+7);

% Set up header
OutputTable(1,1) = {'First stage of IV regression'};
OutputTable(2,1) = {'Endogeneous Variable:'};
OutputTable(2,2:1+size(EndogeneousVariables,2)) = EndogeneousVariables;

% Set up the variable name column
OutputTable(4,1) = {'Constant'};
for i = 1:N_Instruments
    OutputTable(4+i,1) = Instruments(i);
end
for i = 1:N_Xvars
    OutputTable(4+N_Instruments+i,1) = Xvars(i);
end

% Set up the FE rows
OutputTable(6+N_Xvars+N_Instruments,1) = {'Year FEs'};
OutputTable(7+N_Xvars+N_Instruments,1) = {'Bank FEs'};
OutputTable(6+N_Xvars+N_Instruments:7+N_Xvars+N_Instruments,[2:2:N_Xvars*2+2]) = repmat({'No'},2,3);
OutputTable(6+N_Xvars+N_Instruments:7+N_Xvars+N_Instruments,N_Xvars*2+4) = {'Yes','No'};
OutputTable(6+N_Xvars+N_Instruments:7+N_Xvars+N_Instruments,N_Xvars*2+6) = {'Yes','Yes'};

% Set up the number of observations row
OutputTable(9+N_Instruments+N_Xvars,1) = {'N_obs'};
OutputTable(10+N_Instruments+N_Xvars,1) = {'Fstat'};
OutputTable(11+N_Instruments+N_Xvars,1) = {'Pval'};

end

function OutputTable = createOutputTableStructureStage2(Yvar,EndogeneousVariables,Xvars,InstrumentalVariables)

% Initialize OutputTable
N_Xvars = size(Xvars,2);
N_EndVars = size(EndogeneousVariables,2);
OutputTable = cell((9+N_Xvars+N_EndVars),11);

% Set up header
OutputTable(1,1) = {'Second stage of IV regression'};
OutputTable(2,1) = {'Instruments:'};
OutputTable(2,2:1+size(InstrumentalVariables,2)) = InstrumentalVariables;
OutputTable(3,1) = {'Dependent Variable:'};
OutputTable(3,2) = Yvar;

% Set up the variable name column
for i = 1:N_EndVars
    OutputTable(4+i,1) = EndogeneousVariables(i);
end
OutputTable(5+N_EndVars,1) = {'Constant'};
for i = 1:N_Xvars
    OutputTable(5+N_EndVars+i,1) = Xvars(i);
end

% Set up the FE rows
OutputTable(7+N_Xvars+N_EndVars,1) = {'Year FEs'};
OutputTable(8+N_Xvars+N_EndVars,1) = {'Bank FEs'};
OutputTable(7+N_Xvars+N_EndVars:8+N_Xvars+N_EndVars,[2:2:N_Xvars*2+2]) = repmat({'No'},2,3);
OutputTable(7+N_Xvars+N_EndVars:8+N_Xvars+N_EndVars,N_Xvars*2+4) = {'Yes','No'};
OutputTable(7+N_Xvars+N_EndVars:8+N_Xvars+N_EndVars,N_Xvars*2+6) = {'Yes','Yes'};

% Set up the number of observations row
OutputTable(10+N_EndVars+N_Xvars,1) = {'N_obs'};

end

%%%%%%%%%%%%%%%%%%%%%%%%% Fill Table functions %%%%%%%%%%%%%%%%%%%%%%%%%%%%

function OutputTableS1 = fillOutputTableS1(panelData,OutputTableS1,Yvar,EndogeneousVariables,Xvars,Instruments)

% Initialize variables
N_Xvars = size(Xvars,2);
N_Instruments = size(Instruments,2);

% Fill in table
counter = 0;
for i = 1:3+N_Xvars
    
    % Determine first column number for the specification
    firstColNumber = 2+(i*2-2);
    
    % Run first stage
    IndexYNotNaN = ~isnan(panelData.(Yvar{1}));
    if i>1+N_Xvars
        if i>2+N_Xvars
            [resultsS1,~,Fstat,Pval,N,~] = runSimpleRegressions(panelData(IndexYNotNaN,:),EndogeneousVariables,[Instruments Xvars],{'FiscalYear','BvDIDNumbers'});
        else
            [resultsS1,~,Fstat,Pval,N,~] = runSimpleRegressions(panelData(IndexYNotNaN,:),EndogeneousVariables,[Instruments Xvars],{'FiscalYear'});
        end
    else
        [resultsS1,~,Fstat,Pval,N,~] = runSimpleRegressions(panelData(IndexYNotNaN,:),EndogeneousVariables,[Instruments Xvars(1:counter)],{});
    end
    
    % Fill in results for Xvars
    if i<=1+N_Xvars
        for j = 1:1+N_Instruments+counter
            OutputTableS1(3+j,firstColNumber) = {num2str(resultsS1.beta(j),'%.3f')};
            if abs(resultsS1.t_stat(j))>=2.576
                OutputTableS1(3+j,firstColNumber+1) = {'***'};
            elseif abs(resultsS1.t_stat(j))>=1.96
                OutputTableS1(3+j,firstColNumber+1) = {'**'};
            elseif abs(resultsS1.t_stat(j))>=1.645
                OutputTableS1(3+j,firstColNumber+1) = {'*'};
            end
        end
    else
        for j = 1:N_Instruments+counter-1
            OutputTableS1(4+j,firstColNumber) = {num2str(resultsS1.beta(j),'%.3f')};
            if abs(resultsS1.t_stat(j))>=2.576
                OutputTableS1(4+j,firstColNumber+1) = {'***'};
            elseif abs(resultsS1.t_stat(j))>=1.96
                OutputTableS1(4+j,firstColNumber+1) = {'**'};
            elseif abs(resultsS1.t_stat(j))>=1.645
                OutputTableS1(4+j,firstColNumber+1) = {'*'};
            end
        end
    end
    
    % Fill in number of observations
    OutputTableS1(9+N_Xvars+N_Instruments,firstColNumber) = {num2str(N,'%.0f')};
    OutputTableS1(10+N_Xvars+N_Instruments,firstColNumber) = {num2str(Fstat,'%.3f')};
    OutputTableS1(11+N_Xvars+N_Instruments,firstColNumber) = {num2str(Pval,'%.3f')};
    
    % Increment counter
    if i<=1+N_Xvars
        counter = counter+1;
    end
end





end

function OutputTableS2 = fillOutputTableS2(panelData,OutputTableS2,Yvar,EndogeneousVariables,Xvars,Instruments)

% Initialize variables
N_Xvars = size(Xvars,2);
N_Instruments = size(Instruments,2);

% Fill in table
counter = 0;
for i = 1:3+N_Xvars
    
    % Determine first column number for the specification
    firstColNumber = 2+(i*2-2);
    
    % Run first stage
    if i>1+N_Xvars
        if i>2+N_Xvars
            [resultsIV, N, ~, ~] = calculateTSLSRegression(panelData,Yvar,EndogeneousVariables,Xvars,Instruments,{'FiscalYear','BvDIDNumbers'});
        else
            [resultsIV, N, ~, ~] = calculateTSLSRegression(panelData,Yvar,EndogeneousVariables,Xvars,Instruments,{'FiscalYear'});
        end
    else
        [resultsIV, N, ~, ~] = calculateTSLSRegression(panelData,Yvar,EndogeneousVariables,Xvars(1:counter),Instruments,{});
    end
    
    % Fill in results for Xvars
    if i<=1+N_Xvars
        for j = 1:2+counter
            OutputTableS2(4+j,firstColNumber) = {num2str(resultsIV.b_IV(j),'%.3f')};
            if abs(resultsIV.t_stat(j))>=2.576
                OutputTableS2(4+j,firstColNumber+1) = {'***'};
            elseif abs(resultsIV.t_stat(j))>=1.96
                OutputTableS2(4+j,firstColNumber+1) = {'**'};
            elseif abs(resultsIV.t_stat(j))>=1.645
                OutputTableS2(4+j,firstColNumber+1) = {'*'};
            end
        end
    else % When FEs are included
        for j = 1:counter
            if j==1
                OutputTableS2(4+j,firstColNumber) = {num2str(resultsIV.b_IV(j),'%.3f')};
                if abs(resultsIV.t_stat(j))>=2.576
                    OutputTableS2(4+j,firstColNumber+1) = {'***'};
                elseif abs(resultsIV.t_stat(j))>=1.96
                    OutputTableS2(4+j,firstColNumber+1) = {'**'};
                elseif abs(resultsIV.t_stat(j))>=1.645
                    OutputTableS2(4+j,firstColNumber+1) = {'*'};
                end
            else
                OutputTableS2(5+j,firstColNumber) = {num2str(resultsIV.b_IV(j),'%.3f')};
                if abs(resultsIV.t_stat(j))>=2.576
                    OutputTableS2(5+j,firstColNumber+1) = {'***'};
                elseif abs(resultsIV.t_stat(j))>=1.96
                    OutputTableS2(5+j,firstColNumber+1) = {'**'};
                elseif abs(resultsIV.t_stat(j))>=1.645
                    OutputTableS2(5+j,firstColNumber+1) = {'*'};
                end
            end
        end
    end
        
    % Fill in number of observations
    OutputTableS2(11+N_Xvars,firstColNumber) = {num2str(N,'%.0f')};
    
    % Increment counter
    if i<=1+N_Xvars
        counter = counter+1;
    end
end




end