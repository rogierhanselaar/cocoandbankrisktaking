clear
%% Load data
pcinfo=java.net.InetAddress.getLocalHost;
if ~strcmpi(pcinfo.getHostName,'LAPTOP-6P6G4FB7')
    error('Havent set the path yet for the dell laptop');
%     pathname = '';
else
    pathname1 = 'C:\Users\rogie\Git\cocoandbankrisktaking\Code\';
    pathname2 = 'C:\Users\rogie\Git\usefulmatlabfunctions\';
end

% Load merged data
addpath(pathname1);
addpath(pathname2);
[DataIssuers,DataIssuersWithoutLloyds,DataNonIssuers] = loadData();
DataIssuers.CountryISOcode = categorical(DataIssuers.CountryISOcode);
Data = [DataIssuersWithoutLloyds;DataNonIssuers];

% Create some additional variables
Data.LogTotalAssets_EUR = log(Data.TotalAssets_EUR);
Data.Leverage3 = (Data.TotalLiabilities_EUR)./Data.TotalAssets_EUR;
Data.Leverage2 = (Data.TotalAssets_EUR-Data.TotalLiabilities_EUR)./Data.TotalAssets_EUR;
Data.Leverage = Data.TotalAssets_EUR./(Data.TotalAssets_EUR-Data.TotalLiabilities_EUR); Data.Leverage(isinf(Data.Leverage))=NaN;Data.Leverage(Data.Leverage<=0)=NaN;
Data.CashAndDueFromBanksOverTotalAssets = Data.CashAndDueFromBanks_EUR./Data.TotalAssets_EUR;
Data.NetInterestIncome_EUR(Data.NetInterestIncome_EUR<=0)=NaN;
Data.LogNetInterestIncome_EUR = log(Data.NetInterestIncome_EUR);
Data.LogCashAndDueFromBanks_EUR = log(Data.CashAndDueFromBanks_EUR);
Data.LogCashAndDueFromBanks_EUR(isinf(Data.LogCashAndDueFromBanks_EUR))=NaN;
Data.DummyBaselIII = Data.FiscalYear>=2011+0;
Data.DummyInteractionBaselIIIFiscalTreatment = Data.DummyBaselIII.*Data.DummyFiscalTreatment;

%% Run simple panel regressions without matching
% Yvars = {'LoanLossResGrossLoans','LoanLossProvNetIntRev','ImpairedLoansGrossLoans','NonInterestExpenseAverageAssets','NonOpItemsNetIncome','NonOpItemsTaxesAvgAst','NetLoansTotDepBor'};
Yvars = {'LoanLossProvNetIntRev','ImpairedLoansGrossLoans','LoanLossResGrossLoans'};
% Xvars = {'DummyOneOrMoreCoCosIssued','LogTotalAssets_EUR','Leverage','ReturnOnAvgAssetsROAA','CostToIncomeRatio','RecurringEarningPower'};
% Xvars = {'DummyHasIssuedCoCo','LogTotalAssets_EUR','Leverage'};
Xvars = {'DummyHasIssuedCoCoAtOrBelowMedianTrigger','LogTotalAssets_EUR','Leverage'};

performRegAndGenOutput(Data,Yvars,Xvars);

%% Run simple panel regressions with matching
load('Matching\TreatmentAndControlFirms.mat')
DataMatched = outerjoin(Data,TreatmentAndControlFirms,...
    'Type','Left',...
    'LeftKeys','BvDIDNumbers',...
    'RightKeys','BvDIDNumbers',...
    'RightVariables',{'TreatmentFirms','ControlFirms_N_b3'});
DataMatched(DataMatched.TreatmentFirms~=1 & DataMatched.ControlFirms_N_b3~=1,:)=[];

performRegAndGenOutput(DataMatched,Yvars,Xvars);

%% Run IV panel regression with matching
EndogeneousVariables = {'DummyHasIssuedCoCo'};
% InstrumentalVariables = {'DummyFiscalTreatment','DummyInteractionBaselIIIFiscalTreatment'}; 
InstrumentalVariables = {'DummyInteractionBaselIIIFiscalTreatment'}; 
Xvars = {'LogTotalAssets_EUR','Leverage'};
for y = Yvars
    [OutputTableS1,OutputTableS2] = createIVTableWithResults(DataMatched,y,EndogeneousVariables,Xvars,InstrumentalVariables);
%     [OutputTableS1,OutputTableS2] = createIVTableWithResults(Data,y,EndogeneousVariables,Xvars,InstrumentalVariables);
    resultsIV.(y{1}).stage1 = OutputTableS1;
    resultsIV.(y{1}).stage2 = OutputTableS2;
end
IV=1;
insertTablesIntoExcelSheet(Data,IV,resultsIV.(Yvars{1}).stage2,resultsIV.(Yvars{1}).stage1)
% insertTablesIntoExcelSheet(Data,IV,resultsIV.(Yvars{2}).stage2,resultsIV.(Yvars{2}).stage1)
% insertTablesIntoExcelSheet(Data,IV,resultsIV.(Yvars{3}).stage2,resultsIV.(Yvars{3}).stage1)

%%% !!! Think about tweaking the BaselIII variable: Denmark has allowed AT1
%%% capital to count towards core capital since beginning of 00's.
