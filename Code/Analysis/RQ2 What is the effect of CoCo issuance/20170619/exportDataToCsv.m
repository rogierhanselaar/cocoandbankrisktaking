% function exportDataToCsv(DataC,DataU)
function exportDataToCsv(DataC)

DataC = reorderVarsInData(DataC);
% DataU = reorderVarsInData(DataU);

todaysDate = year(datetime('today'))*10000+month(datetime('today'))*100+day(datetime('today'));
writetable(DataC,strcat('C:\Users\rogie\Dropbox\PhD\Data\ProjectsWithProfPurnanandam_RogierHanselaar\CoCosAndRiskTaking\Data\DataForCoAuthors\',num2str(todaysDate),'DataConsolidated_2000_2016.csv'))
% writetable(DataU,'C:\Users\rogie\Dropbox\PhD\Data\ProjectsWithProfPurnanandam_RogierHanselaar\CoCosAndRiskTaking\Data\DataForCoAuthors\20170728DataUnconsolidated_2000_2016.csv')


end

function DataForExport = reorderVarsInData(Data)

% Find the location of the variable types
IndexCategorical = table2array(varfun(@(x) iscategorical(x),Data));
IndexNumeric = table2array(varfun(@(x) isnumeric(x),Data));
IndexLogical = table2array(varfun(@(x) islogical(x),Data));
IndexCells = table2array(varfun(@(x) iscell(x),Data)) & ~IndexCategorical;

% Replace all comma's in the cells
variableNames = Data.Properties.VariableNames(table2array(varfun(@(x) iscell(x),Data)));
for varName = variableNames
    Data.(varName{1}) = strrep(Data.(varName{1}),',',' comma');
end

DataForExport = [Data(:,IndexCategorical), Data(:,IndexNumeric), Data(:,IndexLogical), Data(:,IndexCells)];



end
