function plotParalelTrends(MatchedSample)

MatchedSample = sortrows(MatchedSample,'FiscalYear','ascend');
MatchedSample = sortrows(MatchedSample,'BvDIDNumbers','ascend');

MatchedSample.YearsRelativeToFirstIssuance = NaN(size(MatchedSample,1),1);
for f = unique(MatchedSample.BvDIDNumbers)'
    IndexFirm = MatchedSample.BvDIDNumbers==f;
    loc_issuance = find(MatchedSample.DummyHasIssuedCoCo>0 & IndexFirm,1);
    MatchedSample.YearsRelativeToFirstIssuance(loc_issuance) = 0;
    for i = [-1,-2,1,2]
        if MatchedSample.BvDIDNumbers(loc_issuance + i) ==f
            MatchedSample.YearsRelativeToFirstIssuance(loc_issuance + i) = i;
        end
    end
end

IndexNotNaN = ~isnan(MatchedSample.YearsRelativeToFirstIssuance);
MeansPerYearRelativeToIssuance = ...
    varfun(@nanmean,...
    MatchedSample(IndexNotNaN, {'YearsRelativeToFirstIssuance',...
    'TreatmentFirms','ImpairedLoansSlashGrossLoans_wp1p99'}),...
    'GroupingVariables',{'YearsRelativeToFirstIssuance','TreatmentFirms'});

end