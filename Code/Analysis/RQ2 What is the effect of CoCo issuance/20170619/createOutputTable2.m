function [OutputTable] = createOutputTable2(results,Yvars,Xvars,FEs)

%% Some checks
if size(Xvars,1)~=1
    error('Please make sure that Xvars is a horizontal cell array.')
end
% if ~(strcmpi(Xvars{1},'DummyIsIssuer') || strcmpi(Xvars{1},'DummyHasIssuedCoCo'))
%     error('Please make sure that the first variable is a CoCo issuance variable. If you dont do that, the variable "N_obs==1" does not make sense.')
% end

%% Create table with a separate variable per iteration
% Create OutputTable
OutputTable = createOutputTableStructure(Yvars,Xvars,FEs);

% Fill OutputTable with results
OutputTable = fillOutputTable(OutputTable,results,Yvars,Xvars,FEs);


end

function OutputTable = createOutputTableStructure(Yvars,Xvars,FEs)

% Initialize OutputTable
N_Xvars = size(Xvars,2);
N_Yvars = size(Yvars,2);
OutputTable = cell((7+N_Xvars),(1+3*N_Yvars));

% Set up header
OutputTable(1,1) = {'Panel regression FEs:'};
if isempty(FEs)
    OutputTable(1,2) = {'-'};
else
    counter=0;
    for f = FEs
        counter=counter+1;
        OutputTable(1,1+counter) = FEs(1,counter);
    end
end
OutputTable(3,1) = {'Dependent Variable:'};
for i = 1:N_Yvars
    OutputTable(3,2+(i*3-3)) = Yvars(i);
    OutputTable(4,2+(i*3-3):3+(i*3-3)) = {'Coefficient','t-stat'}; 
end

% Set up the variable name column
if isempty(FEs)
    OutputTable(5,1) = {'Constant'};
    for i = 1:N_Xvars
        OutputTable(5+i,1) = Xvars(i);
    end
else
    for i = 1:N_Xvars
        OutputTable(4+i,1) = Xvars(i);
    end
end

% Set up the number of observations row
OutputTable(7+N_Xvars,1) = {'N'};
OutputTable(8+N_Xvars,1) = {'R2'};
OutputTable(9+N_Xvars,1) = {'#Firm FE'};

end

function OutputTable = fillOutputTable(OutputTable,results,Yvars,Xvars,FEs)

% Initialize variables
N_Xvars = size(Xvars,2);
N_Yvars = size(Yvars,2);

% Fill in table
for i = 1:N_Yvars
    
    % Determine first column number for the specification
    firstColNumber = 2+(i*3-3);
    
    % Fill in results for Xvars
    if isempty(FEs)
        N_iterations = N_Xvars+1;
    else
        N_iterations = N_Xvars;
    end
    for j = 1:N_iterations
        OutputTable(4+j,firstColNumber) = {num2str(results.(Yvars{i}).results.beta(j),'%.3f')};
        OutputTable(4+j,firstColNumber+1) = {num2str(results.(Yvars{i}).results.t_stat(j),'%.3f')};
    end
    
    % Fill in number of observations
    OutputTable(7+N_Xvars,firstColNumber) = {num2str(results.(Yvars{i}).N,'%.0f')};
    OutputTable(8+N_Xvars,firstColNumber) = {num2str(results.(Yvars{i}).R2,'%.3f')};
    if isfield(results.(Yvars{i}),'N_FEs')
        OutputTable(9+N_Xvars,firstColNumber) = {num2str(results.(Yvars{i}).N_FEs,'%.3f')};
    end
    
end




end
