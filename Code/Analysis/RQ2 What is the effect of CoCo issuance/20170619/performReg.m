function [OutputTable,OutputTable2] = performReg(Data,Yvars,Xvars,FE,clustering)

N_Yvars = size(Yvars,2);
for i = 1:N_Yvars
    [resultsTemp,R2Temp,FstatTemp,PvalTemp,NTemp,N_obsEqualTo1Temp] = runSimpleRegressions(Data,Yvars(i),Xvars,FE);
    results.(Yvars{i}).results = resultsTemp;
    results.(Yvars{i}).R2 = R2Temp;
    results.(Yvars{i}).Fstat = FstatTemp;
    results.(Yvars{i}).Pval = PvalTemp;
    results.(Yvars{i}).N = NTemp;
    results.(Yvars{i}).N_obsEqualTo1 = N_obsEqualTo1Temp;
    
    [resultsTemp2,R2Temp2,FstatTemp2,PvalTemp2,NTemp2,N_obsEqualTo1Temp2,N_FirmFEs2] = PanelWithClusteringAndFE_CoCo(Data,Yvars(i),Xvars,FE, clustering);
    results2.(Yvars{i}).results = resultsTemp2;
    results2.(Yvars{i}).R2 = R2Temp2;
    results2.(Yvars{i}).Fstat = FstatTemp2;
    results2.(Yvars{i}).Pval = PvalTemp2;
    results2.(Yvars{i}).N = NTemp2;
    results2.(Yvars{i}).N_obsEqualTo1 = N_obsEqualTo1Temp2;
    results2.(Yvars{i}).N_FEs = N_FirmFEs2;
end
[OutputTable] = createOutputTable2(results,Yvars,Xvars,FE);
[OutputTable2] = createOutputTable2(results2,Yvars,Xvars,FE);




end