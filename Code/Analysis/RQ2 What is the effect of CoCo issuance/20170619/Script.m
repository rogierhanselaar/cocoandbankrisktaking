clear
%% Load data
pcinfo=java.net.InetAddress.getLocalHost;
if ~strcmpi(pcinfo.getHostName,'LAPTOP-6P6G4FB7')
    error('Havent set the path yet for the dell laptop');
%     pathname = '';
else
    pathname1 = 'C:\Users\rogie\Git\cocoandbankrisktaking\Code\HelperFunctions\20170619\';
    pathname2 = 'C:\Users\rogie\Git\usefulmatlabfunctions\';
end

% Load merged data
addpath(pathname1);
addpath(pathname2);
[DataU,DataC,~] = loadData();

% Prep the data and export to csv
% exportDataToCsv(DataC)

%% Run simple panel regressions without matching
% Yvars = {'LoanLossResGrossLoans','LoanLossProvNetIntRev','ImpairedLoansGrossLoans','NonInterestExpenseAverageAssets','NonOpItemsNetIncome','NonOpItemsTaxesAvgAst','NetLoansTotDepBor'};
% Yvars = {'ImpairedLoansSlashGrossLoans','LoanLossResSlashGrossLoans'};
% Xvars = {'DummyHasIssuedCoCo'};
% Xvars = {'DummyOneOrMoreCoCosIssued','LogTotalAssets_EUR','Leverage_1minusEoverA_wp1p99','ReturnOnAvgAssetsROAA','CostToIncomeRatio','RecurringEarningPower'};
% Xvars = {'DummyHasIssuedCoCo','LogTotalAssets_EUR','Leverage_1minusEoverA_wp1p99'};
% Xvars = {'DummyHasIssuedCoCoAtOrBelowMedianTrigger','LogTotalAssets_EUR','Leverage_1minusEoverA_wp1p99'};
% performRegAndGenOutput(DataC(DataC.YearpartofCLOSDATE>=2009,:),Yvars,Xvars);

Yvars = {'ImpairedLoansSlashGrossLoans_wp1p99','LoanLossResSlashGrossLoans_wp1p99'};
% Xvars = {'DummyHasIssuedCoCo','LogTotalAssets_EUR_wp1p99','Leverage_1minusEoverA_wp1p99'};
% Xvars = {'DummyHasIssuedCoCo'};
% Yvars = {'AnnualVol_pct_wp1p99'};
% Yvars = {'ImpairedLoansSlashGrossLoans'};
% Yvars = {'CoCoIssued'};
% Yvars = {'DummyIsIssuer'};
% Xvars = {'DummyHasIssuedCoCo','LogTotalAssets_EUR_wp1p99_Lag1','Leverage_1minusEoverA_wp1p99','NetInterestMargin_wp1p99_Lag1'};
Xvars = {'DummyHasIssuedCoCo_equity_conversion_pure','DummyHasIssuedCoCo_write_down_pure','DummyHasIssuedCoCo_hybrid_pure','LogTotalAssets_EUR_wp1p99_Lag1','Leverage_1minusEoverA_wp1p99','NetInterestMargin_wp1p99_Lag1'};
% Xvars = {'CoCoRelativeSize','LogTotalAssets_EUR_wp1p99','Leverage_1minusEoverA_wp1p99','NetInterestMargin_wp1p99'};
FE = {};
% FE = {'BvDIDNumbers'};
% FE = {'BvDIDNumbers','FiscalYear'};
clustering = {'FiscalYear'};
% clustering = {'FiscalYear','BvDIDNumbers'};

[OutputTable,OutputTable2] = performReg(DataC,Yvars,Xvars,FE,clustering);

%% Run simple panel regressions with matching
load('Matching\20170923MatchedSample.mat')
load('Matching\20170923MatchedSample_dnstf.mat')

% plotParalelTrends(MatchedSample_dnstf)

% performRegAndGenOutput(DataMatched,Yvars,Xvars);

%% Run IV panel regression with matching
EndogeneousVariables = {'DummyHasIssuedCoCo'};
InstrumentalVariables = {'DummyFiscalTreatment','DummyInteractionBaselIIIFiscalTreatment'}; 
% InstrumentalVariables = {'DummyFiscalTreatment','DummyInteractionImplementationEUFiscalTreatment'}; 
% InstrumentalVariables = {'DummyFiscalTreatment'}; 
% InstrumentalVariables = {'InteractionFiscalTreatmentAndTax'}; 
% Xvars = {'LogTotalAssets_EUR','Leverage3'};
Xvars = {'LogTotalAssets_EUR_wp1p99_Lag1','Leverage_1minusEoverA_wp1p99','NetInterestMargin_wp1p99_Lag1'};

Yvars = [Yvars {'LoanLossProvSlashGrossLoans'}];
for y = Yvars
    [OutputTableS1,OutputTableS2] = createIVTableWithResults(MatchedSample_dnstf,y,EndogeneousVariables,Xvars,InstrumentalVariables);
%     [OutputTableS1,OutputTableS2] = createIVTableWithResults(Data,y,EndogeneousVariables,Xvars,InstrumentalVariables);
    resultsIV.(y{1}).stage1 = OutputTableS1;
    resultsIV.(y{1}).stage2 = OutputTableS2;
end
IV=1;
insertTablesIntoExcelSheet(DataU,IV,resultsIV.(Yvars{1}).stage2,resultsIV.(Yvars{1}).stage1)
% insertTablesIntoExcelSheet(Data,IV,resultsIV.(Yvars{2}).stage2,resultsIV.(Yvars{2}).stage1)
% insertTablesIntoExcelSheet(Data,IV,resultsIV.(Yvars{3}).stage2,resultsIV.(Yvars{3}).stage1)

%%% !!! Think about tweaking the BaselIII variable: Denmark has allowed AT1
%%% capital to count towards core capital since beginning of 00's.
