function Data = matchingByRandomlyAssigningAndDiscarding(Data,Yvar,Xvars,Filter,k,withReplacement)

% Impose filter and intialize variables
[DataTemp,N] = initializeVarsBeforeMatching(Data,Yvar,Xvars,Filter);
M_all = [];
M = [];
ControlGroupSpecs=[];

for i = 1:100000
    % Randomly assign control group
    DataTemp = randomlyAssignMatchedFirm(DataTemp,Yvar,k,withReplacement);
    
    % Calculate Mahalanobis distance
    TreatmentGroupIndicator = {'TreatmentFirms'};
    ControlGroupIndicator = {'ControlFirms'};
    Mtemp = calculateMahalanobisDistance(DataTemp,true(N,1),Xvars,TreatmentGroupIndicator,ControlGroupIndicator);
    
    % Store control group with Mtemp if Mtemp<min(M)
    if Mtemp<min(M_all)
        ControlGroupSpecs = [ControlGroupSpecs,DataTemp.ControlFirms];
        M = [M,Mtemp];
    end
    
    % Store all Mahalanobis distances
    M_all = [M_all;Mtemp];
end
% Find the control group for which M is minimized
DataTemp.ControlFirms = ControlGroupSpecs(:,M==min(M));

% Merge the TreatmentFirms and ControlFirms back into Data
Data = outerjoin(Data,DataTemp,...
    'Type','Left',...
    'LeftKeys','BvDIDNumbers',...
    'RightKeys','BvDIDNumbers',...
    'RightVariables',{'ControlFirms','TreatmentFirms'});

end


function DataTemp = randomlyAssignMatchedFirm(DataTemp,Yvar,k,withReplacement)

% Initialize variables
DataTemp = sortrows(DataTemp,Yvar{1},'descend');
N = size(DataTemp,1);
N_TFs = sum(DataTemp.(Yvar{1}));

% Construct a random set of control firms
if N_TFs*k < N-N_TFs % Check whether there are enough firms to draw from
    RandomNumbersDrawn = NaN;
    for i = 1:N_TFs*k
        % Draw a random number
        RandomNumber = randi([N_TFs+1 N]);
        if withReplacement==0
            % Redraw until the number has not been drawn before
            while sum(RandomNumber==RandomNumbersDrawn)>0
                RandomNumber = randi([N_TFs+1 N]);
            end
            RandomNumbersDrawn = [RandomNumbersDrawn; RandomNumber];
        end
        DataTemp.ControlFirms(RandomNumber) = DataTemp.ControlFirms(RandomNumber)+1;
    end
else
    error('There are not enough potential control firms to draw from')
end


end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% OLD STUFF %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Initialize array for matched firms
% Temp = categorical({''});
% Temp(size(Data,1),k) = '<undefined>';
% Data.ControlFirms = Temp;
% 
% % Randomly match non treatment firms to treatment firms
% DataTemp = Data(filter,:);
% DataTemp = sortrows(DataTemp,Yvar{1},'descend');
% N = size(DataTemp,1);
% N_TFs = sum(DataTemp.(Yvar{1}));
% RandomNumbersDrawn = NaN;
% for i = 1:N_TFs
%     for j = 1:k
%         % Draw a random number 
%         RandomNumber = randi([N_TFs+1 N]);
%         if withReplacement==0
%             % Check whether there are enough firms to draw from
%             if N_TFs*k < N-N_TFs
%                 % Redraw until the number has not been drawn before
%                 while sum(RandomNumber==RandomNumbersDrawn)>0
%                     RandomNumber = randi([N_TFs+1 N]);
%                 end
%                 RandomNumbersDrawn = [RandomNumbersDrawn; RandomNumber];
%             else
%                 error('There are not enough potential control firms to draw from')
%             end
%         end
%         % Assign a BvDIDNumber drawn using the random number to be a
%         % control firm
%         DataTemp.ControlFirms(i,j) = DataTemp.BvDIDNumbers(RandomNumber);
%     end
% end
% 
% % Merge the ControlFirms back into Data
% Data = outerjoin(Data,DataTemp,...
%     'Type','Left',...
%     'LeftKeys','BvDIDNumbers',...
%     'RightKeys','BvDIDNumbers',...
%     'RightVariables','ControlFirms');