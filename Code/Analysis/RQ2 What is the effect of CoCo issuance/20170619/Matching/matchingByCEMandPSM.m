function [Data,MatchedSample,MatchedSample_dnstf] = matchingByCEMandPSM(Data,Yvar,Xvars,k,withReplacement)

%%%%%%%%%%% This function still needs to be tested extensively %%%%%%%%%%%%

% Impose filter and intialize variables
[DataTemp,N] = initializeVarsBeforeMatching(Data,Yvar,Xvars,Data.Filter);

% Merge treatmentfirms back into data (these are filtered on
% data availablity in initializeVarsBeforeMatching(...))
Data = outerjoin(Data,DataTemp,...
    'Type','Left',...
    'LeftKeys','BvDIDNumbers',...
    'RightKeys','BvDIDNumbers',...
    'RightVariables','TreatmentFirms');
Data = sortrows(Data,'FiscalYear','ascend');
Data = sortrows(Data,'BvDIDNumbers','ascend');

% Run probit to calculate propensity scores
[b,~,~,~] = runBinaryResponseRegression(DataTemp,Yvar,Xvars,'probit');
X = [ones(N,1) table2array(DataTemp(:,Xvars))];
FittedPS = strcat(Yvar{1},'_PS');
DataTemp.(FittedPS) = NaN(N,1);
DataTemp.(FittedPS) = normcdf(X*b);

% Create propensity score bins based on histogram treatment firms
BinningNumber = [3];
for N_b = BinningNumber
    % Create bins
    quantileBins.(strcat('N_b_',num2str(N_b))) = array2table([1/N_b:1/N_b:1]','VariableNames',{'CumulativeProbability'});
    quantileBins.(strcat('N_b_',num2str(N_b))).LowerCutOff = NaN(N_b,1);
    quantileBins.(strcat('N_b_',num2str(N_b))).UpperCutOff = NaN(N_b,1);
    for b = 1:N_b
        quantileBins.(strcat('N_b_',num2str(N_b))).LowerCutOff(b) = quantile(DataTemp.(FittedPS)(DataTemp.TreatmentFirms==1),(b-1)*1/N_b);
        quantileBins.(strcat('N_b_',num2str(N_b))).UpperCutOff(b) = quantile(DataTemp.(FittedPS)(DataTemp.TreatmentFirms==1),b*1/N_b);
    end
    
    % Initialize vars
    ControlGroupSpecs = [];
    M=[];
    M_dnstf = [];
    M_all=[];
    M_all_dnstf = [];
    BvDIDNumbers_control_all = categorical();
    BvDIDNumbers_control_all_dnstf = categorical();
    BvDIDNumbers_control_best = categorical();
    BvDIDNumbers_control_best_dnstf = categorical(); 
    BvDIDNumbers_treatment_firms_best_dnstf = categorical();
    BvDIDNumbers_treatment_firms_all_dnstf = categorical();
    
    for i = 1:10
        % For each bin, draw k times the number of treatment firms in that bin
        % from non-treatment firms that also fall in that bin
        BvDIDNumbers_control = categorical();
        BvDIDNumbers_treatment_dnstf = categorical();
        for b = 1:N_b
            
            % Select eligble control firms
            if b==1
                IndexElegibleFirms = ...
                    DataTemp.(FittedPS) > 0 & ...
                    DataTemp.(FittedPS) <= quantileBins.(strcat('N_b_',num2str(N_b))).UpperCutOff(b);
            else
                IndexElegibleFirms = ...
                    DataTemp.(FittedPS) > quantileBins.(strcat('N_b_',num2str(N_b))).LowerCutOff(b) & ...
                    DataTemp.(FittedPS) <= quantileBins.(strcat('N_b_',num2str(N_b))).UpperCutOff(b);
            end
            IndexElegibleControlFirms = IndexElegibleFirms & DataTemp.TreatmentFirms~=1;
            DataTempEligbleControls = DataTemp(IndexElegibleControlFirms,:);
            IndexElegibleTreatmentFirms = IndexElegibleFirms & DataTemp.TreatmentFirms==1;
            DataTempEligbleTreatmenFirms = DataTemp(IndexElegibleTreatmentFirms,:);
            
            % Initialize number of treatment firms in bin and number of
            % elegible control firms in bin
            N_TreatmentFirms = sum(DataTemp.TreatmentFirms==1 & IndexElegibleFirms);
            N_elegibleControlFirms = sum(IndexElegibleControlFirms);
            
            % Select control firms
            if withReplacement==1
                if size(DataTempEligbleControls,1)>N_TreatmentFirms*k
                    [BvDIDNumbers,IndexDrawn] = datasample(DataTempEligbleControls.BvDIDNumbers,N_TreatmentFirms*k,'Replace',false);
                else
                    [BvDIDNumbers,IndexDrawn] = datasample(DataTempEligbleControls.BvDIDNumbers,N_TreatmentFirms*k,'Replace',true);
                end
            elseif withReplacement==0
                [BvDIDNumbers,IndexDrawn] = datasample(DataTempEligbleControls.BvDIDNumbers,N_TreatmentFirms*k,'Replace',false);
            else
                error('Choose either withReplacement==0 or withReplacement==1')
            end
            DataTempEligbleControls.(strcat('ControlFirms_N_b',num2str(N_b),'_bin',num2str(b))) = NaN(N_elegibleControlFirms,1);
            DataTempEligbleControls.(strcat('ControlFirms_N_b',num2str(N_b),'_bin',num2str(b)))(IndexDrawn)=1;
            
            % Merge back into data
            Data = outerjoin(Data,DataTempEligbleControls,...
                'Type','Left',...
                'LeftKeys','BvDIDNumbers',...
                'RightKeys','BvDIDNumbers',...
                'RightVariables',strcat('ControlFirms_N_b',num2str(N_b),'_bin',num2str(b)));
            
            % Combine BvDIDNumbers control group
            BvDIDNumbers_control = [BvDIDNumbers_control;BvDIDNumbers];
            N_control = size(unique(BvDIDNumbers),1);
            
            % For Matched_sample_dnstf (discard non-supported treatment
            % firms)
            [BvDIDNumbers,IndexDrawn] = datasample(DataTempEligbleTreatmenFirms.BvDIDNumbers,N_control/k,'Replace',false);
            BvDIDNumbers_treatment_dnstf = [BvDIDNumbers_treatment_dnstf;BvDIDNumbers]; 
        end
        
        % Create one index per N_b
        IndexTemp=zeros(size(Data,1),1);
        for b = 1:N_b
            IndexTemp = IndexTemp | Data.(strcat('ControlFirms_N_b',num2str(N_b),'_bin',num2str(b)))==1;
            Data.(strcat('ControlFirms_N_b',num2str(N_b),'_bin',num2str(b)))=[];
        end
        Data.(strcat('ControlFirms_N_b',num2str(N_b)))=zeros(size(Data,1),1);
        Data.(strcat('ControlFirms_N_b',num2str(N_b)))=IndexTemp;
        
        % Calculate Mahalanobis distance
        TreatmentGroupIndicator = {'TreatmentFirms'};
        ControlGroupIndicator = {strcat('ControlFirms_N_b',num2str(N_b))};
        [MatchedSample,MatchedSample_dnstf] = createMatchedSample(Data,BvDIDNumbers_control,BvDIDNumbers_treatment_dnstf);
        Mtemp = calculateMahalanobisDistance(MatchedSample,Xvars,TreatmentGroupIndicator,ControlGroupIndicator);
        Mtemp_dnstf = calculateMahalanobisDistance(MatchedSample_dnstf,Xvars,TreatmentGroupIndicator,ControlGroupIndicator);
        
        % Store control group with Mtemp if Mtemp<min(M)
        if Mtemp<min(M_all)
            ControlGroupSpecs = [ControlGroupSpecs,Data.(strcat('ControlFirms_N_b',num2str(N_b)))];
            M = [M,Mtemp];
            BvDIDNumbers_control_best = [BvDIDNumbers_control_best BvDIDNumbers_control];
        end
        if Mtemp_dnstf<min(M_all_dnstf)
            M_dnstf = [M_dnstf,Mtemp_dnstf];
            BvDIDNumbers_control_best_dnstf = [BvDIDNumbers_control_best_dnstf unique(BvDIDNumbers_control)];
            BvDIDNumbers_treatment_firms_best_dnstf = [BvDIDNumbers_treatment_firms_best_dnstf BvDIDNumbers_treatment_dnstf];
        end
        
        % Store all Mahalanobis distances
        M_all = [M_all;Mtemp];
        BvDIDNumbers_control_all = [BvDIDNumbers_control_all BvDIDNumbers_control];
        M_all_dnstf = [M_all_dnstf;Mtemp_dnstf];
        BvDIDNumbers_control_all_dnstf = [BvDIDNumbers_control_all_dnstf unique(BvDIDNumbers_control)];
        BvDIDNumbers_treatment_firms_all_dnstf = [BvDIDNumbers_treatment_firms_all_dnstf BvDIDNumbers_treatment_dnstf];

    end
   % Pick the control group spec with the smallest M
   Data.(strcat('ControlFirms_N_b',num2str(N_b))) = ControlGroupSpecs(:,end);
   EmptyCat = categorical();
   [MatchedSample, ~] = createMatchedSample(Data,BvDIDNumbers_control_best(:,end),EmptyCat);
   [ ~,MatchedSample_dnstf] = createMatchedSample(Data,BvDIDNumbers_control_best_dnstf(:,end),BvDIDNumbers_treatment_firms_best_dnstf(:,end));
end




end


function [MatchedSample,MatchedSample_dnstf] = createMatchedSample(Data,BvDIDNumbers_control,BvDIDNumbers_treatment)

BvDIDNumbers_in_matched_sample = [unique(Data.BvDIDNumbers(Data.TreatmentFirms>0));BvDIDNumbers_control];
MatchedSample = Data(Data.BvDIDNumbers==BvDIDNumbers_in_matched_sample(1),:);
for firm = BvDIDNumbers_in_matched_sample(2:end)'
    MatchedSample = [MatchedSample;Data(Data.BvDIDNumbers==firm,:)];
end
if ~isempty(BvDIDNumbers_treatment)
    BvDIDNumbers_in_matched_sample_dnstf = [BvDIDNumbers_treatment;unique(BvDIDNumbers_control)];
    MatchedSample_dnstf = Data(Data.BvDIDNumbers==BvDIDNumbers_in_matched_sample_dnstf(1),:);
    for firm = BvDIDNumbers_in_matched_sample_dnstf(2:end)'
        MatchedSample_dnstf = [MatchedSample_dnstf;Data(Data.BvDIDNumbers==firm,:)];
    end
else
    MatchedSample_dnstf = table();
end
end