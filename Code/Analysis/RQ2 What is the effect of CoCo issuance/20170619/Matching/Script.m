clear
%% Load data
pcinfo=java.net.InetAddress.getLocalHost;
if ~strcmpi(pcinfo.getHostName,'LAPTOP-6P6G4FB7')
    error('Havent set the path yet for the dell laptop');
%     pathname = '';
else
    pathname1 = 'C:\Users\rogie\Git\cocoandbankrisktaking\Code\';
    pathname2 = 'C:\Users\rogie\Git\cocoandbankrisktaking\Code\HelperFunctions\20170619\';
end

% Load merged data
addpath(pathname1);
addpath(pathname2);
[~,DataC,~] = loadData();
DataC.CountryISOcode = categorical(DataC.CountryISOcode);
Data = DataC;
BvDIDNumbers_issuers = table(unique(DataC.BvDIDNumbers(DataC.CoCoIssued>0)),'VariableNames',{'BvDIDNumbers_issuers'});
DataC = outerjoin(DataC,BvDIDNumbers_issuers,...
    'Type','Left',...
    'LeftKeys','BvDIDNumbers',...
    'RightKeys','BvDIDNumbers_issuers',...
    'RightVariables','BvDIDNumbers_issuers');
DataC.DummyIsIssuer = DataC.BvDIDNumbers==DataC.BvDIDNumbers_issuers;

%% Match issuers with non-issuers
Yvar = {'DummyIsIssuer'};
Xvars = {'LogTotalAssets_EUR_wp1p99','Leverage_1minusEoverA_wp1p99','NetInterestMargin_wp1p99_Lag1'};
Data = sortrows(Data,'FiscalYear','ascend');
Data = sortrows(Data,'BvDIDNumbers','ascend');
Data.Filter = Data.FiscalYear==2009;
delete(findall(0,'Type','figure'))

% Assess balance prior to matching
Data.DummyIsNotIssuer = Data.DummyIsIssuer==0;
ControlGroupIndicator = {'DummyIsNotIssuer'};
TreatmentGroupIndicator = {'DummyIsIssuer'};
BalanceResultsPrior = assessSampleBalance(Data,Xvars,TreatmentGroupIndicator,ControlGroupIndicator);
k=1;
% withReplacement=0;
withReplacement=1;

% Randomly assign k non-issuer(s) to each issuer and discard until
% Mahalanobis distance is small --> M does not get smaller than 4, while it
% would ideally be smaller than 1.5ish. A potential explanation might be
% the lack of common support.
% Data = matchingByRandomlyAssigningAndDiscarding(Data,Yvar,Xvars,Index2009,k,withReplacement);

% Use CEM (Coarsened Exact Matching) based on propensity scores
[Data,MatchedSample,MatchedSample_dnstf] = matchingByCEMandPSM(Data,Yvar,Xvars,k,withReplacement);

% Assess balance post matching
TreatmentGroupIndicator = {'TreatmentFirms'};
ControlGroupIndicator = {'ControlFirms_N_b3'};
BalanceResultsPostNb3_MS = assessSampleBalance(MatchedSample,Xvars,TreatmentGroupIndicator,ControlGroupIndicator);
MatchedSample_dnstf.(ControlGroupIndicator{1}) = ~MatchedSample_dnstf.(TreatmentGroupIndicator{1});
BalanceResultsPostNb3_MS_dnstf = assessSampleBalance(MatchedSample_dnstf,Xvars,TreatmentGroupIndicator,ControlGroupIndicator);

% Export contol group
DateTemp = year(today())*10000 + month(today())*100 + day(today());
save(strcat(num2str(DateTemp),'MatchedSample.mat'),'MatchedSample')
save(strcat(num2str(DateTemp),'MatchedSample_dnstf.mat'),'MatchedSample_dnstf')







