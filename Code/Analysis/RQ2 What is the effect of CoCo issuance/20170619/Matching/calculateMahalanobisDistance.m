function M = calculateMahalanobisDistance(Data,Xvars,TreatmentGroupIndicator,ControlGroupIndicator)

DataTemp = Data(Data.Filter,:);

X = table2array(DataTemp(DataTemp.(TreatmentGroupIndicator{1})==1 | DataTemp.(ControlGroupIndicator{1})==1,Xvars));
X_T = table2array(DataTemp(DataTemp.(TreatmentGroupIndicator{1})==1,Xvars));
X_T_mean = nanmean(X_T);
N_xt = size(X_T,1);
X_C = table2array(DataTemp(DataTemp.(ControlGroupIndicator{1})==1,Xvars));
X_C_mean = nanmean(X_C);
N_xc = size(X_C,1);
if sum(sum(isnan(X)))>0
    X;
end
M = (1/N_xt + 1/N_xc)^(-1)*(X_T_mean - X_C_mean)*inv(cov(X))*(X_T_mean - X_C_mean)'; % From slide 6 of presentation Rubin 2015_Rubin_2_Re_Randomization

end