function insertTablesIntoExcelSheet(Data,IV,varargin)

% Concatenate cells into one big cell array
N_tables = size(varargin,2);
outputForExcel = {};
for i = 1:N_tables
    outputForExcel = [outputForExcel;varargin{i}];
    N_cols = size(outputForExcel,2);
    outputForExcel = [outputForExcel;cell(3,N_cols)];
end

% Write to excel; if there is a TreatmentFirms variable (and thus there has
% been matching) save the sheet under a name that reflects that.
if IV==0
    try
        Data.TreatmentFirms;
        xlswrite('regressionResultsMatching.xlsx',outputForExcel)
    catch
        xlswrite('regressionResults.xlsx',outputForExcel)
    end
else
    xlswrite('regressionResultsMatchingIV.xlsx',outputForExcel)
end


end