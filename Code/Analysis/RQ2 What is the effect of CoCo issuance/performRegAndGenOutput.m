function performRegAndGenOutput(Data,Yvars,Xvars)

N_Yvars = size(Yvars,2);
% Without FEs
FE = {};
for i = 1:N_Yvars
    [resultsTemp,R2Temp,FstatTemp,PvalTemp,NTemp,N_obsEqualTo1Temp] = runSimpleRegressions(Data,Yvars(i),Xvars,FE);
    resultsWithoutFEs.(Yvars{i}).results = resultsTemp;
    resultsWithoutFEs.(Yvars{i}).R2 = R2Temp;
    resultsWithoutFEs.(Yvars{i}).Fstat = FstatTemp;
    resultsWithoutFEs.(Yvars{i}).Pval = PvalTemp;
    resultsWithoutFEs.(Yvars{i}).N = NTemp;
    resultsWithoutFEs.(Yvars{i}).N_obsEqualTo1 = N_obsEqualTo1Temp;
end
[OutputTable] = createOutputTable(resultsWithoutFEs,Yvars,Xvars,FE);

% With FiscalYear FEs
FE = {'FiscalYear'};
for i = 1:N_Yvars
    [resultsTemp,R2Temp,FstatTemp,PvalTemp,NTemp,N_obsEqualTo1Temp] = runSimpleRegressions(Data,Yvars(i),Xvars,FE);
    resultsWithFiscalYearFEs.(Yvars{i}).results = resultsTemp;
    resultsWithFiscalYearFEs.(Yvars{i}).R2 = R2Temp;
    resultsWithFiscalYearFEs.(Yvars{i}).Fstat = FstatTemp;
    resultsWithFiscalYearFEs.(Yvars{i}).Pval = PvalTemp;
    resultsWithFiscalYearFEs.(Yvars{i}).N = NTemp;
    resultsWithFiscalYearFEs.(Yvars{i}).N_obsEqualTo1 = N_obsEqualTo1Temp;
end
[OutputTableWithFiscalYearFEs] = createOutputTable(resultsWithFiscalYearFEs,Yvars,Xvars,FE);

% With BvDIDNumbers FEs
FE = {'BvDIDNumbers'};
for i = 1:N_Yvars
    [resultsTemp,R2Temp,FstatTemp,PvalTemp,NTemp,N_obsEqualTo1Temp] = runSimpleRegressions(Data,Yvars(i),Xvars,FE);
    resultsWithBvDIDNumbersFEs.(Yvars{i}).results = resultsTemp;
    resultsWithBvDIDNumbersFEs.(Yvars{i}).R2 = R2Temp;
    resultsWithBvDIDNumbersFEs.(Yvars{i}).Fstat = FstatTemp;
    resultsWithBvDIDNumbersFEs.(Yvars{i}).Pval = PvalTemp;
    resultsWithBvDIDNumbersFEs.(Yvars{i}).N = NTemp;
    resultsWithBvDIDNumbersFEs.(Yvars{i}).N_obsEqualTo1 = N_obsEqualTo1Temp;
end
[OutputTableWithBvDIDNumbersFEs] = createOutputTable(resultsWithBvDIDNumbersFEs,Yvars,Xvars,FE);

% With both FiscalYear and BvDIDNumbers FEs
FE = {'BvDIDNumbers','FiscalYear'};
for i = 1:N_Yvars
    [resultsTemp,R2Temp,FstatTemp,PvalTemp,NTemp,N_obsEqualTo1Temp] = runSimpleRegressions(Data,Yvars(i),Xvars,FE);
    resultsWithFiscalYearAndBvDIDNumbersFEs.(Yvars{i}).results = resultsTemp;
    resultsWithFiscalYearAndBvDIDNumbersFEs.(Yvars{i}).R2 = R2Temp;
    resultsWithFiscalYearAndBvDIDNumbersFEs.(Yvars{i}).Fstat = FstatTemp;
    resultsWithFiscalYearAndBvDIDNumbersFEs.(Yvars{i}).Pval = PvalTemp;
    resultsWithFiscalYearAndBvDIDNumbersFEs.(Yvars{i}).N = NTemp;
    resultsWithFiscalYearAndBvDIDNumbersFEs.(Yvars{i}).N_obsEqualTo1 = N_obsEqualTo1Temp;
end
[OutputTableWithFiscalYearAndBvDIDNumbersFEs] = createOutputTable(resultsWithFiscalYearAndBvDIDNumbersFEs,Yvars,Xvars,FE);

% Output each set of OutputTables into one excel sheet
IV=0;
insertTablesIntoExcelSheet(Data,IV,OutputTable,OutputTableWithFiscalYearFEs,OutputTableWithBvDIDNumbersFEs,OutputTableWithFiscalYearAndBvDIDNumbersFEs);


end