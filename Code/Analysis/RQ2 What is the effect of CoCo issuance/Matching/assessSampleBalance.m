function BalanceResults = assessSampleBalance(Data,Xvars,Filter,TreatmentGroupIndicator,ControlGroupIndicator)

DataTemp = Data(Filter,:);
N_Xvars = size(Xvars,2);
BalanceResults = array2table(Xvars','VariableNames',{'Xvars'});
BalanceResults.DifferenceInMeans = NaN(N_Xvars,1);
BalanceResults.EqualMeansTstat = NaN(N_Xvars,1);
BalanceResults.EqualMeansPval = NaN(N_Xvars,1);
BalanceResults.KStestStat = NaN(N_Xvars,1);
BalanceResults.KStestPval = NaN(N_Xvars,1);

for v = Xvars
    IndexXvar = strcmpi(BalanceResults.Xvars,v{1});
    
    % Difference in means
    BalanceResults.DifferenceInMeans(IndexXvar) = nanmean(DataTemp.(v{1})(DataTemp.(TreatmentGroupIndicator{1})==1)) - nanmean(DataTemp.(v{1})(DataTemp.(ControlGroupIndicator{1})==1));
    
    % T-tests of equal means, without assumptions on equality of variance
    % or sample size (Welch's T-test)
    [~,p,~,stats] = ttest2(DataTemp.(v{1})(DataTemp.(TreatmentGroupIndicator{1})==1),DataTemp.(v{1})(DataTemp.(ControlGroupIndicator{1})==1),'Vartype','unequal');
    
    BalanceResults.EqualMeansTstat(IndexXvar) = stats.tstat;
    BalanceResults.EqualMeansPval(IndexXvar) = p;
    
    % KS-tests of equal distribution
    [~,p,ksstat] = kstest2(DataTemp.(v{1})(DataTemp.(TreatmentGroupIndicator{1})==1),DataTemp.(v{1})(DataTemp.(ControlGroupIndicator{1})==1));
    BalanceResults.KStestStat(IndexXvar) = ksstat;
    BalanceResults.KStestPval(IndexXvar) = p;
end

% Create histograms
createHistograms(DataTemp,Xvars,TreatmentGroupIndicator,ControlGroupIndicator)

end

function createHistograms(Data,Xvars,TreatmentGroupIndicator,ControlGroupIndicator)

for x = Xvars
    figure()
    histogram(Data.(x{1})(Data.(ControlGroupIndicator{1})==1),5)
    hold on
    histogram(Data.(x{1})(Data.(TreatmentGroupIndicator{1})==1),5)
    legend(ControlGroupIndicator{1},TreatmentGroupIndicator{1})
    title(x{1})
    hold off
end



end