function Data = matchingByCEMandPSM(Data,Yvar,Xvars,Filter,k,withReplacement)

%%%%%%%%%%% This function still needs to be tested extensively %%%%%%%%%%%%

% Impose filter and intialize variables
[DataTemp,N] = initializeVarsBeforeMatching(Data,Yvar,Xvars,Filter);

% Merge treatmentfirms back into data (these are filtered on
% data availablity in initializeVarsBeforeMatching(...))
Data = outerjoin(Data,DataTemp,...
    'Type','Left',...
    'LeftKeys','BvDIDNumbers',...
    'RightKeys','BvDIDNumbers',...
    'RightVariables','TreatmentFirms');
Data = sortrows(Data,'FiscalYear','ascend');
Data = sortrows(Data,'BvDIDNumbers','ascend');

% Run probit to calculate propensity scores
[b,~,~,~] = runBinaryResponseRegression(DataTemp,Yvar,Xvars,'probit');
X = [ones(N,1) table2array(DataTemp(:,Xvars))];
FittedPS = strcat(Yvar{1},'_PS');
DataTemp.(FittedPS) = NaN(N,1);
DataTemp.(FittedPS) = normcdf(X*b);

% Create propensity score bins based on histogram treatment firms
BinningNumber = [3];
for N_b = BinningNumber
    % Create bins
    quantileBins.(strcat('N_b_',num2str(N_b))) = array2table([1/N_b:1/N_b:1]','VariableNames',{'CumulativeProbability'});
    quantileBins.(strcat('N_b_',num2str(N_b))).LowerCutOff = NaN(N_b,1);
    quantileBins.(strcat('N_b_',num2str(N_b))).UpperCutOff = NaN(N_b,1);
    for b = 1:N_b
        quantileBins.(strcat('N_b_',num2str(N_b))).LowerCutOff(b) = quantile(DataTemp.(FittedPS)(DataTemp.TreatmentFirms==1),(b-1)*1/N_b);
        quantileBins.(strcat('N_b_',num2str(N_b))).UpperCutOff(b) = quantile(DataTemp.(FittedPS)(DataTemp.TreatmentFirms==1),b*1/N_b);
    end
    
    % Initialize vars
    ControlGroupSpecs = [];
    M=[];
    M_all=[];
    
    for i = 1:1000
        % For each bin, draw k times the number of treatment firms in that bin
        % from non-treatment firms that also fall in that bin
        for b = 1:N_b
            
            % Select eligble control firms
            if b==1
                IndexElegibleFirms = ...
                    DataTemp.(FittedPS) > 0 & ...
                    DataTemp.(FittedPS) <= quantileBins.(strcat('N_b_',num2str(N_b))).UpperCutOff(b);
            else
                IndexElegibleFirms = ...
                    DataTemp.(FittedPS) > quantileBins.(strcat('N_b_',num2str(N_b))).LowerCutOff(b) & ...
                    DataTemp.(FittedPS) <= quantileBins.(strcat('N_b_',num2str(N_b))).UpperCutOff(b);
            end
            IndexElegibleControlFirms = IndexElegibleFirms & DataTemp.TreatmentFirms~=1;
            DataTempEligbleControls = DataTemp(IndexElegibleControlFirms,:);
            
%             if sum(strcmpi(DataTempEligbleControls.NAME,'ERB Hellas Plc'))>0
%                 b;
%             end
            
            % Initialize number of treatment firms in bin and number of
            % elegible control firms in bin
            N_TreatmentFirms = sum(DataTemp.TreatmentFirms==1 & IndexElegibleFirms);
            N_elegibleControlFirms = sum(IndexElegibleControlFirms);
            
            % Select control firms
            if withReplacement==1
                [BvDIDNumbers,IndexDrawn] = datasample(DataTempEligbleControls.BvDIDNumbers,N_TreatmentFirms*k,'Replace',true);
            elseif withReplacement==0
                [BvDIDNumbers,IndexDrawn] = datasample(DataTempEligbleControls.BvDIDNumbers,N_TreatmentFirms*k,'Replace',false);
            else
                error('Choose either withReplacement==0 or withReplacement==1')
            end
            DataTempEligbleControls.(strcat('ControlFirms_N_b',num2str(N_b),'_bin',num2str(b))) = NaN(N_elegibleControlFirms,1);
            DataTempEligbleControls.(strcat('ControlFirms_N_b',num2str(N_b),'_bin',num2str(b)))(IndexDrawn)=1;
            
            % Merge back into data
            Data = outerjoin(Data,DataTempEligbleControls,...
                'Type','Left',...
                'LeftKeys','BvDIDNumbers',...
                'RightKeys','BvDIDNumbers',...
                'RightVariables',strcat('ControlFirms_N_b',num2str(N_b),'_bin',num2str(b)));
        end
        
        % Create one index per N_b
        IndexTemp=zeros(size(Data,1),1);
        for b = 1:N_b
            IndexTemp = IndexTemp | Data.(strcat('ControlFirms_N_b',num2str(N_b),'_bin',num2str(b)))==1;
            Data.(strcat('ControlFirms_N_b',num2str(N_b),'_bin',num2str(b)))=[];
        end
        Data.(strcat('ControlFirms_N_b',num2str(N_b)))=zeros(size(Data,1),1);
        Data.(strcat('ControlFirms_N_b',num2str(N_b)))=IndexTemp;
        
        % Calculate Mahalanobis distance
        TreatmentGroupIndicator = {'TreatmentFirms'};
        ControlGroupIndicator = {strcat('ControlFirms_N_b',num2str(N_b))};
        Mtemp = calculateMahalanobisDistance(Data,Filter,Xvars,TreatmentGroupIndicator,ControlGroupIndicator);
        
        % Store control group with Mtemp if Mtemp<min(M)
        if Mtemp<min(M_all)
            ControlGroupSpecs = [ControlGroupSpecs,Data.(strcat('ControlFirms_N_b',num2str(N_b)))];
            M = [M,Mtemp];
        end
        
        % Store all Mahalanobis distances
        M_all = [M_all;Mtemp];
    end
   % Pick the control group spec with the smallest M
   Data.(strcat('ControlFirms_N_b',num2str(N_b))) = ControlGroupSpecs(:,end);
end



end