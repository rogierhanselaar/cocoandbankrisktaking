clear
%% Load data
pcinfo=java.net.InetAddress.getLocalHost;
if ~strcmpi(pcinfo.getHostName,'LAPTOP-6P6G4FB7')
    error('Havent set the path yet for the dell laptop');
%     pathname = '';
else
    pathname1 = 'C:\Users\rogie\Git\cocoandbankrisktaking\Code\';
end

% Load merged data
addpath(pathname1);
[DataIssuers,DataIssuersWithoutLloyds,DataNonIssuers] = loadData();
DataIssuers.CountryISOcode = categorical(DataIssuers.CountryISOcode);
Data = [DataIssuersWithoutLloyds;DataNonIssuers];

% Create some additional variables
Data.LogTotalAssets_EUR = log(Data.TotalAssets_EUR);
% Data.Leverage = (Data.TotalAssets_EUR-Data.TotalLiabilities_EUR)./Data.TotalAssets_EUR;
Data.Leverage = Data.TotalAssets_EUR./(Data.TotalAssets_EUR-Data.TotalLiabilities_EUR); 
Data.Leverage(isinf(Data.Leverage))=NaN;
Data.Leverage(Data.Leverage<=0)=NaN;
Data.CashAndDueFromBanksOverTotalAssets = Data.CashAndDueFromBanks_EUR./Data.TotalAssets_EUR;
Data.NetInterestIncome_EUR(Data.NetInterestIncome_EUR<=0)=NaN;
Data.LogNetInterestIncome_EUR = log(Data.NetInterestIncome_EUR);
Data.LogCashAndDueFromBanks_EUR = log(Data.CashAndDueFromBanks_EUR);
Data.LogCashAndDueFromBanks_EUR(isinf(Data.LogCashAndDueFromBanks_EUR))=NaN;


%% Match issuers with non-issuers
Yvar = {'DummyIsIssuer'};
Xvars = {'LogTotalAssets_EUR','Leverage'};
Data = sortrows(Data,'FiscalYear','ascend');
Data = sortrows(Data,'BvDIDNumbers','ascend');
Index2009 = Data.FiscalYear==2009;
delete(findall(0,'Type','figure'))

% Assess balance prior to matching
Data.DummyIsNotIssuer = Data.DummyIsIssuer==0;
ControlGroupIndicator = {'DummyIsNotIssuer'};
TreatmentGroupIndicator = {'DummyIsIssuer'};
BalanceResultsPrior = assessSampleBalance(Data,Xvars,Index2009,TreatmentGroupIndicator,ControlGroupIndicator);
k=1;
withReplacement=0;

% Randomly assign k non-issuer(s) to each issuer and discard until
% Mahalanobis distance is small --> M does not get smaller than 4, while it
% would ideally be smaller than 1.5ish. A potential explanation might be
% the lack of common support.
% Data = matchingByRandomlyAssigningAndDiscarding(Data,Yvar,Xvars,Index2009,k,withReplacement);

% Use CEM (Coarsened Exact Matching) based on propensity scores
Data = matchingByCEMandPSM(Data,Yvar,Xvars,Index2009,k,withReplacement);

% Assess balance post matching
TreatmentGroupIndicator = {'TreatmentFirms'};
ControlGroupIndicator = {'ControlFirms_N_b3'};
BalanceResultsPostNb3 = assessSampleBalance(Data,Xvars,Index2009,TreatmentGroupIndicator,ControlGroupIndicator);

% Export contol group
TreatmentAndControlFirms = unique(Data(Data.ControlFirms_N_b3==1 | Data.TreatmentFirms==1,{'BvDIDNumbers','ControlFirms_N_b3','TreatmentFirms'}));
save('TreatmentAndControlFirms.mat','TreatmentAndControlFirms')







