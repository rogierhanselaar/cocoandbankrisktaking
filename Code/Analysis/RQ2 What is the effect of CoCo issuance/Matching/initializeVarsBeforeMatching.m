function [DataTemp,N] = initializeVarsBeforeMatching(Data,Yvar,Xvars,filter)

% Impose filter and intialize variables
DataTemp = Data(filter,:);
N = size(DataTemp,1);
DataTemp.TreatmentFirms = DataTemp.(Yvar{1});
DataTemp.ControlFirms = zeros(N,1);

% Remove firms that do not have data on Xvars
for v = Xvars
    DataTemp(isnan(DataTemp.(v{1})),:)=[];
end

% Recalculate N
N = size(DataTemp,1);

% Set random number generator to shuffle
% rng('shuffle')


end