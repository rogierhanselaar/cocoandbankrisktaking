function [OutputTableSeparateVars,OutputTableAdditionalVars] = createTableWithBinaryResponseRegressionResults(Data,Yvar,Xvars,XvarsAlwaysIncluded,model)

%% Some checks
if size(Xvars,1)~=1
    error('Please make sure that Xvars is a horizontal cell array.')
end
if sum(unique(Data.(Yvar{1}))~=[0;1])>0
    error('The outcome variable does not seem to be a binary response variable')
end
if ~isempty(XvarsAlwaysIncluded)
    for i = 1:size(XvarsAlwaysIncluded,2)
        if ~strcmpi(Xvars{1,i},XvarsAlwaysIncluded{1,i})
            error('Please adapt Xvar in such a wat that the first variables of Xvar are equal to those in XvarsAlwaysIncluded')
        end
    end
end


%% Create table with a separate variable per iteration
% Create OutputTable
OutputTableSeparateVars = createOutputTable(Yvar,Xvars,model);

% Fill OutputTable with results
OutputTableSeparateVars = fillOutputTableSeparateVars(OutputTableSeparateVars,Data,Yvar,Xvars,XvarsAlwaysIncluded,model);

%% Create table with an additional variable after each iteration
% Create OutputTable
OutputTableAdditionalVars = createOutputTable(Yvar,Xvars,model);

% Fill OutputTable with results
OutputTableAdditionalVars = fillOutputTableAdditionalVars(OutputTableAdditionalVars,Data,Yvar,Xvars,model);



end


function OutputTable = createOutputTable(Yvar,Xvars,model)

% Initialize OutputTable
N_Xvars = size(Xvars,2);
OutputTable = cell((7+N_Xvars),(1+4*N_Xvars));

% Set up header
OutputTable(1,1) = {'Binary response regression model:'};
OutputTable(1,2) = {model};
OutputTable(2,1) = {'Dependent Variable:'};
OutputTable(2,2) = Yvar;
for i = 1:N_Xvars
    OutputTable(3,2+(i*4-4)) = {['(' num2str(i) ')']};
    OutputTable(4,2+(i*4-4):4+(i*4-4)) = {'Coefficient','z-stat','Average Marginal Effect'}; 
end

% Set up the variable name column
OutputTable(5,1) = {'Constant'};
for i = 1:N_Xvars
    OutputTable(5+i,1) = Xvars(i);
end

% Set up the number of observations row
OutputTable(7+N_Xvars,1) = {'N_obs==1'};
OutputTable(8+N_Xvars,1) = {'N_obs'};

end


function OutputTable = fillOutputTableSeparateVars(OutputTable,Data,Yvar,Xvars,XvarsAlwaysIncluded,model)

% Initialize variables
N_Xvars = size(Xvars,2);
N_XvarsAlwaysIncluded = size(XvarsAlwaysIncluded,2);

% Fill in table
for i = 1:N_Xvars
    
    % Run binary response regressions
    if strcmpi(Xvars{i},XvarsAlwaysIncluded)==1
        [b,stats,N,MeanMargEff] = runBinaryResponseRegression(Data,Yvar,XvarsAlwaysIncluded,model);
    else
        [b,stats,N,MeanMargEff] = runBinaryResponseRegression(Data,Yvar,[XvarsAlwaysIncluded Xvars(i)],model);
    end
    % Determine first column number for the specification
    firstColNumber = 2+(i*4-4);
    
    % Fill in results for constant
    OutputTable(5,firstColNumber) = {num2str(b(1),'%.3f')};
    OutputTable(5,firstColNumber+1) = {num2str(stats.t(1),'%.3f')};
    OutputTable(5,firstColNumber+2) = {num2str(MeanMargEff(1),'%.3f')};
    
    % Fill in results for XvarsAlwaysIncluded
    for j = 1:N_XvarsAlwaysIncluded
        OutputTable(5+j,firstColNumber) = {num2str(b(1+j),'%.3f')};
        OutputTable(5+j,firstColNumber+1) = {num2str(stats.t(1+j),'%.3f')};
        OutputTable(5+j,firstColNumber+2) = {num2str(MeanMargEff(1+j),'%.3f')};
    end
    if isempty(j)
        j=0;
    end
    
    % Fill in results for the Xvars if not already included in
    % XvarsAlwaysIncluded
    if strcmpi(Xvars{i},XvarsAlwaysIncluded)~=1
        OutputTable(5+i,firstColNumber) = {num2str(b(2+j),'%.3f')};
        OutputTable(5+i,firstColNumber+1) = {num2str(stats.t(2+j),'%.3f')};
        OutputTable(5+i,firstColNumber+2) = {num2str(MeanMargEff(2+j),'%.3f')};
    end
    
    % Fill in number of observations
    OutputTable(7+N_Xvars,firstColNumber) = {num2str(sum(Data.(Yvar{1})),'%.0f')};
    OutputTable(8+N_Xvars,firstColNumber) = {num2str(N,'%.0f')};
    
end




end

function OutputTable = fillOutputTableAdditionalVars(OutputTable,Data,Yvar,Xvars,model)

% Initialize variables
N_Xvars = size(Xvars,2);

% Fill in table
for i = 1:N_Xvars
    % Run binary response regressions
    [b,stats,N,MeanMargEff] = runBinaryResponseRegression(Data,Yvar,Xvars(1:i),model);
    
    % Determine first column number for the specification
    firstColNumber = 2+(i*4-4);
    
    % Fill in results for constant
    OutputTable(5,firstColNumber) = {num2str(b(1),'%.3f')};
    OutputTable(5,firstColNumber+1) = {num2str(stats.t(1),'%.3f')};
    OutputTable(5,firstColNumber+2) = {num2str(MeanMargEff(1),'%.3f')};
    
    for j = 1:i
        % Fill in results for the Xvars
        OutputTable(5+j,firstColNumber) = {num2str(b(1+j),'%.3f')};
        OutputTable(5+j,firstColNumber+1) = {num2str(stats.t(1+j),'%.3f')};
        OutputTable(5+j,firstColNumber+2) = {num2str(MeanMargEff(1+j),'%.3f')};
    end
    
    % Fill in number of observations
    OutputTable(7+N_Xvars,firstColNumber) = {num2str(sum(Data.(Yvar{1})),'%.0f')};
    OutputTable(8+N_Xvars,firstColNumber) = {num2str(N,'%.0f')};
end




end

