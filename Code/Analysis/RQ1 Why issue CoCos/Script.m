clear
%% Load data
pcinfo=java.net.InetAddress.getLocalHost;
if ~strcmpi(pcinfo.getHostName,'LAPTOP-6P6G4FB7')
    error('Havent set the path yet for the dell laptop');
%     pathname = '';
else
    pathname1 = 'C:\Users\rogie\Git\cocoandbankrisktaking\Code\';
end

% Load merged data
addpath(pathname1);
[DataIssuers,DataIssuersWithoutLloyds,DataNonIssuers] = loadData();
DataIssuers.CountryISOcode = categorical(DataIssuers.CountryISOcode);
Data = [DataIssuersWithoutLloyds;DataNonIssuers];

% Create some additional variables
Data.LogTotalAssets_EUR = log(Data.TotalAssets_EUR);
% Data.Leverage = (Data.TotalAssets_EUR-Data.TotalLiabilities_EUR)./Data.TotalAssets_EUR;
Data.Leverage = Data.TotalAssets_EUR./(Data.TotalAssets_EUR-Data.TotalLiabilities_EUR); Data.Leverage(isinf(Data.Leverage))=NaN;Data.Leverage(Data.Leverage<=0)=NaN;
Data.CashAndDueFromBanksOverTotalAssets = Data.CashAndDueFromBanks_EUR./Data.TotalAssets_EUR;
Data.NetInterestIncome_EUR(Data.NetInterestIncome_EUR<=0)=NaN;
Data.LogNetInterestIncome_EUR = log(Data.NetInterestIncome_EUR);
Data.LogCashAndDueFromBanks_EUR = log(Data.CashAndDueFromBanks_EUR);
Data.LogCashAndDueFromBanks_EUR(isinf(Data.LogCashAndDueFromBanks_EUR))=NaN;


%% Run cross-sectional binary response regressions using averages
Yvar = {'DummyIsIssuer'};
Xvars = {'LogTotalAssets_EUR','Leverage','ReturnOnAvgAssetsROAA','CostToIncomeRatio','RecurringEarningPower','LoanLossResGrossLoans','LoanLossProvNetIntRev','ImpairedLoansGrossLoans','NonInterestExpenseAverageAssets','NonOpItemsNetIncome','NonOpItemsTaxesAvgAst','NetLoansTotDepBor'};
DataCrossSectionally = varfun(@nanmean,Data(:,[{'BvDIDNumbers'} Yvar Xvars]),'GroupingVariables',{'BvDIDNumbers'});
DataCrossSectionally.Properties.VariableNames(3:end) = [Yvar Xvars];
XvarsAlwaysIncluded = {'LogTotalAssets_EUR'};

% Probit
[ProbitOutputTableSeparateVars,ProbitOutputTableAdditionalVars] = createTableWithBinaryResponseRegressionResults(DataCrossSectionally,Yvar,Xvars,XvarsAlwaysIncluded,'probit');

% Logit
[LogitOutputTableSeparateVars,LogitOutputTableAdditionalVars] = createTableWithBinaryResponseRegressionResults(DataCrossSectionally,Yvar,Xvars,XvarsAlwaysIncluded,'logit');

% % Linear probability
% [LinearOutputTableSeparateVars,LinearOutputTableAdditionalVars] = createTableWithBinaryResponseRegressionResults(DataCrossSectionally,Yvar,Xvars,'linear');

% Run a probit with only variables that came in significant
Xvars = {'LogTotalAssets_EUR','Leverage'};
% Xvars = {'LogTotalAssets_EUR','Leverage','TotalCapitalRatio','ReturnOnAvgAssetsROAA','ReturnOnAvgEquityROAE','LogNetInterestIncome_EUR','NetInterestMargin','NetInterestIncomeAverageEarningAssets','DividendPayOut','CostToIncomeRatio','RecurringEarningPower','InterbankRatio','NetLoansTotAssets','CashAndDueFromBanks_EUR','CashAndDueFromBanksOverTotalAssets'};
DataCrossSectionally = varfun(@nanmean,Data(:,[{'BvDIDNumbers'} Yvar Xvars]),'GroupingVariables',{'BvDIDNumbers'});
DataCrossSectionally.Properties.VariableNames(3:end) = [Yvar Xvars];
[ProbitOutputTableSeparateVars2,ProbitOutputTableAdditionalVars2] = createTableWithBinaryResponseRegressionResults(DataCrossSectionally,Yvar,Xvars,XvarsAlwaysIncluded,'probit');

%% Run binary response regressions with time-series element
Yvar = {'DummyIsIssuer'};
% Xvars = {'LogTotalAssets_EUR','Leverage','TotalCapitalRatio','ReturnOnAvgAssetsROAA','ReturnOnAvgEquityROAE','PreTaxOpIncAvgAssets','LogNetInterestIncome_EUR','NetInterestMargin','NetInterestIncomeAverageEarningAssets','DividendPayOut','CostToIncomeRatio','RecurringEarningPower','InterbankRatio','NetLoansTotAssets','CashAndDueFromBanks_EUR','CashAndDueFromBanksOverTotalAssets'};
Xvars = {'LogTotalAssets_EUR','Leverage','ReturnOnAvgEquityROAE','LogNetInterestIncome_EUR'};

% Probit
% [TSProbitOutputTableSeparateVars,TSProbitOutputTableAdditionalVarsTS] = createTableWithBinaryResponseRegressionResults(DataCrossSectionally,Yvar,Xvars,'probit');
[TSProbitOutputTableSeparateVars,TSProbitOutputTableAdditionalVarsTS] = createTableWithBinaryResponseRegressionResults(Data,Yvar,Xvars,XvarsAlwaysIncluded,'probit');

