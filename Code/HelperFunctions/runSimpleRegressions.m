function [results,R2,Fstat,Pval,N,N_obsEqualTo1] = runSimpleRegressions(Data,Yvar,Xvars,FE)

Ytemp = Data.(Yvar{1});
Xtemp = table2array(Data(:,Xvars));
IndexNaN = (isnan(Ytemp) | sum(isnan(Xtemp),2));
Data(IndexNaN,:)=[];
N_obsEqualTo1 = sum(Data.(Xvars{1})==1);
varNames = Xvars';

% I have implemented FE using demeaning instead of dummies. This will
% (hopefully) lead to quick code.
X = table2array(Data(:,Xvars));
y = table2array(Data(:,Yvar));
N_FEs = 0;
if ~cellfun(@isempty,FE)
    for i = FE
        [X,y,N_FEs] = implementFEs(Data,X,y,i,N_FEs);
    end
    varNames = [Xvars'];
else
    % Remove missing observations
    IndexNaN = isnan(y) | isnan(sum(X,2));
    y(IndexNaN) = [];
    X(IndexNaN,:) = [];
    
    % Add constant
    X = [ones(size(y)) X];
    varNames = [{'constant'};Xvars'];
end


N = size(y,1);
k = size(X,2);
[b,~,res,~,stats] = regress(y,X);

% Calculate the standard errors of the beta estimates
if ~cellfun(@isempty,FE)
    s2 = 1/(N-(N_FEs+k))*(res'*res);
else
    s2 = 1/(N-k)*(res'*res);
end
Varb = s2*inv(X'*X);
se = diag(Varb).^0.5;
t = b./se;
R2 = stats(1);
Fstat = stats(2);
Pval = stats(3);

% Nicely format output into table
results = cell2table([varNames,num2cell([b,se,t])],'VariableNames',{'Variables','beta','se','t_stat'});

end




function [X,y,N_FEs] = implementFEs(panelData,X,y,i,N_FEs)

% Remove missing observations of the FE indicated by i
D = dummyvar(categorical(panelData.(i{1})));
IndexNaN = isnan(y) | isnan(sum(X,2)) | isnan(sum(D,2));
y(IndexNaN,:) = [];
X(IndexNaN,:) = [];
D(IndexNaN,:) = [];
D(:,sum(D,1)==0)=[];
N_FEs = N_FEs + size(D,2);

bTemp = (D'*D)\D'*y;
y = y-D*bTemp;
bTemp = (D'*D)\D'*X;
X = X-D*bTemp;

end