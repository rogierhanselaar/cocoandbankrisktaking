function [DataIssuers,DataIssuersWithoutLloyds,DataNonIssuers] = loadData()

%% Load data
pcinfo=java.net.InetAddress.getLocalHost;
if ~strcmpi(pcinfo.getHostName,'LAPTOP-6P6G4FB7')
    pathname = 'C:\Users\54167rha\Dropbox\PhD\Data\ProjectsWithProfPurnanandam_RogierHanselaar\CoCosAndRiskTaking\Data\';
else
    pathname = 'C:\Users\rogie\Dropbox\PhD\Data\ProjectsWithProfPurnanandam_RogierHanselaar\CoCosAndRiskTaking\Data\';
end

% Load processed BankScope data
load(strcat(pathname,'MergedAndProcessedData.mat'));

% Only include banks in countries for which there is information on fiscal
% treatment
Data(isnan(Data.DummyFiscalTreatment),:)=[];

% Separate firms into those that have issued cocos and those that have not
FirmsThatIssuedCoCos = array2table(unique(Data.BvDIDNumbers(Data.CoCoIssued>0)),'VariableNames',{'BvDIDNumbers2'});
Data = outerjoin(Data,FirmsThatIssuedCoCos,...
    'Type','Left',...
    'LeftKeys',{'BvDIDNumbers'},...
    'RightKeys',{'BvDIDNumbers2'},...
    'RightVariables',{'BvDIDNumbers2'});
IndexNonIssuers = isundefined(Data.BvDIDNumbers2);
Data.DummyIsIssuer = ~IndexNonIssuers;
DataNonIssuers = Data(IndexNonIssuers,:);
DataIssuers = Data(~IndexNonIssuers,:);
DataIssuers.BvDIDNumbers2=[];DataNonIssuers.BvDIDNumbers2=[];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Lowest TA: 'Zuercher Kantonalbank Oesterreich AG' with 93 million EUR in
% Total Assets ---> Investigate further: It should be a bank with more than 150
% Billion CHF in total Assets 
% Note: overwegen Zuercher Kantonalbank Oesterreich AG te verwijderen op
% basis van het feit dat de CoCo issue een veelvoud is van de total assets;
% of overwegen alle government banks te identificeren en te verwijderen
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Remove 'Zuercher Kantonalbank Oesterreich AG' from the data
DataIssuers(DataIssuers.BvDIDNumbers==categorical({'AT38589'}),:)=[];

% Remove Lloyds from DataIssuers in new table
DataIssuersWithoutLloyds = DataIssuers;
DataIssuersWithoutLloyds(DataIssuersWithoutLloyds.ISINNumber==categorical({'GB0008706128'}),:)=[];

% Keep only those non-issuers that are larger than the smallest issuer in
% terms of total assets.
varsToSelectOn = {'TotalAssets_EUR'};
DataNonIssuers = removeDeviatingNonIssuers(DataIssuers,DataNonIssuers,varsToSelectOn);

% --> Think about programming one-to-one matching here, currently there are
% 50 non-issuers for every issuer... On the other hand, matching on things
% like size allows for less to explain why some firms issue CoCo's and why
% others do not


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Old Stuff %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Assign own BankID to each BvDIDNumber
% DataIssuers.BankID = categorical(grp2idx(cellstr(DataIssuers.BvDIDNumbers)));
% 
% % Assign own CountryID to each CountryISOcode
% DataIssuers.CountryID = categorical(grp2idx(cellstr(DataIssuers.CountryISOcode)));


end


function DataNonIssuers = removeDeviatingNonIssuers(DataIssuers,DataNonIssuers,varsToSelectOn)
NonIssuersToBeRemoved = categorical();
for v = varsToSelectOn
    Minimum = min(DataIssuers.(v{1})); 
    NonIssuersToBeRemoved = [NonIssuersToBeRemoved;unique(DataNonIssuers.BvDIDNumbers(DataNonIssuers.(v{1})<Minimum))];
end
IndexRemove = zeros(size(DataNonIssuers,1),1);
for i = 1:size(NonIssuersToBeRemoved,1)
    IndexRemove = IndexRemove | DataNonIssuers.BvDIDNumbers==NonIssuersToBeRemoved(i);
end
DataNonIssuers(IndexRemove,:)=[];
end
