function [DataU,DataC,Data] = loadData()

%% Load data
pathname = 'C:\Users\rogie\Dropbox\PhD\Data\ProjectsWithProfPurnanandam_RogierHanselaar\CoCosAndRiskTaking\Data\';

% Load processed BankScope data
load(strcat(pathname,'20170728MergedAndProcessedDataUnconsolidated2000_2016.mat'));
load(strcat(pathname,'20170921MergedAndProcessedDataConsolidated2000_2016.mat'));
load(strcat('C:\Users\rogie\Dropbox\PhD\Data\ProjectsWithProfPurnanandam_RogierHanselaar\CoCosAndRiskTaking\Data\','MergedAndProcessedData.mat'));

% Add relative coco size and levereage
DataC.CoCoRelativeSize = 100*DataC.CoCoIssuedAmountIssued_EUR_cumsum./DataC.TotalAssets_EUR;
DataC.Leverage_1minusEoverA = 1 - DataC.EquityToAssetsRatio;

% Create fraction of coco type issued (write-down vs equity conversion)
DataC.FractionEquityCoCo_outstanding = DataC.CoCoIssuedEquityConversionType_cumsum./DataC.CoCoIssued_cumsum;
DataC.FractionEquityCoCo_outstanding(isnan(DataC.FractionEquityCoCo_outstanding))=0;
DataC.FractionWriteDownCoCo_outstanding = DataC.CoCoIssuedWriteDownType_cumsum./DataC.CoCoIssued_cumsum;
DataC.FractionWriteDownCoCo_outstanding(isnan(DataC.FractionWriteDownCoCo_outstanding ))=0;

% Create dummy that is one if a firm has issued a write-down coco in that
% or prior years AND not issued an equity conversion coco in that or prior
% years
DataC = createCoCoTypeDummies(DataC);

% Winsorize and lag variables
DataC = sortrows(DataC,'FiscalYear','ascend');
DataC = sortrows(DataC,'BvDIDNumbers','ascend');
IndexSetToNaN = logical([1;DataC.BvDIDNumbers(2:end)~=DataC.BvDIDNumbers(1:end-1)]);
for v = {'ImpairedLoansSlashGrossLoans','LoanLossResSlashGrossLoans','LogTotalAssets_EUR','Leverage3','TaxesSlashPreDashtaxProfit','ReturnOnAvgAssetsROAA','AnnualVol_pct','EquityToAssetsRatio','NetInterestMargin','CoCoRelativeSize','CoCoIssuedAmountIssued_EUR_cumsum','Leverage_1minusEoverA'}
    DataC.(strcat(v{1},'_wp1p99')) = winsor(DataC.(v{1}), [1 99]);
    DataC.(strcat(v{1},'_wp1p99_Lag1')) = [NaN;DataC.(strcat(v{1},'_wp1p99'))(1:end-1)];
    DataC.(strcat(v{1},'_wp1p99_Lag1'))(IndexSetToNaN) = NaN;
end

end

function DataC = createCoCoTypeDummies(DataC)

BvDIDNumbers_issuers = unique(DataC.BvDIDNumbers(DataC.DummyHasIssuedCoCo>0));
BvDIDNumbers_issuers_write_down_coco = unique(DataC.BvDIDNumbers(DataC.DummyHasIssuedCoCoWriteDownType>0));
BvDIDNumbers_issuers_equity_conversion_coco = unique(DataC.BvDIDNumbers(DataC.DummyHasIssuedCoCoEquityConversionType>0));
BvDIDNumbers_issuers_write_down_coco_pure = setdiff(BvDIDNumbers_issuers_write_down_coco, BvDIDNumbers_issuers_equity_conversion_coco);
BvDIDNumbers_issuers_equity_conversion_coco_pure = setdiff(BvDIDNumbers_issuers_equity_conversion_coco, BvDIDNumbers_issuers_write_down_coco);
BvDIDNumbers_issuers_hybrid_coco_pure = setdiff(setdiff(BvDIDNumbers_issuers, BvDIDNumbers_issuers_write_down_coco_pure), BvDIDNumbers_issuers_equity_conversion_coco_pure);

N = size(DataC,1);
DataC.DummyIsIssuer = zeros(N,1);
for f = BvDIDNumbers_issuers'
    IndexFirm = DataC.BvDIDNumbers==f;
    DataC.DummyIsIssuer(IndexFirm) = 1;
end

N = size(DataC,1);
DataC.DummyHasIssuedCoCo_write_down_pure = zeros(N,1);
for f = BvDIDNumbers_issuers_write_down_coco_pure'
    IndexFirm = DataC.BvDIDNumbers==f;
    DataC.DummyHasIssuedCoCo_write_down_pure(IndexFirm) = DataC.DummyHasIssuedCoCo(IndexFirm);
end

DataC.DummyHasIssuedCoCo_equity_conversion_pure = zeros(N,1);
for f = BvDIDNumbers_issuers_equity_conversion_coco_pure'
    IndexFirm = DataC.BvDIDNumbers==f;
    DataC.DummyHasIssuedCoCo_equity_conversion_pure(IndexFirm) = DataC.DummyHasIssuedCoCo(IndexFirm);
end

DataC.DummyHasIssuedCoCo_hybrid_pure = zeros(N,1);
for f = BvDIDNumbers_issuers_hybrid_coco_pure'
    IndexFirm = DataC.BvDIDNumbers==f;
    DataC.DummyHasIssuedCoCo_hybrid_pure(IndexFirm) = DataC.DummyHasIssuedCoCo(IndexFirm);
end

end



