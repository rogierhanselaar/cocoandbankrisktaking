function [b,stats,N,MeanMargEff] = runBinaryResponseRegression(Data,Yvar,Xvars,model)

y = Data.(Yvar{1})+0;
X = table2array(Data(:,Xvars));

% Remove missing values
IndexNaN = sum(isnan(X),2)>0 | isnan(y);
Data(IndexNaN,:)=[];
y(IndexNaN)=[];
X(IndexNaN,:)=[];
N = size(y,1);

% Run binary response regression
if strcmpi(model,'probit')
    [b,~,stats] = glmfit(X,y,'binomial','link','probit');
    MeanMargEff = b.*mean(normpdf([ones(N,1) X]*b));
elseif strcmpi(model,'logit')
    [b,~,stats] = glmfit(X,y,'binomial','link','logit');
    MeanMargEff = b.*mean(exp([ones(N,1) X]*b)./((1+exp([ones(N,1) X]*b)).^2));
elseif strcmpi(model,'linear')
    [stats,~,~,~,N] = runSimpleRegressions(Data,Yvar,Xvars,{});
    b = stats.beta;
    stats.t = stats.t_stat;
    MeanMargEff = stats.beta;
else
    error('The model should be either "probit", "logit", or "linear"')
end



end

