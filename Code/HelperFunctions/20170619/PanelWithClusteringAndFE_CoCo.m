function [results,Rsquared,Fstat,Pval,N,N_obsEqualTo1,N_FirmFEs] = PanelWithClusteringAndFE_CoCo(panelData, Yvariable, Xvariables, FE, clustering)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INPUTS:
% - panelData: table containing all the required data
% - Yvariable: cell with string containing the name of the y-variable
% - Xvariables: horzontal cell-array with strings containing the names of
%               the x-variables
% - FE: horzontal cell-array with strings containing the names of the
%       variables for which fixed effects should be included
% - clustering: horzontal cell-array with strings containing the names of
%               the variables on which the SEs should be clustered
%
% OUTPUTS:
% - b: beta estimates
% - se: estimates of the standard errors of b
% - t: t-statistics
%
% Based on "Robust Inference with Multi-way Clustering" by A. Colin
% Cameron, Jonah B. Gelbach, and Douglas L. Miller. 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if size(clustering,1)>1 || size(FE,1)>1 || ~iscellstr(FE) || ~iscellstr(clustering)
    error('Please insert the names of the variables to be included as FE and the names of the variables to be clustered on as a one row cell array of strings')
end

if size(clustering,2)>2
    error('For now, clustering on zero, one or two dimensions is supported')
end


% Remove missing observations
X = table2array(panelData(:,Xvariables));
y = table2array(panelData(:,Yvariable));
IndexNaN = isnan(y) | isnan(sum(X,2));
if ~cellfun(@isempty,FE)
    for i = FE
        D = dummyvar(categorical(panelData.(i{1})));
        IndexNaN = IndexNaN | sum(isnan(D),2)>0;
    end
end
panelData(IndexNaN,:)=[];
X = table2array(panelData(:,Xvariables));
y = table2array(panelData(:,Yvariable));

% Set number of CoCo issues equal to 1
N_obsEqualTo1 = sum(panelData.(Xvariables{1})==1);

% Count number of Firm FEs
N_FirmFEs = size(unique(panelData.BvDIDNumbers),1);

% I have implemented FE using differencing instead of dummies. This will
% (hopefully) lead to quick code.
if ~cellfun(@isempty,FE)
    for i = FE
        D = dummyvar(categorical(grp2idx(categorical(panelData.(i{1})))));
        N_FirmFEs = size(D,2);
        
        bTemp = (D'*D)\D'*y;
        y = y-D*bTemp;
        bTemp = (D'*D)\D'*X;
        X = X-D*bTemp;
    end
    varNames = [Xvariables'];
else
    X = [ones(size(y)) X];
    varNames = [{'constant'};Xvariables'];
end
[N, k] = size(X);

% Calculate beta estimates
b = (X'*X)\X'*y;

% Calculate residuals
res = y-X*b;

% Calculate within Rsquared
Rsquared_within = 1 - (res'*res)/((y-mean(y))'*(y-mean(y)));

% Calculate  Rsquared
y_nondemeaned = table2array(panelData(:,Yvariable));
Rsquared = 1 - (res'*res)/((y_nondemeaned-mean(y_nondemeaned))'*(y_nondemeaned-mean(y_nondemeaned)));


%%% Clustering
% Create clustered variance matrix, and correct in the selection matrix for
% small-sample modifications in the same way Stata does, according to
% "Robust Inference with Multi-way Clustering" by A. Colin Cameron, Jonah
% B. Gelbach, and Douglas L. Miller. In practice, Stata seems to use a
% slightly different (more lenient) calculation of the SEs. The differences
% are most likely in the small sample corrections.
if ~cellfun(@isempty,clustering)
    SelMat = zeros(size(res*res'));
    for i = clustering
        SelMat = SelMat + CreateSelectionMatrix(panelData.(i{1}),res,N,k);
    end
    if size(clustering,2)==2
        SelMat = SelMat - CreateSelectionMatrix(table2array(panelData(:,clustering)),res,N,k);
    end
    
    % Calculate the variance matrix of beta estimates
    Varb = (X'*X)\X'*((res*res').*SelMat)*X/(X'*X);
else
    Varb = var(res)*inv(X'*X);
end

% Calculate the standard errors of the beta estimates
se = sqrt(diag(Varb));
if ~cellfun(@isempty,FE)
    g = size(D,2);
    k = k+g;
    se = se*sqrt((N-k+g)/(N-k));
end
t = b./se;

% Nicely format output into table
results = cell2table([varNames,num2cell([b,se,t])],'VariableNames',{'Variables','beta','se','t_stat'});

% Set remaining output to missing
Fstat = NaN;
Pval = NaN;

end

function S = CreateSelectionMatrix(clusVar,res,N,k)
if ~isempty(clusVar)
    S = zeros(size(res*res'));
    G = unique(clusVar,'rows');
    N_G = size(G,1);
    correction = (N_G/(N_G-1))*((N-1)/(N-k));
    for j = 1:N_G
        locObsInClus = find(ismember(clusVar,G(j,:),'rows'));
        S(locObsInClus,locObsInClus) = 1*correction;
    end
    
end
end


