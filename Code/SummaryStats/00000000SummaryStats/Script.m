clear
%% Load data
pathname = 'C:\Users\54167rha\Dropbox\PhD\Data\ProjectsWithProfPurnanandam_RogierHanselaar\CoCosAndRiskTaking\Data\BankScopeData\';
load(strcat(pathname,'ProcessedBankScopeData.mat'));
vars_level = {'TotalAssets_EUR','TradingLiabilities_EUR','OtherOperatingIncome_EUR','NetInterestIncome_EUR','TradingAssetsDebtSecuritieswherenoissuerbreakdown_EUR','TradingAssetsDebtSecuritiesGovernments_EUR','TradingAssetsDebtSecuritiesBanks_EUR','TradingAssetsDebtSecuritiesCorporates_EUR','TradingAssetsDebtSecuritiesStructured_EUR','TradingAssetsDebtSecuritiesOther_EUR','TradingAssetsEquities_EUR','TradingAssetsCommodities_EUR','TradingAssetsOther_EUR'};
vars_ratio = {'LoanLossResGrossLoans','LoanLossProvNetIntRev','LoanLossResImpairedLoans','ImpairedLoansGrossLoans','NCOAverageGrossLoans','NCONetIncBefLnLssProv','ImpairedLoansEquity','UnreservedImpairedLoansEquity','NetInterestMargin','PreTaxOpIncAvgAssets','NonOpItemsTaxesAvgAst','ReturnOnAvgAssetsROAA','ReturnOnAvgEquityROAE','DividendPayOut','IncNetOfDistAvgEquity','NonOpItemsNetIncome','CostToIncomeRatio','RecurringEarningPower','InterbankRatio','NetLoansTotAssets','NetLoansDepSTFunding','NetLoansTotDepBor','LiquidAssetsDepSTFunding','LiquidAssetsTotDepBor','NetInterestIncome','NetInterestIncomeAverageEarningAssets','NonInterestExpenseAverageAssets'};

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% It would make sense to merge the BankScope data also with the Bloomberg
% data on CoCo issuance and then make some additional sum stats about it
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Create overall info
N_countries = size(unique(Data.CountryISOcode(Data.FiscalYear~=2008)),1);
N_banks = size(unique(Data.BvDIDNumbers(Data.FiscalYear~=2008)),1);
N_years = size(unique(Data.FiscalYear(Data.FiscalYear~=2008)),1);
N_obs = size(Data(Data.FiscalYear~=2008,:),1);

N_countries_withISIN = size(unique(Data.CountryISOcode(Data.FiscalYear~=2008 & ~cell2mat(cellfun(@isempty,Data.ISINNumber,'UniformOutput',false)))),1);
N_banks_withISIN = size(unique(Data.BvDIDNumbers(Data.FiscalYear~=2008 & ~cell2mat(cellfun(@isempty,Data.ISINNumber,'UniformOutput',false)))),1);
N_years_withISIN = size(unique(Data.FiscalYear(Data.FiscalYear~=2008 & ~cell2mat(cellfun(@isempty,Data.ISINNumber,'UniformOutput',false)))),1);
N_obs_withISIN = size(Data(Data.FiscalYear~=2008 & ~cell2mat(cellfun(@isempty,Data.ISINNumber,'UniformOutput',false)),:),1);


%% Create table with country means
% Calculate the number of banks and number of obs per country 
tempData = unique(Data(Data.FiscalYear~=2008,{'CountryName','BvDIDNumbers'}));
Country_N_banks = varfun(@(x) nanmean(double(x)),tempData,'InputVariables','BvDIDNumbers','GroupingVariables',{'CountryName'});Country_N_banks.Fun_BvDIDNumbers=[];
Country_N_obs = varfun(@(x) nanmean(double(x)),Data(Data.FiscalYear~=2008,:),'InputVariables','BvDIDNumbers','GroupingVariables',{'CountryName'});Country_N_obs.Fun_BvDIDNumbers=[];
tempData = unique(Data(Data.FiscalYear~=2008 & ~cell2mat(cellfun(@isempty,Data.ISINNumber,'UniformOutput',false)),{'CountryName','BvDIDNumbers'}));
Country_N_banks_withISIN = varfun(@(x) nanmean(double(x)),tempData,'InputVariables','BvDIDNumbers','GroupingVariables',{'CountryName'});Country_N_banks_withISIN.Fun_BvDIDNumbers=[];
Country_N_obs_withISIN = varfun(@(x) nanmean(double(x)),Data(Data.FiscalYear~=2008 & ~cell2mat(cellfun(@isempty,Data.ISINNumber,'UniformOutput',false)),:),'InputVariables','BvDIDNumbers','GroupingVariables',{'CountryName'});Country_N_obs_withISIN.Fun_BvDIDNumbers=[];

% Calculate country means
TableCountryMeans = table(unique(Data.CountryName));TableCountryMeans.Properties.VariableNames(1) = {'CountryName'};
TableCountryMeans_withISIN = table(unique(Data.CountryName));TableCountryMeans_withISIN.Properties.VariableNames(1) = {'CountryName'};
for v = [vars_level vars_ratio]
    CountryMeans = varfun(@mean,Data(Data.FiscalYear~=2008 & ~isnan(Data.(v{1})),:),'InputVariables',v,'GroupingVariables',{'CountryName'});
    CountryMeans.Properties.VariableNames{'GroupCount'} = strcat('N_obs_',v{1});
    TableCountryMeans = outerjoin(TableCountryMeans,CountryMeans,...
        'Type','Left',...
        'LeftKeys',{'CountryName'},...
        'RightKeys',{'CountryName'},...
        'RightVariables',{strcat('N_obs_',v{1}),strcat('mean_',v{1})});
    CountryMeans_withISIN = varfun(@mean,Data(Data.FiscalYear~=2008 & ~isnan(Data.(v{1})) & ~cell2mat(cellfun(@isempty,Data.ISINNumber,'UniformOutput',false)),:),'InputVariables',v,'GroupingVariables',{'CountryName'});
    CountryMeans_withISIN.Properties.VariableNames{'GroupCount'} = strcat('N_obs_',v{1});
    TableCountryMeans_withISIN = outerjoin(TableCountryMeans_withISIN,CountryMeans_withISIN,...
        'Type','Left',...
        'LeftKeys',{'CountryName'},...
        'RightKeys',{'CountryName'},...
        'RightVariables',{strcat('N_obs_',v{1}),strcat('mean_',v{1})});
end
writetable(TableCountryMeans,'TableByCountry.xlsx')
writetable(TableCountryMeans_withISIN,'TableByCountry_withISIN.xlsx')

%% Create table with year means
% Calculate the number of banks and number of obs per country 
tempData = unique(Data(Data.FiscalYear~=2008,{'FiscalYear','BvDIDNumbers'}));
Year_N_banks = varfun(@(x) nanmean(double(x)),tempData,'InputVariables','BvDIDNumbers','GroupingVariables',{'FiscalYear'});Year_N_banks.Fun_BvDIDNumbers=[];
Year_N_obs = varfun(@(x) nanmean(double(x)),Data(Data.FiscalYear~=2008,:),'InputVariables','BvDIDNumbers','GroupingVariables',{'FiscalYear'});Year_N_obs.Fun_BvDIDNumbers=[];
tempData = unique(Data(Data.FiscalYear~=2008 & ~cell2mat(cellfun(@isempty,Data.ISINNumber,'UniformOutput',false)),{'FiscalYear','BvDIDNumbers'}));
Year_N_banks_withISIN = varfun(@(x) nanmean(double(x)),tempData,'InputVariables','BvDIDNumbers','GroupingVariables',{'FiscalYear'});Year_N_banks_withISIN.Fun_BvDIDNumbers=[];
Year_N_obs_withISIN = varfun(@(x) nanmean(double(x)),Data(Data.FiscalYear~=2008 & ~cell2mat(cellfun(@isempty,Data.ISINNumber,'UniformOutput',false)),:),'InputVariables','BvDIDNumbers','GroupingVariables',{'FiscalYear'});Year_N_obs_withISIN.Fun_BvDIDNumbers=[];

% Calculate year means
TableYearMeans = table(unique(Data.FiscalYear(Data.FiscalYear~=2008)));TableYearMeans.Properties.VariableNames(1) = {'FiscalYear'};
TableYearMeans_withISIN = table(unique(Data.FiscalYear(Data.FiscalYear~=2008)));TableYearMeans_withISIN.Properties.VariableNames(1) = {'FiscalYear'};
for v = [vars_level vars_ratio]
    YearMeans = varfun(@mean,Data(Data.FiscalYear~=2008 & ~isnan(Data.(v{1})),:),'InputVariables',v,'GroupingVariables',{'FiscalYear'});
    YearMeans.Properties.VariableNames{'GroupCount'} = strcat('N_obs_',v{1});
    TableYearMeans = outerjoin(TableYearMeans,YearMeans,...
        'Type','Left',...
        'LeftKeys',{'FiscalYear'},...
        'RightKeys',{'FiscalYear'},...
        'RightVariables',{strcat('N_obs_',v{1}),strcat('mean_',v{1})});
    YearMeans_withISIN = varfun(@mean,Data(Data.FiscalYear~=2008 & ~isnan(Data.(v{1})) & ~cell2mat(cellfun(@isempty,Data.ISINNumber,'UniformOutput',false)),:),'InputVariables',v,'GroupingVariables',{'FiscalYear'});
    YearMeans_withISIN.Properties.VariableNames{'GroupCount'} = strcat('N_obs_',v{1});
    TableYearMeans_withISIN = outerjoin(TableYearMeans_withISIN,YearMeans_withISIN,...
        'Type','Left',...
        'LeftKeys',{'FiscalYear'},...
        'RightKeys',{'FiscalYear'},...
        'RightVariables',{strcat('N_obs_',v{1}),strcat('mean_',v{1})});
end
writetable(TableYearMeans,'TableYearMeans.xlsx')
writetable(TableYearMeans_withISIN,'TableYearMeans_withISIN.xlsx')

