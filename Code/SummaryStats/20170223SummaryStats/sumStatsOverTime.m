function SumStats = sumStatsOverTime(DataIssuers,DataNonIssuers,variableNames)

years = unique(year(DataIssuers.CloseDateDN));
for v = variableNames
    % Calculate averages and medians per year
    SumStats.(v{1}).Means.Years = [years;0;1];
    SumStats.(v{1}).Means.Issuers = NaN(size(years,1)+2,1);
    SumStats.(v{1}).Means.IssuersObs = NaN(size(years,1)+2,1);
    SumStats.(v{1}).Means.IssuersCoverage = NaN(size(years,1)+2,1);
    SumStats.(v{1}).Means.NonIssuers = NaN(size(years,1)+2,1);
    SumStats.(v{1}).Means.NonIssuersObs = NaN(size(years,1)+2,1);
    SumStats.(v{1}).Means.NonIssuersCoverage = NaN(size(years,1)+2,1);
    SumStats.(v{1}).Medians.Years = [years;0;1];
    SumStats.(v{1}).Medians.Issuers = NaN(size(years,1)+2,1);
    SumStats.(v{1}).Medians.IssuersCoverage = NaN(size(years,1)+2,1);
    SumStats.(v{1}).Medians.NonIssuers = NaN(size(years,1)+2,1);
    SumStats.(v{1}).Medians.NonIssuersCoverage = NaN(size(years,1)+2,1);
    for y = [years' 0 1]
        if y>1
            IndexYIssuers = DataIssuers.FiscalYear==y;
            IndexYNonIssuers = DataNonIssuers.FiscalYear==y;
            SumStats.(v{1}).Means.Issuers(SumStats.(v{1}).Means.Years==y) = nanmean(DataIssuers.(v{1})(IndexYIssuers));
            SumStats.(v{1}).Means.IssuersObs(SumStats.(v{1}).Means.Years==y) = sum(~isnan(DataIssuers.(v{1})(IndexYIssuers)));
            SumStats.(v{1}).Means.IssuersCoverage(SumStats.(v{1}).Means.Years==y) = sum(~isnan(DataIssuers.(v{1})(IndexYIssuers)))/size(DataIssuers(IndexYIssuers,:),1);
            SumStats.(v{1}).Means.NonIssuers(SumStats.(v{1}).Means.Years==y) = nanmean(DataNonIssuers.(v{1})(IndexYNonIssuers));
            SumStats.(v{1}).Means.NonIssuersObs(SumStats.(v{1}).Means.Years==y) = sum(~isnan(DataNonIssuers.(v{1})(IndexYNonIssuers)));
            SumStats.(v{1}).Means.NonIssuersCoverage(SumStats.(v{1}).Means.Years==y) = sum(~isnan(DataNonIssuers.(v{1})(IndexYNonIssuers)))/size(DataNonIssuers(IndexYNonIssuers,:),1);
            SumStats.(v{1}).Medians.Issuers(SumStats.(v{1}).Medians.Years==y) = nanmedian(DataIssuers.(v{1})(year(DataIssuers.CloseDateDN)==y));
            SumStats.(v{1}).Medians.IssuersCoverage(SumStats.(v{1}).Medians.Years==y) = sum(~isnan(DataIssuers.(v{1})(IndexYIssuers)))/size(DataIssuers(IndexYIssuers,:),1);
            SumStats.(v{1}).Medians.NonIssuers(SumStats.(v{1}).Medians.Years==y) = nanmedian(DataNonIssuers.(v{1})(IndexYNonIssuers));
            SumStats.(v{1}).Medians.NonIssuersCoverage(SumStats.(v{1}).Medians.Years==y) = sum(~isnan(DataNonIssuers.(v{1})(IndexYNonIssuers)))/size(DataNonIssuers(IndexYNonIssuers,:),1);
        elseif y==0
            SumStats.(v{1}).Means.Issuers(SumStats.(v{1}).Means.Years==y) = nanmean(DataIssuers.(v{1})(DataIssuers.DummyOneOrMoreCoCosIssued==0));
            SumStats.(v{1}).Means.IssuersObs(SumStats.(v{1}).Means.Years==y) = sum(~isnan(DataIssuers.(v{1})(DataIssuers.DummyOneOrMoreCoCosIssued==0)));
            SumStats.(v{1}).Means.IssuersCoverage(SumStats.(v{1}).Means.Years==y) = sum(~isnan(DataIssuers.(v{1})(DataIssuers.DummyOneOrMoreCoCosIssued==0)))/size(DataIssuers(DataIssuers.DummyOneOrMoreCoCosIssued==0,:),1);
            SumStats.(v{1}).Means.NonIssuers(SumStats.(v{1}).Means.Years==y) = nanmean(DataNonIssuers.(v{1})(DataNonIssuers.DummyOneOrMoreCoCosIssued==0));
            SumStats.(v{1}).Means.NonIssuersObs(SumStats.(v{1}).Means.Years==y) = sum(~isnan(DataNonIssuers.(v{1})(DataNonIssuers.DummyOneOrMoreCoCosIssued==0)));
            SumStats.(v{1}).Means.NonIssuersCoverage(SumStats.(v{1}).Means.Years==y) = sum(~isnan(DataNonIssuers.(v{1})(DataNonIssuers.DummyOneOrMoreCoCosIssued==0)))/size(DataNonIssuers(DataNonIssuers.DummyOneOrMoreCoCosIssued==0,:),1);
            SumStats.(v{1}).Medians.Issuers(SumStats.(v{1}).Medians.Years==y) = nanmedian(DataIssuers.(v{1})(DataIssuers.DummyOneOrMoreCoCosIssued==0));
            SumStats.(v{1}).Medians.IssuersCoverage(SumStats.(v{1}).Medians.Years==y) = sum(~isnan(DataIssuers.(v{1})(DataIssuers.DummyOneOrMoreCoCosIssued==0)))/size(DataIssuers(DataIssuers.DummyOneOrMoreCoCosIssued==0,:),1);
            SumStats.(v{1}).Medians.NonIssuers(SumStats.(v{1}).Medians.Years==y) = nanmedian(DataNonIssuers.(v{1})(DataNonIssuers.DummyOneOrMoreCoCosIssued==0));
            SumStats.(v{1}).Medians.NonIssuersCoverage(SumStats.(v{1}).Medians.Years==y) = sum(~isnan(DataNonIssuers.(v{1})(DataNonIssuers.DummyOneOrMoreCoCosIssued==0)))/size(DataNonIssuers(DataNonIssuers.DummyOneOrMoreCoCosIssued==0,:),1);
        elseif y==1
            SumStats.(v{1}).Means.Issuers(SumStats.(v{1}).Means.Years==y) = nanmean(DataIssuers.(v{1})(DataIssuers.DummyOneOrMoreCoCosIssued==1));
            SumStats.(v{1}).Means.IssuersObs(SumStats.(v{1}).Means.Years==y) = sum(~isnan(DataIssuers.(v{1})(DataIssuers.DummyOneOrMoreCoCosIssued==1)));
            SumStats.(v{1}).Means.IssuersCoverage(SumStats.(v{1}).Means.Years==y) = sum(~isnan(DataIssuers.(v{1})(DataIssuers.DummyOneOrMoreCoCosIssued==1)))/size(DataIssuers(DataIssuers.DummyOneOrMoreCoCosIssued==1,:),1);
            SumStats.(v{1}).Means.NonIssuers(SumStats.(v{1}).Means.Years==y) = NaN;
            SumStats.(v{1}).Medians.Issuers(SumStats.(v{1}).Medians.Years==y) = nanmedian(DataIssuers.(v{1})(DataIssuers.DummyOneOrMoreCoCosIssued==1));
            SumStats.(v{1}).Medians.IssuersCoverage(SumStats.(v{1}).Medians.Years==y) = sum(~isnan(DataIssuers.(v{1})(DataIssuers.DummyOneOrMoreCoCosIssued==1)))/size(DataIssuers(DataIssuers.DummyOneOrMoreCoCosIssued==1,:),1);
            SumStats.(v{1}).Medians.NonIssuers(SumStats.(v{1}).Medians.Years==y) = NaN;
        end
    end
    % Transform structs into tables
    SumStats.(v{1}).Means = struct2table(SumStats.(v{1}).Means);
    SumStats.(v{1}).Medians = struct2table(SumStats.(v{1}).Medians);
end




end