clear
%% Load data
pcinfo=java.net.InetAddress.getLocalHost;
if ~strcmpi(pcinfo.getHostName,'LAPTOP-6P6G4FB7')
    error('Havent set the path yet for the dell laptop');
%     pathname = '';
else
    pathname1 = 'C:\Users\rogie\Git\cocoandbankrisktaking\Data\';
    pathname2 = 'C:\Users\rogie\Git\usefulmatlabfunctions\';
    pathname3 = 'C:\Users\rogie\Dropbox\PhD\Data\ProjectsWithProfPurnanandam_RogierHanselaar\CoCosAndRiskTaking\Data\BloombergData\Issues\';
    pathname4 = 'C:\Users\rogie\Git\cocoandbankrisktaking\Data\MergeData\20170619MergeData_2000_2016\';
end

% Load merged data
addpath(pathname1);
addpath(pathname2);
[~,DataC,~] = loadData();

% Load unmerged issuance data
addpath(pathname4);
DataCoCoIssue = importCoCoIssueData(strcat(pathname3,'20161121BloombergDataCocos.xlsx'));
FXData = loadFXData();
[DataCoCoIssue] = convertCurrenciesToEUR(DataCoCoIssue,FXData,{'AmountIssued'});

%% CoCo (amount) issued over time
DataCoCoIssue.Year = year(DataCoCoIssue.IssueDateDN);
DataCoCoIssue = sortrows(DataCoCoIssue,'IssueDate','ascend');
DataCoCoIssue = sortrows(DataCoCoIssue,'Ticker','ascend');

% CoCos issued over time and CoCo amount issued over time
filename = 'TableCoCosOverview.xlsx';
CoCoIssuesOverTime.BBData.Total = varfun(@nansum,DataCoCoIssue(:,{'AmountIssued_EUR','Year'}),'GroupingVariables',{'Year'});
xlswrite(filename,CoCoIssuesOverTime.BBData.Total.GroupCount','L6:S6')
xlswrite(filename,CoCoIssuesOverTime.BBData.Total.nansum_AmountIssued_EUR','L60:S60')
CoCoIssuesOverTime.MergedData.Total = varfun(@nansum,DataC(:,{'CoCoIssued','CoCoIssuedAmountIssued_EUR','YearpartofCLOSDATE'}),'GroupingVariables',{'YearpartofCLOSDATE'});
AmountCoCoIssuedPerIssuer = varfun(@nansum,DataCoCoIssue(:,{'AmountIssued_EUR','Year','Ticker'}),'GroupingVariables',{'Ticker','Year'});
counter1 = 10;
counter2 = 64;
for c = unique(DataC.CountryName)'
    IndexC = strcmpi(DataC.CountryName,c{1});
    CoCoIssuesOverTime.MergedData.(strrep(c{1},' ','_')) = varfun(@nansum,DataC(IndexC,{'CoCoIssued','CoCoIssuedAmountIssued_EUR','YearpartofCLOSDATE'}),'GroupingVariables',{'YearpartofCLOSDATE'});
    xlswrite(filename,CoCoIssuesOverTime.MergedData.(strrep(c{1},' ','_')).nansum_CoCoIssued',strcat('C',num2str(counter1),':S',num2str(counter1)))
    xlswrite(filename,c,strcat('T',num2str(counter1),':T',num2str(counter1)))
    xlswrite(filename,CoCoIssuesOverTime.MergedData.(strrep(c{1},' ','_')).nansum_CoCoIssuedAmountIssued_EUR',strcat('C',num2str(counter2),':S',num2str(counter2)))
    xlswrite(filename,c,strcat('T',num2str(counter2),':T',num2str(counter2)))
    counter1=counter1+1;
    counter2=counter2+1;
end

% First CoCo issues over time
DataC.CoCoIssued_oneOrMore = DataC.CoCoIssued>=1;
DataCoCoIssue.FirstCoCoIssue = NaN(size(DataCoCoIssue,1),1);
DataCoCoIssue.FirstCoCoIssue(2:end) = ~strcmpi(DataCoCoIssue.Ticker(2:end),DataCoCoIssue.Ticker(1:end-1));
FirstCoCoIssuesOverTime.BBData.Total = varfun(@nansum,DataCoCoIssue(:,{'FirstCoCoIssue','Year'}),'GroupingVariables',{'Year'});
xlswrite(filename,FirstCoCoIssuesOverTime.BBData.Total.nansum_FirstCoCoIssue','L33:S33')
DataC.FirstCoCoIssue = NaN(size(DataC,1),1);
DataC.FirstCoCoIssue(2:end) = DataC.CoCoIssued(2:end)>=1 & DataC.DummyHasIssuedCoCo(1:end-1)==0 & DataC.BvDIDNumbers(1:end-1)==DataC.BvDIDNumbers(2:end);
FirstCoCoIssuesOverTime.MergedData.Total = varfun(@nansum,DataC(:,{'FirstCoCoIssue','YearpartofCLOSDATE'}),'GroupingVariables',{'YearpartofCLOSDATE'});
counter1 = 37;
for c = unique(DataC.CountryName)'
    IndexC = strcmpi(DataC.CountryName,c{1});
    FirstCoCoIssuesOverTime.MergedData.(strrep(c{1},' ','_')) = varfun(@nansum,DataC(IndexC,{'FirstCoCoIssue','YearpartofCLOSDATE'}),'GroupingVariables',{'YearpartofCLOSDATE'});
    xlswrite(filename,FirstCoCoIssuesOverTime.MergedData.(strrep(c{1},' ','_')).nansum_FirstCoCoIssue',strcat('C',num2str(counter1),':S',num2str(counter1)))
    xlswrite(filename,c,strcat('T',num2str(counter1),':T',num2str(counter1)))
    counter1=counter1+1;
end


% % CoCos issuers over time
% CoCoIssuersOverTime.BBData.Total = varfun(@nansum,AmountCoCoIssuedPerIssuer(:,{'nansum_AmountIssued','Year'}),'GroupingVariables',{'Year'});
% DataC.CoCoIssued_oneOrMore = DataC.CoCoIssued>=1;
% CoCoIssuersOverTime.MergedData.Total = varfun(@nansum,DataC(:,{'CoCoIssued_oneOrMore','CoCoIssuedAmountIssued_EUR','YearpartofCLOSDATE'}),'GroupingVariables',{'YearpartofCLOSDATE'});
% for c = unique(DataC.CountryName)'
%     IndexC = strcmpi(DataC.CountryName,c{1});
%     CoCoIssuersOverTime.MergedData.(strrep(c{1},' ','_')) = varfun(@nansum,DataC(IndexC,{'CoCoIssued_oneOrMore','YearpartofCLOSDATE'}),'GroupingVariables',{'YearpartofCLOSDATE'});
% end