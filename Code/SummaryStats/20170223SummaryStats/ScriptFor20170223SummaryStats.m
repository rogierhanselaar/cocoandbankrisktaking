clear
%% Load data
pcinfo=java.net.InetAddress.getLocalHost;
if ~strcmpi(pcinfo.getHostName,'LAPTOP-6P6G4FB7')
    error('Havent set the path yet for the dell laptop');
%     pathname = '';
else
    pathname1 = 'C:\Users\rogie\Git\cocoandbankrisktaking\Code\';
    pathname2 = 'C:\Users\rogie\Git\cocoandbankrisktaking\Data\MergeData\';
    pathname3 = 'C:\Users\rogie\Dropbox\PhD\Data\ProjectsWithProfPurnanandam_RogierHanselaar\CoCosAndRiskTaking\Data\BloombergData\Issues\';
end

% Load merged data
addpath(pathname1);
[DataIssuers,DataIssuersWithoutLloyds,DataNonIssuers] = loadData();
DataIssuers.CountryISOcode = categorical(DataIssuers.CountryISOcode);

% Load unmerged issuance data
addpath(pathname2);
DataCoCoIssue = importCoCoIssueData(strcat(pathname3,'20161121BloombergDataCocos.xlsx'));

%% CoCo (amount) issued over time
DataCoCoIssue.Year = year(DataCoCoIssue.IssueDateDN);
DataCoCoIssue = sortrows(DataCoCoIssue,'IssueDate','ascend');
DataCoCoIssue = sortrows(DataCoCoIssue,'Ticker','ascend');

% CoCos issued over time and CoCo amount issued over time
CoCoIssuesOverTime.BBData.Total = varfun(@nansum,DataCoCoIssue(:,{'AmountIssued','Year'}),'GroupingVariables',{'Year'});
CoCoIssuesOverTime.MergedData.Total = varfun(@nansum,DataIssuers(:,{'CoCoIssued','CoCoAmountIssued','YearpartofCLOSDATE'}),'GroupingVariables',{'YearpartofCLOSDATE'});
AmountCoCoIssuedPerIssuer = varfun(@nansum,DataCoCoIssue(:,{'AmountIssued','Year','Ticker'}),'GroupingVariables',{'Ticker','Year'});
for c = unique(DataIssuers.CountryISOcode)'
    IndexC = DataIssuers.CountryISOcode == c;
    CountryCode = cellstr(unique(DataIssuers.CountryISOcode(IndexC)));
    CoCoIssuesOverTime.MergedData.(CountryCode{1}) = varfun(@nansum,DataIssuers(IndexC,{'CoCoIssued','CoCoAmountIssued','YearpartofCLOSDATE'}),'GroupingVariables',{'YearpartofCLOSDATE'});
end

% CoCos issuers over time
CoCoIssuersOverTime.BBData.Total = varfun(@nansum,AmountCoCoIssuedPerIssuer(:,{'nansum_AmountIssued','Year'}),'GroupingVariables',{'Year'});
DataIssuers.CoCoIssued_oneOrMore = DataIssuers.CoCoIssued>=1;
CoCoIssuersOverTime.MergedData.Total = varfun(@nansum,DataIssuers(:,{'CoCoIssued_oneOrMore','CoCoAmountIssued','YearpartofCLOSDATE'}),'GroupingVariables',{'YearpartofCLOSDATE'});
for c = unique(DataIssuers.CountryISOcode)'
    IndexC = DataIssuers.CountryISOcode == c;
    CountryCode = cellstr(unique(DataIssuers.CountryISOcode(IndexC)));
    CoCoIssuersOverTime.MergedData.(CountryCode{1}) = varfun(@nansum,DataIssuers(IndexC,{'CoCoIssued_oneOrMore','YearpartofCLOSDATE'}),'GroupingVariables',{'YearpartofCLOSDATE'});
end

% First CoCo issues over time
DataCoCoIssue.FirstCoCoIssue = NaN(size(DataCoCoIssue,1),1);
DataCoCoIssue.FirstCoCoIssue(2:end) = ~strcmpi(DataCoCoIssue.Ticker(2:end),DataCoCoIssue.Ticker(1:end-1));
FirstCoCoIssuesOverTime.BBData.Total = varfun(@nansum,DataCoCoIssue(:,{'FirstCoCoIssue','Year'}),'GroupingVariables',{'Year'});
DataIssuers.FirstCoCoIssue = NaN(size(DataIssuers,1),1);
DataIssuers.FirstCoCoIssue(2:end) = DataIssuers.CoCoIssued(2:end)>=1 & DataIssuers.DummyOneOrMoreCoCosIssued(1:end-1)==0 & DataIssuers.BvDIDNumbers(1:end-1)==DataIssuers.BvDIDNumbers(2:end);
FirstCoCoIssuesOverTime.MergedData.Total = varfun(@nansum,DataIssuers(:,{'FirstCoCoIssue','YearpartofCLOSDATE'}),'GroupingVariables',{'YearpartofCLOSDATE'});
for c = unique(DataIssuers.CountryISOcode)'
    IndexC = DataIssuers.CountryISOcode == c;
    CountryCode = cellstr(unique(DataIssuers.CountryISOcode(IndexC)));
    FirstCoCoIssuesOverTime.MergedData.(CountryCode{1}) = varfun(@nansum,DataIssuers(IndexC,{'FirstCoCoIssue','YearpartofCLOSDATE'}),'GroupingVariables',{'YearpartofCLOSDATE'});
end


%% Funding Ratios
funding_ratios = {'CoreTier1RegulatoryCapitalRatio','Tier1RegulatoryCapitalRatio','TotalRegulatoryCapitalRatio'}; % Van deze nog even kijken of ze overeenkomen met de andere funding variabelen
fundingRatiosSumStats = sumStatsOverTime(DataIssuers,DataNonIssuers,funding_ratios);

%% Risk ratios over time
risk_ratios = {'LoanLossResGrossLoans','LoanLossProvNetIntRev','ImpairedLoansGrossLoans','NonInterestExpenseAverageAssets','NonOpItemsNetIncome','NetLoansTotDepBor','NonOpItemsTaxesAvgAst'};
riskRatiosSumStats = sumStatsOverTime(DataIssuers,DataNonIssuers,risk_ratios);

%% Market risk over time
market_risk_vars = {'AnnualVol_pct'};
marketRatiosSumStats = sumStatsOverTime(DataIssuers,DataNonIssuers,market_risk_vars);

%% Trading ratios over time
% Calculate additional ratios
DataIssuers.TradingLiabOverTotalAssets = DataIssuers.TradingLiabilities_EUR./DataIssuers.TotalAssets_EUR;
DataNonIssuers.TradingLiabOverTotalAssets = DataNonIssuers.TradingLiabilities_EUR./DataNonIssuers.TotalAssets_EUR;

DataIssuers.NetGainsOnTradingAndDerivativesOverNetInterestIncome = DataIssuers.NetGainsOnTradingAndDerivatives_EUR./DataIssuers.NetInterestIncome_EUR; 
DataNonIssuers.NetGainsOnTradingAndDerivativesOverNetInterestIncome = DataNonIssuers.NetGainsOnTradingAndDerivatives_EUR./DataNonIssuers.NetInterestIncome_EUR;
DataIssuers.NetGainsOnTradingAndDerivativesOverNetInterestIncome(DataIssuers.NetInterestIncome_EUR<=0)=NaN;
DataNonIssuers.NetGainsOnTradingAndDerivativesOverNetInterestIncome(DataNonIssuers.NetInterestIncome_EUR<=0)=NaN;

DataIssuers.TradingAssetsOverTotalAssets = DataIssuers.TotalTradingAssetsAtFVThroughIncomeStatement_EUR./DataIssuers.TotalAssets_EUR; 
DataNonIssuers.TradingAssetsOverTotalAssets = DataNonIssuers.TotalTradingAssetsAtFVThroughIncomeStatement_EUR./DataNonIssuers.TotalAssets_EUR;

DataIssuers.TradingAssets2OverTotalAssets = DataIssuers.TradingSecuritiesAndAtFVThroughIncome_EUR./DataIssuers.TotalAssets_EUR;
DataNonIssuers.TradingAssets2OverTotalAssets = DataNonIssuers.TradingSecuritiesAndAtFVThroughIncome_EUR./DataNonIssuers.TotalAssets_EUR;

% Notes:
% - NetInterestIncome and NetInterestRevenue have similar coverage
% - NetGainsOnTradingAndDerivatives_EUR and NetGainsOnTradingAndDerivatives2_EUR idem

trading_ratios = {'TradingAssetsOverTotalAssets','TradingAssets2OverTotalAssets','TradingLiabOverTotalAssets','NetGainsOnTradingAndDerivativesOverNetInterestIncome'};
tradingRatiosSumStats = sumStatsOverTime(DataIssuers,DataNonIssuers,trading_ratios);

%% To select all banks that issued CoCos in a particular country after filters(!!!) :
% DataIssuers(strcmpi(DataIssuers.CountryName,'Italy') & DataIssuers.CoCoIssued>0,{'NAME','YearpartofCLOSDATE','CoCoIssued'});
